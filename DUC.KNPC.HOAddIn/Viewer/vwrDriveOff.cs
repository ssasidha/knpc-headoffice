﻿using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;
using DUC.KNPC.HOAddIn.HOConfig;

namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrDriveOff : Form
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private DateTime _accountDate;
        private int _shiftNumber;
        private int _eodNo;
        private StationDTO _stationDetail;
        private ShiftDetailsDTO _shiftDetail;

        private StationData.sp_getDriveOffDetailsDataTable driveOffTable;
        private sp_getDriveOffDetailsTableAdapter adapter;

        public vwrDriveOff()
        {
            InitializeComponent();
        }
        
        public vwrDriveOff(DateTime accountDate, ShiftDetailsDTO shiftDetail, StationDTO stationDetail)
        {
           
            _accountDate = accountDate;
            _shiftNumber = shiftDetail.ShiftNo;
            _eodNo = shiftDetail.EODNo;
            _stationDetail = stationDetail;
            InitializeComponent();
        }

        private void vwrDriveOff_Load(object sender, System.EventArgs e)
        {

            adapter = new sp_getDriveOffDetailsTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);

            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationName", _stationDetail.StationName.Equals("Select All") ? "All" : _stationDetail.StationName));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationId", _stationDetail.StationId > 0 ? _stationDetail.StationId.ToString() : "All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ShiftNumber", _shiftNumber > 0 ? _shiftNumber.ToString() : "All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountDate", _accountDate.ToString()));

            try
            {
                StationData data = new StationData();
                data.EnforceConstraints = false;
                adapter.Fill(data.sp_getDriveOffDetails, _accountDate, _shiftNumber > 0 ? _shiftNumber :(int?) null, _stationDetail.StationId > 0 ? _stationDetail.StationId : (int?)null);
                driveOffTable = data.sp_getDriveOffDetails;
            }
            catch (Exception ex)
            {
                driveOffTable = new StationData.sp_getDriveOffDetailsDataTable();
            }

            ReportDataSource ds = new ReportDataSource("DriveOffDetails", driveOffTable.Rows);

            this.reportViewer1.LocalReport.DataSources.Add(ds);
            this.reportViewer1.RefreshReport();
            this.panel1.Dock = DockStyle.Fill;
            this.panel1.Controls.Add(this.reportViewer1);
            this.reportViewer1.Dock = DockStyle.Fill;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
