﻿using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using DUC.KNPC.HOAddIn.HOConfig;
using Microsoft.Reporting.WinForms;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrEndOfDay : Form
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private DateTime _accountDate;
        private StationDTO _stationDetail;
        private StationData.EOD_GetSafeDropReportDataTable _safeDropDT = new StationData.EOD_GetSafeDropReportDataTable();
        private EOD_GetSafeDropReportTableAdapter _safeDropAdapter = new EOD_GetSafeDropReportTableAdapter();
        private EOD_GetDepartmentWiseSaleTableAdapter _WiseSale = new EOD_GetDepartmentWiseSaleTableAdapter();
        private EOD_MOPSummaryGetMOPTotalTableAdapter _mopAdapter = new EOD_MOPSummaryGetMOPTotalTableAdapter();
        private EOD_FuelStockReconciliationTableAdapter _fuelDA = new EOD_FuelStockReconciliationTableAdapter();
        private EOD_GetPumpMeterReadingTableAdapter _pumbDA = new EOD_GetPumpMeterReadingTableAdapter();
        private EOD_GetJournalEntriesForAccountDateTableAdapter _journ = new EOD_GetJournalEntriesForAccountDateTableAdapter();

        public vwrEndOfDay(FilterDTO filter)
        {
            _safeDropAdapter.Connection = new SqlConnection(CONNECTION_STRING);
            _mopAdapter.Connection = new SqlConnection(CONNECTION_STRING);
            _WiseSale.Connection = new SqlConnection(CONNECTION_STRING);
            _mopAdapter.Connection = new SqlConnection(CONNECTION_STRING);
            _fuelDA.Connection = new SqlConnection(CONNECTION_STRING);
            _pumbDA.Connection = new SqlConnection(CONNECTION_STRING);
            _journ.Connection = new SqlConnection(CONNECTION_STRING);

            InitializeComponent();
            _accountDate =filter.AccountDate;
            _stationDetail = filter.Station;
        }

        private void OnViewerLoaded(object sender, EventArgs e)
        {
            Repository rep = new Repository();
            var accDetails = rep.GetAccountDateDetails(_accountDate, _stationDetail.StationId);

            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpAccountDate", _accountDate.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationName", _stationDetail.StationName.Equals("Select All") ? "All" : _stationDetail.StationName));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationId", _stationDetail.StationId > 0 ? _stationDetail.StationId.ToString() : "All"));

            if (accDetails.Rows.Count > 0)
            {
                if (accDetails.Rows[0]["EndTime"].ToString().Trim().Equals(""))
                {
                    this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountEndDate", " "));
                }
                else
                {
                    this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountEndDate", DateTime.Parse(accDetails.Rows[0]["EndTime"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt")));
                }
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountStartDate", DateTime.Parse(accDetails.Rows[0]["StartTime"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt")));
            }
            else
            {
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountEndDate", " "));
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountStartDate", " "));
            }
            StationData data = new StationData();
            data.EnforceConstraints = false;

            _safeDropAdapter.Fill(data.EOD_GetSafeDropReport, _accountDate, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : (int?)null);
            ReportDataSource ds = new ReportDataSource("dsEoD", data.EOD_GetSafeDropReport.Rows);

            _WiseSale.Fill(data.EOD_GetDepartmentWiseSale, _accountDate, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : (int?)null);
            ReportDataSource ds2 = new ReportDataSource("dsDeparmentwiseSales", data.EOD_GetDepartmentWiseSale.Rows);

            _mopAdapter.Fill(data.EOD_MOPSummaryGetMOPTotal, _accountDate, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : (int?)null);
            ReportDataSource ds3 = new ReportDataSource("dsMopwiseSalesSummary", data.EOD_MOPSummaryGetMOPTotal.Rows);

            _fuelDA.Fill(data.EOD_FuelStockReconciliation, _accountDate, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : (int?)null);
            ReportDataSource ds4 = new ReportDataSource("dsFuelStockReconciliation", data.EOD_FuelStockReconciliation.Rows);

            _pumbDA.Fill(data.EOD_GetPumpMeterReading, _accountDate, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : (int?)null);
            ReportDataSource ds5 = new ReportDataSource("ds_Pump", data.EOD_GetPumpMeterReading.Rows);

            _journ.Fill(data.EOD_GetJournalEntriesForAccountDate,_accountDate,_stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : (int?)null);
            ReportDataSource ds6 = new ReportDataSource("StationJournalDS", data.EOD_GetJournalEntriesForAccountDate.Rows);

            this.reportViewer1.LocalReport.DataSources.Add(ds);
            this.reportViewer1.LocalReport.DataSources.Add(ds2);
            this.reportViewer1.LocalReport.DataSources.Add(ds3);
            this.reportViewer1.LocalReport.DataSources.Add(ds4);
            this.reportViewer1.LocalReport.DataSources.Add(ds5);
            this.reportViewer1.LocalReport.DataSources.Add(ds6);

            this.reportViewer1.RefreshReport();
            this.panel1.Controls.Add(this.reportViewer1);
        }
    }
}
