﻿using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using DUC.KNPC.HOAddIn.HOConfig;
using Microsoft.Reporting.WinForms;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrAccountDateSummary : Form
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private DateTime _accountDate;
        private StationDTO _stationDetail;
        private StationData.sp_GetAccountDateSummaryReportDataTable _accountDateSummary = new StationData.sp_GetAccountDateSummaryReportDataTable();
        private sp_GetAccountDateSummaryReportTableAdapter _adapter = new sp_GetAccountDateSummaryReportTableAdapter();
        public vwrAccountDateSummary(FilterDTO filter)
        {
            _adapter.Connection = new SqlConnection(CONNECTION_STRING);
            InitializeComponent();

            _accountDate = filter.AccountDate;
            _stationDetail = filter.Station;
        }

      
        private void OnViewerLoaded(object sender, EventArgs e)
        {
            Repository rep = new Repository();
            var accDetails = rep.GetAccountDateDetails(_accountDate, _stationDetail.StationId);

            try
            {
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpAccountDate", _accountDate.ToString("dd/MM/yyyy")));
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpSiteName", _stationDetail.StationName.Equals("Select All") ? "All" : _stationDetail.StationName));
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpSiteId", _stationDetail.StationId > 0 ? _stationDetail.StationId.ToString() : "All"));

                if (accDetails.Rows.Count > 0)
                {
                    if (accDetails.Rows[0]["EndTime"].ToString().Trim().Equals(""))
                    {
                        this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountEndDate", " "));
                    }
                    else
                    {
                        this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountEndDate", DateTime.Parse(accDetails.Rows[0]["EndTime"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt")));
                    }
                    this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountStartDate", DateTime.Parse(accDetails.Rows[0]["StartTime"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt")));
                }
                else
                {
                    this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountEndDate", " "));
                    this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountStartDate", " "));
                }
                StationData data = new StationData();
                data.EnforceConstraints = false;
                _adapter.Fill(data.sp_GetAccountDateSummaryReport, _accountDate, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : (int?)null);
                ReportDataSource ds = new ReportDataSource("dsAccountDateSummary", data.sp_GetAccountDateSummaryReport.Rows);

                this.reportViewer1.LocalReport.DataSources.Add(ds);

                this.reportViewer1.RefreshReport();
                this.panel1.Controls.Add(this.reportViewer1);
                this.reportViewer1.Dock = DockStyle.Fill;
                this.panel1.Dock = DockStyle.Fill;
            }
            catch(Exception ex)
            {

            }
        }
    }
}
