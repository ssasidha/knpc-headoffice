﻿using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using DUC.KNPC.HOAddIn.HOConfig;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrAuthorizationFailure : Form
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private DateTime _accountDateFrom, _accountDateTo;
        private PaymentTypeDTO _Token;
        private ProductGroupDTO _Product;
        private string _TerminalID;
        private StationDTO _stationDetail;
        
        private StationData.TokenFailure_PrepaidDataTable tokenfailureDataTable; 
        private StationData.TokenFailure_DryStockDataTable dsFaliureDataTable ;

        private TokenFailure_PrepaidTableAdapter _PrepaidDA;
        private TokenFailure_DryStockTableAdapter _dryStockDA;

        public vwrAuthorizationFailure(FilterDTO filter)
        {
            _accountDateFrom = filter.FromDate;
            _accountDateTo = filter.ToDate;
            _stationDetail = filter.Station;
            _Token = filter.PaymentType;
            _Product = filter.ProductType;
            _TerminalID =filter.TerminalID;
            InitializeComponent();
        }

        private void OnReportLoaded(object sender, EventArgs e)
        {

            _dryStockDA =  new TokenFailure_DryStockTableAdapter();
            _PrepaidDA = new TokenFailure_PrepaidTableAdapter();

            _dryStockDA.Connection = new SqlConnection(CONNECTION_STRING);
            _PrepaidDA.Connection = new SqlConnection(CONNECTION_STRING);

            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationName", _stationDetail.StationName.Equals("Select All") ? "All" : _stationDetail.StationName));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationId", _stationDetail.StationId < 0 ? "All": _stationDetail.StationId.ToString()));           
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("TransactionFromDate", _accountDateFrom.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("TransactionToDate", _accountDateTo.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ProductGroup", _Product.GroupName.Equals("Select All") ? "All" : _Product.GroupName));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("TerminalId", _TerminalID.Equals("Select All") ? "All" : _TerminalID));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("TokenType", _Token.PayType.Equals("Select All") ? "All" :_Token.PayType));

            try
            {
                StationData data = new StationData();
                data.EnforceConstraints = false;
                _PrepaidDA.Fill(data.TokenFailure_Prepaid, _accountDateFrom, _accountDateTo, _Token.PayType.Equals("Select All") ? null : _Token.PayType , _TerminalID.Equals("Select All") ? (int?)null : int.Parse(_TerminalID), _stationDetail.StationId);
                _dryStockDA.Fill(data.TokenFailure_DryStock, _accountDateFrom, _accountDateTo, _Token.PayType.Equals("Select All") ? null : _Token.PayType, _TerminalID.Equals("Select All") ? (int?)null : int.Parse(_TerminalID), _stationDetail.StationId);

                tokenfailureDataTable = data.TokenFailure_Prepaid;
                dsFaliureDataTable = data.TokenFailure_DryStock;
            }
            catch (Exception ex)
            {
                tokenfailureDataTable = new StationData.TokenFailure_PrepaidDataTable();
                dsFaliureDataTable = new StationData.TokenFailure_DryStockDataTable();
            }

            ReportDataSource ds1 = new ReportDataSource("DryStockDS", dsFaliureDataTable.Rows);
            ReportDataSource ds2 = new ReportDataSource("TokenFailureDS", tokenfailureDataTable.Rows);
            
            this.reportViewer1.LocalReport.DataSources.Add(ds2);
            this.reportViewer1.LocalReport.DataSources.Add(ds1);

            this.reportViewer1.RefreshReport();
            this.panel1.Controls.Add(this.reportViewer1);
        }
    }
}
