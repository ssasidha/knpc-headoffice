﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;
using DUC.KNPC.HOAddIn.HOConfig;
namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrFuelDelivery : Form
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private DateTime _accountDateFrom,_accountDateTo;
        private StationDTO _stationDetail;
        private StationData.FuelDelivery_GetFuelDeliveryFromToDataTable _FuelDeliveryDT = new StationData.FuelDelivery_GetFuelDeliveryFromToDataTable();
        private FuelDelivery_GetFuelDeliveryFromToTableAdapter _FuelDeliveryDA = new FuelDelivery_GetFuelDeliveryFromToTableAdapter();
        private int _product;


        public vwrFuelDelivery(FilterDTO filterDto)
        {
            _FuelDeliveryDA.Connection = new SqlConnection(CONNECTION_STRING);
            InitializeComponent();
            _accountDateFrom = filterDto.FromDate;
            _accountDateTo = filterDto.ToDate;
            _stationDetail = filterDto.Station;
            _product = filterDto.Product;
        }

        private void OnViewerLoaded(object sender, EventArgs e)
        {
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("FromDate", _accountDateFrom.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ToDate", _accountDateTo.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationName", _stationDetail.StationName.Equals("Select All") ? "All" : _stationDetail.StationName));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationId", _stationDetail.StationId > 0 ? _stationDetail.StationId.ToString() : "All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("Grade", _product > 0 ? _product.ToString() : "All" ));
           
            StationData data = new StationData();
            data.EnforceConstraints = false;
            _FuelDeliveryDA.Fill(data.FuelDelivery_GetFuelDeliveryFromTo, _accountDateFrom, _accountDateTo, _product > 0 ? (int?)_product : null, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : null);
            ReportDataSource ds = new ReportDataSource("dsFuelDelivery", data.FuelDelivery_GetFuelDeliveryFromTo.Rows);

            this.reportViewer1.LocalReport.DataSources.Add(ds);
            this.reportViewer1.RefreshReport();
            this.panel1.Controls.Add(this.reportViewer1);
        }
    }
}
