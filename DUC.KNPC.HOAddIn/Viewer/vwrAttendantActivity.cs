﻿using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using Microsoft.Reporting.WinForms;
using System;
using System.Windows.Forms;
using DUC.KNPC.HOAddIn.ToolbarCommands;
using System.Data.SqlClient;
using DUC.KNPC.HOAddIn.HOConfig;

namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrAttendantActivity : Form
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");

        private DateTime _accountDate;
        private int _shiftNumber    ;
        private int _attendantId;
        private short _attendantStatus;
        private string _attendantName;
        private StationDTO _stationDetail;
        private string _shiftName;
        private StationData.sp_GetAttendantActivityDataTable _activityDT = new StationData.sp_GetAttendantActivityDataTable();
        private sp_GetAttendantActivityTableAdapter _adapter;
      

        internal vwrAttendantActivity(FilterDTO searchFilterDTO)
        {
            _accountDate = searchFilterDTO.AccountDate;
            _shiftName = searchFilterDTO.Shift.ShiftName;
            _shiftNumber = searchFilterDTO.Shift.ShiftNo;
            _attendantId = searchFilterDTO.Attendant.Id;
            _attendantName = searchFilterDTO.Attendant.Name;
            _stationDetail = searchFilterDTO.Station;
            _adapter = new sp_GetAttendantActivityTableAdapter();
            _adapter.Connection = new SqlConnection(CONNECTION_STRING);
            InitializeComponent();
        }


        private void OnReportLoaded(object sender, EventArgs e)
        {
            string attendantStatus = " ";
            switch (_attendantStatus)
            {
                case -1:
                    attendantStatus = "All";
                    break;
                case 1:
                    attendantStatus = "Logged In";
                    break;
                case 2:
                    attendantStatus = "Logged Off";
                    break;
            }
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ShiftNumber", _shiftNumber > 0 ? _shiftName : "All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountDate", _accountDate.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AttendantId", _attendantId > 0 ? _attendantId.ToString() : "All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AttendantName",_attendantId > 0 ? _attendantName:"All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AttendantStatus", attendantStatus));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter(
                "StationName", _stationDetail.StationId != -1 ? _stationDetail.StationName : "All"));
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationId", _stationDetail.StationId!=-1?_stationDetail.StationId.ToString():"All"));

            this._adapter.Fill(this._activityDT, _accountDate, _shiftNumber, _attendantId, _attendantStatus,
                _stationDetail.StationId > 0 ? (int?) _stationDetail.StationId : null);
            var ds = new ReportDataSource("AttendantActivityDS", _activityDT.Rows);
            this.reportViewer1.LocalReport.DataSources.Add(ds);
            this.reportViewer1.RefreshReport();
            this.panel1.Controls.Add(this.reportViewer1);
        }
    }
}
