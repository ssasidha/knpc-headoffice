﻿using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using DUC.KNPC.HOAddIn.HOConfig;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrToken : Form
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private DateTime _accountDate;
        private StationDTO _stationDetail;
        private ShiftDetailsDTO _ShiftList;
        private PaymentTypeDTO _PaymentType;
        private int _shiftNumber;
        private string _terminalID;
        private string _fillingPoint;
        private StationData.TokenDetails_GetTokenDetailsDataTable _TokenDT = new StationData.TokenDetails_GetTokenDetailsDataTable();
        private TokenDetails_GetTokenDetailsTableAdapter _TokenDA =  new TokenDetails_GetTokenDetailsTableAdapter();


        public vwrToken(FilterDTO filterdto)
        {
            _TokenDA.Connection = new SqlConnection(CONNECTION_STRING);

            InitializeComponent();
            _accountDate = filterdto.AccountDate;
            _stationDetail = filterdto.Station;
            _ShiftList = filterdto.Shift;
            _PaymentType = filterdto.PaymentType;
            _shiftNumber = filterdto.Shift.ShiftNo;
            _terminalID = filterdto.TerminalID;
            _fillingPoint = filterdto.Filling.Name;
        }

        private void OnViewerLoaded(object sender, EventArgs e)
        {
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountDate", _accountDate.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationName", _stationDetail.StationName.Equals("Select All") ? "All" : _stationDetail.StationName));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationId", _stationDetail.StationId > 0 ? _stationDetail.StationId.ToString():"All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ShiftNumber", _shiftNumber > 0 ? _shiftNumber.ToString() : "All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("MOP", _PaymentType.PayType.ToString().Equals("Select All") ? "All" : _PaymentType.PayType.ToString()));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("FillingPoint", _fillingPoint.Equals("Select All") ? "All" : _fillingPoint));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("TerminalID", _terminalID.Equals("Select All") ? "All" : _terminalID));
        
            var shi = _ShiftList.ShiftNo > 0 ? (int?)_ShiftList.ShiftNo : null;

            StationData data = new StationData();
            data.EnforceConstraints = false;

            _TokenDA.Fill(data.TokenDetails_GetTokenDetails, _accountDate, _ShiftList.ShiftNo > 0 ? (int?)_ShiftList.ShiftNo : null, _ShiftList.EODNo > 0 ? (int?)_ShiftList.EODNo : null, _PaymentType.PayType.Equals("Select All") ? null : _PaymentType.PayType.ToString(), _terminalID.Equals("Select All") ? (int?)null : (int?)int.Parse(_terminalID), _fillingPoint.Equals("Select All") ? (int?)null : (int?)int.Parse(_fillingPoint), _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : null);
            ReportDataSource ds = new ReportDataSource("ds_Token", data.TokenDetails_GetTokenDetails.Rows);
            this.reportViewer1.LocalReport.DataSources.Add(ds);
            this.reportViewer1.RefreshReport();
            this.panel1.Controls.Add(this.reportViewer1);
        }
    }
}
