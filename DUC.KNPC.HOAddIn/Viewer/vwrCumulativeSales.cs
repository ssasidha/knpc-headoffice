﻿using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using DUC.KNPC.HOAddIn.HOConfig;
using Microsoft.Reporting.WinForms;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrCumulativeSales : Form
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private DateTime _fromDate;
        private DateTime _toDate;
        private StationDTO _stationDetail;
        private StationData.sp_getCumulativeSalesDataTable _dataTable = new StationData.sp_getCumulativeSalesDataTable();
        private sp_getCumulativeSalesTableAdapter _adapter = new sp_getCumulativeSalesTableAdapter();

        public vwrCumulativeSales(FilterDTO filter)
        {
            _adapter.Connection = new SqlConnection(CONNECTION_STRING);
            InitializeComponent();

            _fromDate = filter.FromDate;
            _toDate = filter.ToDate;
            _stationDetail = filter.Station;
        }

        private void OnViewerLoaded(object sender, EventArgs e)
        {
            Repository rep = new Repository();

            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpFromDate", _fromDate.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpToDate", _toDate.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpStationName", _stationDetail.StationName.Equals("Select All") ? "All" : _stationDetail.StationName));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpStationId", _stationDetail.StationId > 0 ? _stationDetail.StationId.ToString() : "All"));

            StationData data = new StationData();
            data.EnforceConstraints = false;

            _adapter.Fill(data.sp_getCumulativeSales, _fromDate, _toDate, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : (int?)null);
            ReportDataSource ds = new ReportDataSource("dsCumulativeSalesData", data.sp_getCumulativeSales.Rows);

            this.reportViewer1.LocalReport.DataSources.Add(ds);

            this.reportViewer1.RefreshReport();
            this.panel1.Controls.Add(this.reportViewer1);
            this.panel1.Dock = DockStyle.Fill;
            this.reportViewer1.Dock = DockStyle.Fill;
        }
    }
}
