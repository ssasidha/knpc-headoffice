﻿using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;
using DUC.KNPC.HOAddIn.HOConfig;

namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrDetailedSafeDrop : Form
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private DateTime _accountDate;
        private int _shiftNumber;
        private int _eodNo;
        private StationDTO _stationDetail;
        private ShiftDetailsDTO _shiftDetail;
        private StationData.SP_GetAttendantSafeDropAllDataTable safeDropTable ;
        private SP_GetAttendantSafeDropAllTableAdapter adapter ;

        public vwrDetailedSafeDrop()
        {
            InitializeComponent();
        }

        public vwrDetailedSafeDrop(FilterDTO dto)
        {
            _accountDate = dto.AccountDate;
            _shiftNumber = dto.Shift.ShiftNo;
            _eodNo = dto.Shift.EODNo;
            _stationDetail = dto.Station;
            InitializeComponent();
        }

        private void vwrDetailedSafeDrop_Load(object sender, System.EventArgs e)
        {
            var shiftDetails = (new GetShiftDetailsTableAdapter()).GetData(_accountDate, _shiftNumber, _eodNo, _stationDetail.StationId);
            adapter = new SP_GetAttendantSafeDropAllTableAdapter();

            adapter.Connection = new SqlConnection(CONNECTION_STRING);

            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("SiteName", _stationDetail.StationName.Equals("Select All") ? "All" : _stationDetail.StationName));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("SiteID", _stationDetail.StationId > 0 ? _stationDetail.StationId.ToString() : "All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ShiftNumber", _shiftNumber > 0 ? _shiftNumber.ToString() : "All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpAccountDate", _accountDate.ToString("dd/MM/yyyy")));

            if (shiftDetails.Rows.Count > 0)
            {
                this.reportViewer1.LocalReport.SetParameters(new ReportParameter("ShiftStart", DateTime.Parse(shiftDetails.Rows[0]["StartOfShift"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt")));
                this.reportViewer1.LocalReport.SetParameters(new ReportParameter("ShiftEnd", DateTime.Parse(shiftDetails.Rows[0]["EndOfShift"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt")));
            }
            else
            {
                this.reportViewer1.LocalReport.SetParameters(new ReportParameter("ShiftStart", " "));
                this.reportViewer1.LocalReport.SetParameters(new ReportParameter("ShiftEnd", " "));
            }


            try
            {
                StationData data = new StationData();
                data.EnforceConstraints = false;
                adapter.Fill(data.SP_GetAttendantSafeDropAll, _accountDate, _stationDetail.StationId, _shiftNumber > 0 ? _shiftNumber : -1);
                safeDropTable = data.SP_GetAttendantSafeDropAll;
            }
            catch (Exception ex)
            {
                safeDropTable = new StationData.SP_GetAttendantSafeDropAllDataTable();
            }
            ReportDataSource ds = new ReportDataSource("AttendantSafeDropReportAll", safeDropTable.Rows);

            this.reportViewer1.LocalReport.DataSources.Add(ds);
            this.reportViewer1.RefreshReport();
            this.panel1.Dock = DockStyle.Fill;
            this.panel1.Controls.Add(this.reportViewer1);
            this.reportViewer1.Dock = DockStyle.Fill;
        }

    }
}
