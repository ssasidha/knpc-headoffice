﻿using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using DUC.KNPC.HOAddIn.HOConfig;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrEndOfShift : Form
    {

        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private DateTime _accountDate;
        private StationDTO _stationDetail;
        private ShiftDetailsDTO _shiftDetail;
        private EOS_GetAttendantShiftSummaryTableAdapter _ShiftSummaryDA = new EOS_GetAttendantShiftSummaryTableAdapter();
        private EOS_GradeWiseSaleTableAdapter _GradeWiseSaleAdapter = new EOS_GradeWiseSaleTableAdapter();
        private EOS_GetDepartmentWiseSaleTableAdapter _DepartementWiseSale = new EOS_GetDepartmentWiseSaleTableAdapter();
        private EOS_GetPumpMeterReadingTableAdapter _pumbDA = new EOS_GetPumpMeterReadingTableAdapter();
        private EOS_MOPSummaryGetMOPTotalTableAdapter _MOPSalesDA = new EOS_MOPSummaryGetMOPTotalTableAdapter();
        private EOD_GetJournalEntriesForAccountDateAndShiftTableAdapter _journ = new EOD_GetJournalEntriesForAccountDateAndShiftTableAdapter();

        public vwrEndOfShift()
        {
            InitializeComponent();
        }

        public vwrEndOfShift(FilterDTO filterDto)
        {
            _ShiftSummaryDA.Connection = new SqlConnection(CONNECTION_STRING);
            _GradeWiseSaleAdapter.Connection = new SqlConnection(CONNECTION_STRING);
            _DepartementWiseSale.Connection = new SqlConnection(CONNECTION_STRING);
            _pumbDA.Connection = new SqlConnection(CONNECTION_STRING);
            _MOPSalesDA.Connection = new SqlConnection(CONNECTION_STRING);
            _journ.Connection = new SqlConnection(CONNECTION_STRING);

            InitializeComponent();
            _accountDate = filterDto.AccountDate;
            _stationDetail = filterDto.Station;
            _shiftDetail = filterDto.Shift;
        }


        private void OnViewerLoaded(object sender, EventArgs e)
        {

            Repository rep = new Repository();
            var shiftStartDetails  = rep.GetShiftDetails(_accountDate, _shiftDetail.ShiftNo, _shiftDetail.EODNo, _stationDetail.StationId);

            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpAccountDate", _accountDate.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationName", _stationDetail.StationName.Equals("Select All") ? "All" : _stationDetail.StationName));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationId", _stationDetail.StationId> 0 ? _stationDetail.StationId.ToString() : "All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ShiftNumber", _shiftDetail.ShiftNo.ToString()));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationId", _shiftDetail.ShiftName.ToString()));
            if (shiftStartDetails.Rows.Count > 0)
            {
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ShiftStart", DateTime.Parse(shiftStartDetails.Rows[0]["StartOfShift"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt")));
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ShiftEnd", DateTime.Parse(shiftStartDetails.Rows[0]["EndOfShift"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt")));
            }
            else
            {
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ShiftStart", " "));
                this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ShiftEnd", " "));
            }
            StationData data = new StationData();
            data.EnforceConstraints = false;
            
            _ShiftSummaryDA.Fill(data.EOS_GetAttendantShiftSummary, _accountDate, _shiftDetail.ShiftNo > 0 ? (int?)_shiftDetail.ShiftNo : null, _shiftDetail.EODNo > 0 ? (int?)_shiftDetail.EODNo : null,(int?)null, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : null);
            ReportDataSource ds1 = new ReportDataSource("ds_Eos", data.EOS_GetAttendantShiftSummary.Rows);

            _GradeWiseSaleAdapter.Fill(data.EOS_GradeWiseSale, _accountDate, _shiftDetail.ShiftNo > 0 ? (int?)_shiftDetail.ShiftNo : null, _shiftDetail.EODNo > 0 ? (int?)_shiftDetail.EODNo : null, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : null);
            ReportDataSource ds2 = new ReportDataSource("ds_GradeWise", data.EOS_GradeWiseSale.Rows);

            _MOPSalesDA.Fill(data.EOS_MOPSummaryGetMOPTotal, _accountDate, _shiftDetail.ShiftNo > 0 ? (int?)_shiftDetail.ShiftNo : null, _shiftDetail.EODNo > 0 ? (int?)_shiftDetail.EODNo : null, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : null);
            ReportDataSource ds3 = new ReportDataSource("ds_mop", data.EOS_MOPSummaryGetMOPTotal.Rows);

            _pumbDA.Fill(data.EOS_GetPumpMeterReading, _accountDate, _shiftDetail.ShiftNo > 0 ? (int?)_shiftDetail.ShiftNo : null, _shiftDetail.EODNo > 0 ? (int?)_shiftDetail.EODNo : null, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : null);
            ReportDataSource ds4 = new ReportDataSource("ds_PumpMeterReading", data.EOS_GetPumpMeterReading.Rows);

            _DepartementWiseSale.Fill(data.EOS_GetDepartmentWiseSale, _accountDate, _shiftDetail.ShiftNo > 0 ? (int?)_shiftDetail.ShiftNo : null, _shiftDetail.EODNo > 0 ? (int?)_shiftDetail.EODNo : null, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : null);
            ReportDataSource ds5 = new ReportDataSource("ds_EosDept", data.EOS_GetDepartmentWiseSale.Rows);

            _journ.Fill(data.EOD_GetJournalEntriesForAccountDateAndShift, _accountDate, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : (int?)null, _shiftDetail.ShiftNo);
            ReportDataSource ds6 = new ReportDataSource("StationJournalDS", data.EOD_GetJournalEntriesForAccountDateAndShift.Rows);

            this.reportViewer1.LocalReport.DataSources.Add(ds1);
            this.reportViewer1.LocalReport.DataSources.Add(ds2);
            this.reportViewer1.LocalReport.DataSources.Add(ds3);
            this.reportViewer1.LocalReport.DataSources.Add(ds4);
            this.reportViewer1.LocalReport.DataSources.Add(ds5);
            this.reportViewer1.LocalReport.DataSources.Add(ds6);

            this.reportViewer1.RefreshReport();
            this.panel1.Controls.Add(this.reportViewer1);
        }
    }
}
