﻿using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using DUC.KNPC.HOAddIn.HOConfig;
using Microsoft.Reporting.WinForms;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.Viewer
{
    public partial class vwrOfflineTransactions : Form
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private DateTime _accountDate;
        private int _shiftNumber;
        private int _eodNo;
        private StationDTO _stationDetail;
        private PaymentTypeDTO _tokenTypes;
        private StationData.OfflineTokenDetails_GetTokenDetailsDataTable _reportDT = new StationData.OfflineTokenDetails_GetTokenDetailsDataTable();
        private OfflineTokenDetails_GetTokenDetailsTableAdapter _adapter = new OfflineTokenDetails_GetTokenDetailsTableAdapter();

        public vwrOfflineTransactions(FilterDTO filterDto)
        {
          
            _adapter.Connection = new SqlConnection(CONNECTION_STRING);

            _accountDate = filterDto.AccountDate;
            _shiftNumber = filterDto.Shift.ShiftNo;
            _eodNo = filterDto.Shift.EODNo;
            _tokenTypes = filterDto.PaymentType;
            _stationDetail = filterDto.Station;
            InitializeComponent();
        }

        private void OnReportLoaded(object sender, EventArgs e)
        {

            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationName", _stationDetail.StationName.Equals("Select All") ? "All" : _stationDetail.StationName));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("StationId", _stationDetail.StationId > 0 ? _stationDetail.StationId.ToString() : "All"));

            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ShiftNumber", _shiftNumber > 0 ? _shiftNumber.ToString() : "All"));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("AccountDate", _accountDate.ToString("dd/MM/yyyy")));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("TokenType", _tokenTypes.PayType.Equals("Select All") ? "All" : _tokenTypes.PayType));

            this._adapter.Fill(this._reportDT, _accountDate, _shiftNumber > 0 ? (int?)_shiftNumber : null, _eodNo > 0 ? (int?)_eodNo : null, _tokenTypes.PayTNo > 0 ? _tokenTypes.PayType : null, null, _stationDetail.StationId > 0 ? (int?)_stationDetail.StationId : (int?)null);
            ReportDataSource ds = new ReportDataSource("TokenTransactionDetails", _reportDT.Rows);
            this.reportViewer1.LocalReport.DataSources.Add(ds);
            this.reportViewer1.RefreshReport();
            this.panel1.Controls.Add(this.reportViewer1);
        }
    }
}
