﻿namespace DUC.ADNOC.HOAddIn.ToolbarCommands
{
    partial class STSystemAudit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dpAccountDateTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbStation = new System.Windows.Forms.ComboBox();
            this.btnShow = new System.Windows.Forms.Button();
            this.dpAccountDateFrom = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbToken = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbTerminalID = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.cmbLevel = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // dpAccountDateTo
            // 
            this.dpAccountDateTo.Checked = false;
            this.dpAccountDateTo.CustomFormat = "dd/MM/yyyy";
            this.dpAccountDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpAccountDateTo.Location = new System.Drawing.Point(315, 61);
            this.dpAccountDateTo.Name = "dpAccountDateTo";
            this.dpAccountDateTo.ShowCheckBox = true;
            this.dpAccountDateTo.Size = new System.Drawing.Size(128, 20);
            this.dpAccountDateTo.TabIndex = 58;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(257, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "To";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 56;
            this.label6.Text = "Station";
            // 
            // cmbStation
            // 
            this.cmbStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStation.FormattingEnabled = true;
            this.cmbStation.Location = new System.Drawing.Point(98, 20);
            this.cmbStation.Name = "cmbStation";
            this.cmbStation.Size = new System.Drawing.Size(128, 21);
            this.cmbStation.TabIndex = 55;
            this.cmbStation.SelectedIndexChanged += new System.EventHandler(this.OnStationChanged);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(184, 225);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(103, 27);
            this.btnShow.TabIndex = 54;
            this.btnShow.Text = "Show Report";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.OnExecuteClicked);
            // 
            // dpAccountDateFrom
            // 
            this.dpAccountDateFrom.Checked = false;
            this.dpAccountDateFrom.CustomFormat = "dd/MM/yyyy";
            this.dpAccountDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpAccountDateFrom.Location = new System.Drawing.Point(99, 61);
            this.dpAccountDateFrom.Name = "dpAccountDateFrom";
            this.dpAccountDateFrom.ShowCheckBox = true;
            this.dpAccountDateFrom.Size = new System.Drawing.Size(128, 20);
            this.dpAccountDateFrom.TabIndex = 53;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "From";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 62;
            this.label4.Text = "Device";
            // 
            // cmbToken
            // 
            this.cmbToken.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToken.FormattingEnabled = true;
            this.cmbToken.Location = new System.Drawing.Point(99, 97);
            this.cmbToken.Name = "cmbToken";
            this.cmbToken.Size = new System.Drawing.Size(128, 21);
            this.cmbToken.TabIndex = 61;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 64;
            this.label5.Text = "User";
            // 
            // cmbTerminalID
            // 
            this.cmbTerminalID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTerminalID.FormattingEnabled = true;
            this.cmbTerminalID.Location = new System.Drawing.Point(78, 32);
            this.cmbTerminalID.Name = "cmbTerminalID";
            this.cmbTerminalID.Size = new System.Drawing.Size(128, 21);
            this.cmbTerminalID.TabIndex = 63;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cmbTerminalID);
            this.groupBox1.Location = new System.Drawing.Point(20, 141);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(432, 78);
            this.groupBox1.TabIndex = 65;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Additional Filters";
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(257, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 67;
            this.label3.Text = "Level";
            // 
            // cmbLevel
            // 
            this.cmbLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLevel.FormattingEnabled = true;
            this.cmbLevel.Location = new System.Drawing.Point(315, 98);
            this.cmbLevel.Name = "cmbLevel";
            this.cmbLevel.Size = new System.Drawing.Size(128, 21);
            this.cmbLevel.TabIndex = 66;
            // 
            // STSystemAudit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbLevel);
            this.Controls.Add(this.dpAccountDateTo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbStation);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.cmbToken);
            this.Controls.Add(this.dpAccountDateFrom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "STSystemAudit";
            this.Size = new System.Drawing.Size(473, 265);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dpAccountDateTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbStation;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.DateTimePicker dpAccountDateFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbToken;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbTerminalID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbLevel;
    }
}
