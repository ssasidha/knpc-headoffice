﻿using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class STFuelDeliveryCommand : DUC.KNPC.HOAddIn.SupportToolbarControlBase
    {
        private bool _isStationLoading = false;
        public STFuelDeliveryCommand()
        {
            InitializeComponent();
            _isStationLoading = true;
            RefreshState();
            _isStationLoading = false;
            loadProduct();
        }

        protected override void RefreshState()
        {
            // Update Stations
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";
        }

        private void OnStationChanged(object sender, EventArgs e){
            if (!_isStationLoading)
            {
                loadProduct();
            }
        }

        private void loadProduct()
        {
            Repository rep = new Repository();
            var stationDetails = cmbStation.SelectedItem as StationDTO;
            StationData.Lookup_WetStockDataTable productDetails = rep.GetProducts(stationDetails.StationId);
            var row = productDetails.NewLookup_WetStockRow();
            row.grade = -1;
            row.shorttext = "Select All";
            productDetails.Rows.InsertAt(row, 0);
            cmbProduct.DataSource = productDetails;
            cmbProduct.DisplayMember = "shorttext";
            cmbProduct.ValueMember = "grade";
        }

        private void OnExecuteClicked(object sender, System.EventArgs e)
        {
            errorProvider.Clear();
            if (!dpAccountDateFrom.Checked)
            {
                errorProvider.SetError(dpAccountDateFrom, "Please select an from date");
                return;
            }
            if (!dpAccountDateTo.Checked)
            {
                errorProvider.SetError(dpAccountDateTo, "Please select an to date");
                return;
            }

            var selectedStation = cmbStation.SelectedItem as StationDTO;
            var filter  =  new FilterDTO();
            filter.Product = int.Parse(cmbProduct.SelectedValue.ToString());
            filter.FromDate = dpAccountDateFrom.Value;
            filter.ToDate = dpAccountDateTo.Value;
            filter.Station = selectedStation;
            var reportViewer = new Viewer.vwrFuelDelivery(filter);
            reportViewer.ShowDialog();
        }
    }
}
