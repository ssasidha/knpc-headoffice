﻿namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    partial class STOfflineTransactionCommand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnExecuteReport = new System.Windows.Forms.Button();
            this.cmbShiftNumber = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dpAccountDate = new System.Windows.Forms.DateTimePicker();
            this.eODGetAccountDateDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.cmbStation = new System.Windows.Forms.ComboBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbTokenType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.eODGetAccountDateDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExecuteReport
            // 
            this.btnExecuteReport.Location = new System.Drawing.Point(448, 42);
            this.btnExecuteReport.Name = "btnExecuteReport";
            this.btnExecuteReport.Size = new System.Drawing.Size(85, 28);
            this.btnExecuteReport.TabIndex = 17;
            this.btnExecuteReport.Text = "Show Report";
            this.btnExecuteReport.UseVisualStyleBackColor = true;
            this.btnExecuteReport.Click += new System.EventHandler(this.OnExecuteClicked);
            // 
            // cmbShiftNumber
            // 
            this.cmbShiftNumber.DisplayMember = "eodno";
            this.cmbShiftNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShiftNumber.FormattingEnabled = true;
            this.cmbShiftNumber.Location = new System.Drawing.Point(9, 101);
            this.cmbShiftNumber.Name = "cmbShiftNumber";
            this.cmbShiftNumber.Size = new System.Drawing.Size(182, 21);
            this.cmbShiftNumber.TabIndex = 14;
            this.cmbShiftNumber.ValueMember = "eodno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Shift Number:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(232, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Account Date:";
            // 
            // dpAccountDate
            // 
            this.dpAccountDate.Checked = false;
            this.dpAccountDate.CustomFormat = "dd/MM/yyyy";
            this.dpAccountDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpAccountDate.Location = new System.Drawing.Point(235, 48);
            this.dpAccountDate.Name = "dpAccountDate";
            this.dpAccountDate.Size = new System.Drawing.Size(124, 20);
            this.dpAccountDate.TabIndex = 11;
            this.dpAccountDate.ValueChanged += new System.EventHandler(this.OnAccountDateChanged);
            // 
            // eODGetAccountDateDetailsBindingSource
            // 
            this.eODGetAccountDateDetailsBindingSource.DataMember = "EOD_GetAccountDateDetails";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Station:";
            // 
            // cmbStation
            // 
            this.cmbStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStation.FormattingEnabled = true;
            this.cmbStation.Location = new System.Drawing.Point(9, 47);
            this.cmbStation.Name = "cmbStation";
            this.cmbStation.Size = new System.Drawing.Size(182, 21);
            this.cmbStation.TabIndex = 38;
            this.cmbStation.SelectedIndexChanged += new System.EventHandler(this.OnAccountDateChanged);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbTokenType);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cmbShiftNumber);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.btnExecuteReport);
            this.groupBox2.Controls.Add(this.cmbStation);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dpAccountDate);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(573, 157);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Offline Transaction Report";
            // 
            // cmbTokenType
            // 
            this.cmbTokenType.DisplayMember = "eodno";
            this.cmbTokenType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTokenType.FormattingEnabled = true;
            this.cmbTokenType.Location = new System.Drawing.Point(235, 101);
            this.cmbTokenType.Name = "cmbTokenType";
            this.cmbTokenType.Size = new System.Drawing.Size(182, 21);
            this.cmbTokenType.TabIndex = 41;
            this.cmbTokenType.ValueMember = "eodno";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(232, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Method Of Payment:";
            // 
            // STOfflineTransactionCommand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox2);
            this.Name = "STOfflineTransactionCommand";
            this.Size = new System.Drawing.Size(573, 157);
            ((System.ComponentModel.ISupportInitialize)(this.eODGetAccountDateDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExecuteReport;
        private System.Windows.Forms.BindingSource eODGetAccountDateDetailsBindingSource;
        private System.Windows.Forms.ComboBox cmbShiftNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dpAccountDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbStation;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbTokenType;
        private System.Windows.Forms.Label label3;
    }
}
