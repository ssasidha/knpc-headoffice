﻿using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public class STStationManagerSafeDropCommand : DUC.KNPC.HOAddIn.SupportToolbarControlBase
    {
        #region "Designer"

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbShiftNumber;
        private System.Windows.Forms.BindingSource eODGetAccountDateDetailsBindingSource;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.ComboBox cmbStation;
        private System.Windows.Forms.Button btnExecuteReport;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dpAccountDate;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.GroupBox groupBox2;

        public STStationManagerSafeDropCommand()
        {
            InitializeComponent();
            dpAccountDate.Value = DateTime.Today;
            RefreshState();
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbShiftNumber = new System.Windows.Forms.ComboBox();
            this.eODGetAccountDateDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbStation = new System.Windows.Forms.ComboBox();
            this.btnExecuteReport = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dpAccountDate = new System.Windows.Forms.DateTimePicker();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.eODGetAccountDateDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 42;
            this.label2.Text = "Shift Number:";
            // 
            // cmbShiftNumber
            // 
            this.cmbShiftNumber.DisplayMember = "eodno";
            this.cmbShiftNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShiftNumber.FormattingEnabled = true;
            this.cmbShiftNumber.Location = new System.Drawing.Point(6, 97);
            this.cmbShiftNumber.Name = "cmbShiftNumber";
            this.cmbShiftNumber.Size = new System.Drawing.Size(182, 21);
            this.cmbShiftNumber.TabIndex = 43;
            this.cmbShiftNumber.ValueMember = "eodno";
            // 
            // eODGetAccountDateDetailsBindingSource
            // 
            this.eODGetAccountDateDetailsBindingSource.DataMember = "EOD_GetAccountDateDetails";
            // 
            // cmbStation
            // 
            this.cmbStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStation.FormattingEnabled = true;
            this.cmbStation.Location = new System.Drawing.Point(6, 43);
            this.cmbStation.Name = "cmbStation";
            this.cmbStation.Size = new System.Drawing.Size(182, 21);
            this.cmbStation.TabIndex = 47;
            this.cmbStation.SelectedIndexChanged += new System.EventHandler(this.OnStationChanged);
            // 
            // btnExecuteReport
            // 
            this.btnExecuteReport.Location = new System.Drawing.Point(373, 42);
            this.btnExecuteReport.Name = "btnExecuteReport";
            this.btnExecuteReport.Size = new System.Drawing.Size(101, 24);
            this.btnExecuteReport.TabIndex = 46;
            this.btnExecuteReport.Text = "Show Report";
            this.btnExecuteReport.UseVisualStyleBackColor = true;
            this.btnExecuteReport.Click += new System.EventHandler(this.btnExecuteReport_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 48;
            this.label6.Text = "Station:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(216, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "Account Date:";
            // 
            // dpAccountDate
            // 
            this.dpAccountDate.Checked = false;
            this.dpAccountDate.CustomFormat = "dd/MM/yyyy";
            this.dpAccountDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpAccountDate.Location = new System.Drawing.Point(219, 44);
            this.dpAccountDate.Name = "dpAccountDate";
            this.dpAccountDate.Size = new System.Drawing.Size(131, 20);
            this.dpAccountDate.TabIndex = 40;
            this.dpAccountDate.ValueChanged += new System.EventHandler(this.OnStationChanged);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cmbShiftNumber);
            this.groupBox2.Controls.Add(this.cmbStation);
            this.groupBox2.Controls.Add(this.btnExecuteReport);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dpAccountDate);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(499, 145);
            this.groupBox2.TabIndex = 51;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Station Manager Safe Drop Report";
            // 
            // STStationManagerSafeDropCommand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox2);
            this.Name = "STStationManagerSafeDropCommand";
            this.Size = new System.Drawing.Size(499, 145);
            ((System.ComponentModel.ISupportInitialize)(this.eODGetAccountDateDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        } 

        #endregion

        private void btnExecuteReport_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();

            if (!(cmbStation.SelectedIndex > 0))
            {
                errorProvider.SetError(cmbStation, "Please select a station");
                return;
            }
            if (!dpAccountDate.Checked)
            {
                errorProvider.SetError(dpAccountDate, "Please select an account date");
                return;
            }
            if (!(cmbShiftNumber.SelectedIndex > 0))
            {
                errorProvider.SetError(cmbShiftNumber, "Please select a shift number");
                return;
            }
            var selectedStation = cmbStation.SelectedItem as StationDTO;
            var selectedShift = cmbShiftNumber.SelectedItem as ShiftDetailsDTO;

            if (selectedShift.EODNo == 0)
            {
                errorProvider.SetError(btnExecuteReport, string.Format("Shift \"{0}\" is not closed.", selectedShift.ShiftNo));
                return;
            }

            var filter = new FilterDTO();
            filter.AccountDate = dpAccountDate.Value;
            filter.Station = selectedStation;
            filter.Shift = selectedShift;

            var reportViewer = new Viewer.vwrSupervisorSafeDrop(filter);
            reportViewer.ShowDialog();
        }

        protected override void RefreshState()
        {
            // Update Stations
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";

            PopulateShifts();
        }

        private void OnStationChanged(object sender, EventArgs e)
        {
            if (cmbStation.SelectedIndex > 0 & dpAccountDate.Checked)
            {

                PopulateShifts();
            }
            else
            {

                PopulateEmptyShifts();
            }
        }


        private void PopulateEmptyShifts()
        {
            try
            {
                cmbShiftNumber.DataSource = this.GetEmptyShiftDetails();
                cmbShiftNumber.DisplayMember = "ShiftName";
                cmbShiftNumber.ValueMember = "EODNo";
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbShiftNumber, exception.Message);
            }
        }

        private void PopulateShifts()
        {
            int? selectedStation = null;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = (int?)int.Parse(cmbStation.SelectedValue.ToString());
            cmbShiftNumber.DataSource = this.GetShiftDetails(dpAccountDate.Value, selectedStation);
            cmbShiftNumber.DisplayMember = "ShiftName";
            cmbShiftNumber.ValueMember = "EODNo";
        }
    }
}
