﻿namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    partial class STTokenAuthorizationFailureCommand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbProduct = new System.Windows.Forms.ComboBox();
            this.dpAccountDateTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbStation = new System.Windows.Forms.ComboBox();
            this.btnShow = new System.Windows.Forms.Button();
            this.dpAccountDateFrom = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbToken = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbTerminalID = new System.Windows.Forms.ComboBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 60;
            this.label3.Text = "Product/Grade";
            // 
            // cmbProduct
            // 
            this.cmbProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProduct.FormattingEnabled = true;
            this.cmbProduct.Location = new System.Drawing.Point(10, 154);
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.Size = new System.Drawing.Size(182, 21);
            this.cmbProduct.TabIndex = 59;
            // 
            // dpAccountDateTo
            // 
            this.dpAccountDateTo.Checked = false;
            this.dpAccountDateTo.CustomFormat = "dd/MM/yyyy";
            this.dpAccountDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpAccountDateTo.Location = new System.Drawing.Point(240, 105);
            this.dpAccountDateTo.Name = "dpAccountDateTo";
            this.dpAccountDateTo.Size = new System.Drawing.Size(182, 20);
            this.dpAccountDateTo.TabIndex = 58;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(237, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "To Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 56;
            this.label6.Text = "Station";
            // 
            // cmbStation
            // 
            this.cmbStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStation.FormattingEnabled = true;
            this.cmbStation.Location = new System.Drawing.Point(10, 49);
            this.cmbStation.Name = "cmbStation";
            this.cmbStation.Size = new System.Drawing.Size(182, 21);
            this.cmbStation.TabIndex = 55;
            this.cmbStation.SelectedIndexChanged += new System.EventHandler(this.OnStationChanged);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(449, 203);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(103, 27);
            this.btnShow.TabIndex = 54;
            this.btnShow.Text = "Show Report";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.OnExecuteClicked);
            // 
            // dpAccountDateFrom
            // 
            this.dpAccountDateFrom.Checked = false;
            this.dpAccountDateFrom.CustomFormat = "dd/MM/yyyy";
            this.dpAccountDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpAccountDateFrom.Location = new System.Drawing.Point(10, 105);
            this.dpAccountDateFrom.Name = "dpAccountDateFrom";
            this.dpAccountDateFrom.Size = new System.Drawing.Size(182, 20);
            this.dpAccountDateFrom.TabIndex = 53;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "From Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 191);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 62;
            this.label4.Text = "Method Of Payment";
            // 
            // cmbToken
            // 
            this.cmbToken.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToken.FormattingEnabled = true;
            this.cmbToken.Location = new System.Drawing.Point(9, 207);
            this.cmbToken.Name = "cmbToken";
            this.cmbToken.Size = new System.Drawing.Size(182, 21);
            this.cmbToken.TabIndex = 61;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(237, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 64;
            this.label5.Text = "Terminal ID";
            // 
            // cmbTerminalID
            // 
            this.cmbTerminalID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTerminalID.FormattingEnabled = true;
            this.cmbTerminalID.Location = new System.Drawing.Point(240, 207);
            this.cmbTerminalID.Name = "cmbTerminalID";
            this.cmbTerminalID.Size = new System.Drawing.Size(182, 21);
            this.cmbTerminalID.TabIndex = 63;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbTerminalID);
            this.groupBox2.Controls.Add(this.btnShow);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cmbToken);
            this.groupBox2.Controls.Add(this.cmbProduct);
            this.groupBox2.Controls.Add(this.dpAccountDateTo);
            this.groupBox2.Controls.Add(this.cmbStation);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dpAccountDateFrom);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(617, 244);
            this.groupBox2.TabIndex = 66;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Token Failure Report";
            // 
            // STTokenAuthorizationFailureCommand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox2);
            this.Name = "STTokenAuthorizationFailureCommand";
            this.Size = new System.Drawing.Size(617, 244);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbProduct;
        private System.Windows.Forms.DateTimePicker dpAccountDateTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbStation;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.DateTimePicker dpAccountDateFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbToken;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbTerminalID;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}
