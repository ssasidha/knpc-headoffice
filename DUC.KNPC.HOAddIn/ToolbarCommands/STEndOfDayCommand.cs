﻿using System;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class STEndOfDayCommand : DUC.KNPC.HOAddIn.SupportToolbarControlBase
    {
        public STEndOfDayCommand()
        {
            InitializeComponent();
            dpAccountDate.Value = DateTime.Today;
            RefreshState();
          
        }

        #region "Overrides"

        protected override void RefreshState()
        {
            // Update Stations
            cmbStation.DataSource = this.GetAllStationsWithoutSelectAll();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";
        } 

        #endregion

        #region "Event Hanlders"

        private void OnExecuteClicked(object sender, System.EventArgs e)
        {
            #region validation
            errorProvider.Clear();
            if (cmbStation.SelectedIndex == 0)
            {
                errorProvider.SetError(cmbStation, "Please select a station");
                return;
            }

            if (!dpAccountDate.Checked)
            {
                errorProvider.SetError(dpAccountDate, "Please select an account date");
                return;
            }

            #endregion

            try
            {
                var selectedStation = cmbStation.SelectedItem as StationDTO;
                var filter = new FilterDTO();
                filter.Station = selectedStation;
                filter.AccountDate = dpAccountDate.Value;
                var reportViewer = new Viewer.vwrEndOfDay(filter);
                reportViewer.ShowDialog();
            }
            catch (Exception exception)
            {

                errorProvider.SetError(btnShow, exception.Message);
            }
        }  

        #endregion

        private void cmbStation_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
