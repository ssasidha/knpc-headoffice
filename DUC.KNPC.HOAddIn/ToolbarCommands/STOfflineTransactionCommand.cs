﻿using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class STOfflineTransactionCommand : DUC.KNPC.HOAddIn.SupportToolbarControlBase
    {
        public STOfflineTransactionCommand()
        {
            InitializeComponent();
            dpAccountDate.Value = DateTime.Today;
            RefreshState();
            //groupBox1.Enabled = false;
        }

        #region "Overrides"

        protected override void RefreshState()
        {
            // Update Stations
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";

            PopulateShifts();
            PopulatePayTypes();
        }

        #endregion

        #region "Event Handlers"

        private void OnExecuteClicked(object sender, EventArgs e)
        {
            errorProvider.Clear();

            if (!dpAccountDate.Checked)
            {
                errorProvider.SetError(dpAccountDate, "Please select an account date");
                return;
            }

            var selectedStation = cmbStation.SelectedItem as StationDTO;
            var selectedShift = cmbShiftNumber.SelectedItem as ShiftDetailsDTO;
            var tokenTypes = cmbTokenType.SelectedItem as PaymentTypeDTO;

            FilterDTO filter = new FilterDTO();
            filter.Station = selectedStation;
            filter.Shift = selectedShift;
            filter.PaymentType = tokenTypes;
            filter.AccountDate = dpAccountDate.Value;
            var reportViewer = new Viewer.vwrOfflineTransactions(filter);
            reportViewer.ShowDialog();
        }

        private void OnAccountDateChanged(object sender, EventArgs e)
        {
            if (cmbStation.SelectedIndex > 0 & dpAccountDate.Checked)
            {
                //groupBox1.Enabled = true;
                PopulateShifts();
            }
            else
            {
                //groupBox1.Enabled = false;
                PopulateEmptyShifts();
            }
        }

        private void PopulateEmptyShifts()
        {
            try
            {
                cmbShiftNumber.DataSource = this.GetEmptyShiftDetails();
                cmbShiftNumber.DisplayMember = "ShiftName";
                cmbShiftNumber.ValueMember = "EODNo";
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbShiftNumber, exception.Message);
            }
        }
        #endregion

        #region "Private Methods"

        private void PopulateShifts()
        {
            int? selectedStation = null;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = (int?)int.Parse(cmbStation.SelectedValue.ToString());
            cmbShiftNumber.DataSource = this.GetShiftDetails(dpAccountDate.Value, selectedStation);
            cmbShiftNumber.DisplayMember = "ShiftName";
            cmbShiftNumber.ValueMember = "EODNo";
        }

        private void PopulatePayTypes()
        {
            int selectedStation = -1;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = int.Parse(cmbStation.SelectedValue.ToString());

            cmbTokenType.DataSource = this.GetAllOnlineTokenTypes(selectedStation);
            cmbTokenType.DisplayMember = "PayType";
            cmbTokenType.ValueMember = "PayTNo";

        }

        #endregion

    }
}
