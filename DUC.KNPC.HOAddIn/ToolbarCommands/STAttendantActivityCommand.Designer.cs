﻿namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    partial class STAttendantActivityCommand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnShow = new System.Windows.Forms.Button();
            this.dpAccountDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.eODGetAccountDateDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.cmbStation = new System.Windows.Forms.ComboBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbAttendantId = new System.Windows.Forms.ComboBox();
            this.cmbAttendant = new System.Windows.Forms.ComboBox();
            this.cmbShiftNumber = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.eODGetAccountDateDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(437, 144);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(99, 27);
            this.btnShow.TabIndex = 33;
            this.btnShow.Text = "Show Report";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.OnExecuteClicked);
            // 
            // dpAccountDate
            // 
            this.dpAccountDate.Checked = false;
            this.dpAccountDate.CustomFormat = "dd/MM/yyyy";
            this.dpAccountDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpAccountDate.Location = new System.Drawing.Point(217, 43);
            this.dpAccountDate.Name = "dpAccountDate";
            this.dpAccountDate.Size = new System.Drawing.Size(126, 20);
            this.dpAccountDate.TabIndex = 32;
            this.dpAccountDate.ValueChanged += new System.EventHandler(this.OnAccountDateChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(214, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Account Date:";
            // 
            // eODGetAccountDateDetailsBindingSource
            // 
            this.eODGetAccountDateDetailsBindingSource.DataMember = "EOD_GetAccountDateDetails";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Station:";
            // 
            // cmbStation
            // 
            this.cmbStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStation.FormattingEnabled = true;
            this.cmbStation.Location = new System.Drawing.Point(6, 42);
            this.cmbStation.Name = "cmbStation";
            this.cmbStation.Size = new System.Drawing.Size(182, 21);
            this.cmbStation.TabIndex = 36;
            this.cmbStation.SelectedIndexChanged += new System.EventHandler(this.OnStationChanged);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(214, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 37;
            this.label4.Text = "Attendant Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 36;
            this.label3.Text = "Shift Number:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Attendant ID:";
            // 
            // cmbAttendantId
            // 
            this.cmbAttendantId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAttendantId.FormattingEnabled = true;
            this.cmbAttendantId.Location = new System.Drawing.Point(6, 148);
            this.cmbAttendantId.Name = "cmbAttendantId";
            this.cmbAttendantId.Size = new System.Drawing.Size(182, 21);
            this.cmbAttendantId.TabIndex = 34;
            // 
            // cmbAttendant
            // 
            this.cmbAttendant.FormattingEnabled = true;
            this.cmbAttendant.Location = new System.Drawing.Point(217, 148);
            this.cmbAttendant.Name = "cmbAttendant";
            this.cmbAttendant.Size = new System.Drawing.Size(182, 21);
            this.cmbAttendant.TabIndex = 33;
            // 
            // cmbShiftNumber
            // 
            this.cmbShiftNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShiftNumber.FormattingEnabled = true;
            this.cmbShiftNumber.Location = new System.Drawing.Point(6, 94);
            this.cmbShiftNumber.Name = "cmbShiftNumber";
            this.cmbShiftNumber.Size = new System.Drawing.Size(182, 21);
            this.cmbShiftNumber.TabIndex = 32;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbAttendant);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnShow);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmbAttendantId);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cmbStation);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.cmbShiftNumber);
            this.groupBox2.Controls.Add(this.dpAccountDate);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(575, 197);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Attendant Activity Report";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // STAttendantActivityCommand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox2);
            this.Name = "STAttendantActivityCommand";
            this.Size = new System.Drawing.Size(575, 197);
            this.Load += new System.EventHandler(this.STAttendantActivityCommand_Load);
            ((System.ComponentModel.ISupportInitialize)(this.eODGetAccountDateDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.DateTimePicker dpAccountDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource eODGetAccountDateDetailsBindingSource;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbStation;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbAttendantId;
        private System.Windows.Forms.ComboBox cmbAttendant;
        private System.Windows.Forms.ComboBox cmbShiftNumber;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}
