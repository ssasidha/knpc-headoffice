﻿using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class STDriveoffCommand : DUC.KNPC.HOAddIn.SupportToolbarControlBase
    {
        public STDriveoffCommand()
        {
            InitializeComponent();
            dpAccountDate.Value = DateTime.Today;
            RefreshState();
        }

        protected override void RefreshState()
        {
            // Update Stations
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";

            PopulateShifts();
        }

        private void OnStationChanged(object sender, EventArgs e)
        {
            if (cmbStation.SelectedIndex > 0 & dpAccountDate.Checked)
            {
               // groupBox1.Enabled = true;
                PopulateShifts();
            }
            else
            {
                //groupBox1.Enabled = false;
                PopulateEmptyShifts();
            }
        }

        private void PopulateEmptyShifts()
        {
            try
            {
                cmbShiftNumber.DataSource = this.GetEmptyShiftDetails();
                cmbShiftNumber.DisplayMember = "ShiftName";
                cmbShiftNumber.ValueMember = "EODNo";
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbShiftNumber, exception.Message);
            }
        }

        private void PopulateShifts()
        {
            int? selectedStation = null;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = (int?)int.Parse(cmbStation.SelectedValue.ToString());
            cmbShiftNumber.DataSource = this.GetShiftDetails(dpAccountDate.Value, selectedStation);
            cmbShiftNumber.DisplayMember = "ShiftName";
            cmbShiftNumber.ValueMember = "EODNo";
        }

        private void btnExecuteReport_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();

            #region Validation
            if (!dpAccountDate.Checked)
            {
                errorProvider.SetError(dpAccountDate, "Please select an account date");
                return;
            }
            #endregion

            var selectedStation = cmbStation.SelectedItem as StationDTO;
            var selectedShift = cmbShiftNumber.SelectedItem as ShiftDetailsDTO;

            var searchFilter = new FilterDTO();
            searchFilter.AccountDate = dpAccountDate.Value;
            searchFilter.Shift = selectedShift;
            searchFilter.Station = selectedStation;

            var reportViewer = new Viewer.vwrDriveOff(dpAccountDate.Value, selectedShift, selectedStation);
            reportViewer.ShowDialog();
        }
    }
}
