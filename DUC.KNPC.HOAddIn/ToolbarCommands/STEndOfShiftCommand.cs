﻿using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class STEndOfShiftCommand : DUC.KNPC.HOAddIn.SupportToolbarControlBase
    {
        public STEndOfShiftCommand()
        {
            InitializeComponent();
            dpAccountDate.Value = DateTime.Today;
            RefreshState();
          
        }

        private void btnExecuteReport_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();

            if (!(cmbStation.SelectedIndex > 0))
            {
                errorProvider.SetError(cmbStation, "Please select a station");
                return;
            }
            if (!dpAccountDate.Checked)
            {
                errorProvider.SetError(dpAccountDate, "Please select an account date");
                return;
            }
            if (!(cmbShiftNumber.SelectedIndex > 0))
            {
                errorProvider.SetError(cmbShiftNumber, "Please select a shift number");
                return;
            }
            var selectedStation = cmbStation.SelectedItem as StationDTO;
            var selectedShift = cmbShiftNumber.SelectedItem as ShiftDetailsDTO;

            if (selectedShift.EODNo == 0)
            {
                errorProvider.SetError(btnExecuteReport, string.Format("Shift \"{0}\" is not closed.", selectedShift.ShiftNo));
                return;
            }

            var filter = new FilterDTO();
            filter.AccountDate = dpAccountDate.Value;
            filter.Station = selectedStation;
            filter.Shift = selectedShift;

            var reportViewer = new Viewer.vwrEndOfShift(filter);
            reportViewer.ShowDialog();
        }

        protected override void RefreshState()
        {
            // Update Stations
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";

            PopulateShifts();
        }

        private void OnStationChanged(object sender, EventArgs e)
        {
            if (cmbStation.SelectedIndex > 0 & dpAccountDate.Checked)
            {
             
                PopulateShifts();
            }
            else
            {
               
                PopulateEmptyShifts();
            }
        }


        private void PopulateEmptyShifts()
        {
            try
            {
                cmbShiftNumber.DataSource = this.GetEmptyShiftDetails();
                cmbShiftNumber.DisplayMember = "ShiftName";
                cmbShiftNumber.ValueMember = "EODNo";
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbShiftNumber, exception.Message);
            }
        }

        private void PopulateShifts()
        {
            int? selectedStation = null;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = (int?)int.Parse(cmbStation.SelectedValue.ToString());
            cmbShiftNumber.DataSource = this.GetShiftDetails(dpAccountDate.Value, selectedStation);
            cmbShiftNumber.DisplayMember = "ShiftName";
            cmbShiftNumber.ValueMember = "EODNo";
        }

        private void STDetailedSafeDropCommand_Load(object sender, EventArgs e)
        {

        }

        private void cmbShiftNumber_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
