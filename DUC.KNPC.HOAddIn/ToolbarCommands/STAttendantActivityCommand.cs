﻿
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class STAttendantActivityCommand : SupportToolbarControlBase
    {
        private bool _isAttendantChanging;

        public STAttendantActivityCommand()
        {
            InitializeComponent();
            dpAccountDate.Value = DateTime.Today;
            _isAttendantChanging = false;
        }

        #region "Overrides"

        protected override void RefreshState()
        {
            try
            {
                errorProvider.Clear();
                cmbStation.DataSource = this.GetAllStations();
                cmbStation.ValueMember = "StationId";
                cmbStation.DisplayMember = "StationName";
                dpAccountDate.Checked = false;
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbStation, exception.Message);
            }
            // Update Stations

        }

        #endregion

        #region "Event Handlers"

        private void OnStationChanged(object sender, System.EventArgs e)
        {
            if ( (cmbStation.SelectedIndex > 0) & (dpAccountDate.Checked))
            {
                //groupBox1.Enabled = true;
                PopulateShifts();
                PopulateAttendants();
            }
            else
            {
                //groupBox1.Enabled = false;
                PopulateEmptyShifts();
                PopulateEmptyAttendants();
            }
        }

        private void OnAccountDateChanged(object sender, System.EventArgs e)
        {
            if ((cmbStation.SelectedIndex > 0) & (dpAccountDate.Checked))
            {
                //groupBox1.Enabled = true;
                PopulateShifts();
                PopulateAttendants();
            }
            else
            {
                //groupBox1.Enabled = false;
                PopulateEmptyShifts();
                PopulateEmptyAttendants();
            }
        }

        private void OnExecuteClicked(object sender, System.EventArgs e)
        {
            try
            {
                errorProvider.Clear();
                #region Validation
                if (!dpAccountDate.Checked)
                {
                    errorProvider.SetError(dpAccountDate, "Please select an account date");
                    return;
                }
                #endregion

                var attendant = cmbAttendant.SelectedItem as AttendantDetailsDTO;
                var selectedStation = cmbStation.SelectedItem as StationDTO;
                var selectedShiftno = cmbShiftNumber.SelectedItem as ShiftDetailsDTO;
                var searchFilter = new FilterDTO();

                searchFilter.AccountDate = dpAccountDate.Value;
                searchFilter.Station = selectedStation;
                searchFilter.Shift = selectedShiftno;
                searchFilter.Attendant = attendant;

                var reportViewer = new Viewer.vwrAttendantActivity(searchFilter);
                reportViewer.ShowDialog();
            }
            catch (Exception exception)
            {
                errorProvider.SetError(btnShow, exception.Message);
            }
        }

        #endregion



        private void PopulateShifts()
        {
            try
            {
                errorProvider.Clear();
                int? selectedStation = null;
                if (cmbStation.SelectedIndex > 0)
                    selectedStation = (int?) int.Parse(cmbStation.SelectedValue.ToString());
                cmbShiftNumber.DataSource = this.GetShiftDetails(dpAccountDate.Value, selectedStation);
                cmbShiftNumber.DisplayMember = "ShiftName";
                cmbShiftNumber.ValueMember = "EODNo";
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbShiftNumber, exception.Message);
            }
        }

        private void PopulateEmptyShifts()
        {
            try
            {
                cmbShiftNumber.DataSource = this.GetEmptyShiftDetails();
                cmbShiftNumber.DisplayMember = "ShiftName";
                cmbShiftNumber.ValueMember = "EODNo";
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbShiftNumber, exception.Message);
            }
        }
        private void PopulateEmptyAttendants()
        {
            try
            {
                var attendants = this.GetEmptyAttendants();
                cmbAttendant.DataSource = attendants;
                cmbAttendant.DisplayMember = "Name";
                cmbAttendant.ValueMember = "Id";

                cmbAttendantId.DataSource = attendants;
                cmbAttendantId.DisplayMember = "IdDisplay";
                cmbAttendantId.ValueMember = "Id";
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbShiftNumber, exception.Message);
            }
        }

        private void PopulateAttendants()
        {
            try
            {
                errorProvider.Clear();
                _isAttendantChanging = true;
                int selectedStation = -1;
                if (cmbStation.SelectedIndex > 0)
                    selectedStation = int.Parse(cmbStation.SelectedValue.ToString());

                var attendants = GetAttendants(selectedStation);
                cmbAttendant.DataSource = attendants;
                cmbAttendant.DisplayMember = "Name";
                cmbAttendant.ValueMember = "Id";

                cmbAttendantId.DataSource = attendants;
                cmbAttendantId.DisplayMember = "IdDisplay";
                cmbAttendantId.ValueMember = "Id";

                _isAttendantChanging = false;
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbAttendant, exception.Message);
            }
        }

        private void STAttendantActivityCommand_Load(object sender, EventArgs e)
        {
            RefreshState();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
