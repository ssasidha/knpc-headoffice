﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class STCumulitiveStockCommand : SupportToolbarControlBase
    {
        public STCumulitiveStockCommand()
        {
            InitializeComponent();
            RefreshState();
        }

        protected override void RefreshState()
        {
            // Update Stations
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";

        }

        private void btnExecuteReport_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            #region Validation
            if (Convert.ToInt32(cmbStation.SelectedValue.ToString()) == -1)
            {
                errorProvider1.SetError(cmbStation, "Please select a station");
                return;
            }
            else if (!dpFromDate.Checked)
            {
                errorProvider1.SetError(dpFromDate, "Please select a date");
                return;
            }
            else if (!dpToDate.Checked)
            {
                errorProvider1.SetError(dpToDate, "Please select a date");
                return;
            }
            #endregion

            var selectedStation = cmbStation.SelectedItem as StationDTO;
            var searchFilter = new FilterDTO();
            searchFilter.FromDate = dpFromDate.Value;
            searchFilter.ToDate = dpToDate.Value;
            searchFilter.Station = selectedStation;

            var reportViewer = new Viewer.vwrCumulativeStock(searchFilter);
            reportViewer.ShowDialog();
        }
    }
}
