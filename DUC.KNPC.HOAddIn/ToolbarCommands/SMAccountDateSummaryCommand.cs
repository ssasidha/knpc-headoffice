﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class SMAccountDateSummaryCommand : DUC.KNPC.HOAddIn.SupportToolbarControlBase
    {
        public SMAccountDateSummaryCommand()
        {
            InitializeComponent();
            dpAccountDate.Value = DateTime.Today;
            RefreshState();
        }

        private void btnExecuteReport_Click(object sender, EventArgs e)
        {
            if (!dpAccountDate.Checked)
            {
                this.ShowValidationError("Please select an account Date", dpAccountDate);
                return;
            }

            var selectedStation = cmbStation.SelectedItem as StationDTO;

            FilterDTO filter = new FilterDTO();
            filter.Station = selectedStation;
            filter.AccountDate = dpAccountDate.Value;

            var reportViewer = new Viewer.vwrAccountDateSummary(filter);
            reportViewer.ShowDialog();
        }

        protected override void RefreshState()
        {
            // Update Stations
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";         
        }
    }
}
