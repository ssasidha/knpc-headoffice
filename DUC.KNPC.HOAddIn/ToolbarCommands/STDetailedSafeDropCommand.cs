﻿using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class STDetailedSafeDropCommand : DUC.KNPC.HOAddIn.SupportToolbarControlBase
    {
        public STDetailedSafeDropCommand()
        {
            InitializeComponent();
            dpAccountDate.Value = DateTime.Today;
        }

        private void btnExecuteReport_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();
            #region Validation
            if (!dpAccountDate.Checked)
            {
                errorProvider.SetError(dpAccountDate, "Please select an account date");
                return;
            }
            #endregion

            try
            {
                var filter = new FilterDTO();
                filter.AccountDate = dpAccountDate.Value.Date;
                filter.Station = cmbStation.SelectedItem as StationDTO;
                filter.Shift = cmbShiftNumber.SelectedItem as ShiftDetailsDTO;

                var reportViewer = new Viewer.vwrDetailedSafeDrop(filter);
                reportViewer.ShowDialog();

            }
            catch (Exception exception)
            {
                errorProvider.SetError(btnExecuteReport, exception.Message);

            }
        }

        protected override void RefreshState()
        {
            // Update Stations
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";

        }

        private void OnStationChanged(object sender, EventArgs e)
        {
            if ((cmbStation.SelectedIndex > 0) & (dpAccountDate.Checked))
            {
                //groupBox1.Enabled = true;
                PopulateShifts();
            }
            else
            {
                //groupBox1.Enabled = false;
                PopulateEmptyShifts();
            }
        }

        private void PopulateShifts()
        {
            try
            {
                int? selectedStation = null;
                if (cmbStation.SelectedIndex > 0)
                    selectedStation = (int?) int.Parse(cmbStation.SelectedValue.ToString());
                cmbShiftNumber.DataSource = this.GetShiftDetails(dpAccountDate.Value, selectedStation);
                cmbShiftNumber.DisplayMember = "ShiftName";
                cmbShiftNumber.ValueMember = "EODNo";
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbShiftNumber, exception.Message);
            }
        }

        private void PopulateEmptyShifts()
        {
            try
            {
                cmbShiftNumber.DataSource = this.GetEmptyShiftDetails();
                cmbShiftNumber.DisplayMember = "ShiftName";
                cmbShiftNumber.ValueMember = "EODNo";
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbShiftNumber, exception.Message);
            }
        }



        private void STDetailedSafeDropCommand_Load(object sender, EventArgs e)
        {
            RefreshState();
        }
    }
}
