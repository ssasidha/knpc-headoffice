﻿using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class STTokenAuthorizationFailureCommand : DUC.KNPC.HOAddIn.SupportToolbarControlBase
    {
        public STTokenAuthorizationFailureCommand()
        {
            InitializeComponent();
            dpAccountDateFrom.Value = DateTime.Today;
            dpAccountDateTo.Value = DateTime.Today;
            RefreshState();
            //groupBox1.Enabled = false;
        }

        protected override void RefreshState()
        {
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";
            PopulatePayTypes();
            LoadProductGroups();
            LoadTerminals();
        }

        private void OnExecuteClicked(object sender, EventArgs e)
        {
            
            errorProvider.Clear();

            if (!(cmbStation.SelectedIndex > 0))
            {
                errorProvider.SetError(cmbStation, "Please select a station");
                return;
            }

            if (!dpAccountDateFrom.Checked)
            {
                errorProvider.SetError(dpAccountDateFrom, "Please select an from Date");
                return;
            }

            if (!dpAccountDateTo.Checked)
            {
                errorProvider.SetError(dpAccountDateTo, "Please select an to Date");
                return;
            }

            var selectedStation = cmbStation.SelectedItem as StationDTO;
            var terminalID = cmbTerminalID.SelectedValue;
            var token = cmbToken.SelectedItem as PaymentTypeDTO;
            var product = cmbProduct.SelectedItem as ProductGroupDTO;

            FilterDTO filter = new FilterDTO();
            filter.Station = selectedStation;
            filter.PaymentType = token;
            filter.ProductType = product;
            filter.TerminalID = terminalID.ToString();
            filter.FromDate = dpAccountDateFrom.Value;
            filter.ToDate = dpAccountDateTo.Value;

            var reportViewer = new Viewer.vwrAuthorizationFailure(filter);
            reportViewer.ShowDialog();
        }

        private void OnStationChanged(object sender, EventArgs e)
        {
            if (cmbStation.SelectedIndex > 0)
            {
                //groupBox1.Enabled = true;
                PopulatePayTypes();
                LoadProductGroups();
                LoadTerminals();
            }
            else
            {
                //groupBox1.Enabled = false;
            }
        }

        private void PopulatePayTypes()
        {
            int selectedStation = -1;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = int.Parse(cmbStation.SelectedValue.ToString());
            cmbToken.DataSource = this.GetAllOnlineTokenTypes(selectedStation);
            cmbToken.DisplayMember = "PayType";
            cmbToken.ValueMember = "PayTNo";

        }

        private void LoadProductGroups()
        {
            var groups = new List<ProductGroupDTO>
            {
                new ProductGroupDTO
                {
                    GroupName = "Select All"
                },
                new ProductGroupDTO
                {
                    GroupName = "Fuel(Prepaid)"
                },
                new ProductGroupDTO
                {
                    GroupName = "Fuel(Postpaid Rahal)"
                },
                new ProductGroupDTO
                {
                    GroupName = "C-Store/Car Wash/Lube Change"
                }
            };

            cmbProduct.DataSource = groups;
            cmbProduct.DisplayMember = cmbProduct.ValueMember = "GroupName";
        }

        private void LoadTerminals()
        {
            Repository rep = new Repository();
            int selectedStation = -1;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = int.Parse(cmbStation.SelectedValue.ToString());
            StationData.v_TerminalIDDataTable terminalDetails = rep.GetAllTerminals(selectedStation);
            var row = terminalDetails.Newv_TerminalIDRow();
            row.eft_termid = "Select All";
            terminalDetails.Rows.InsertAt(row, 0);
            cmbTerminalID.DataSource = terminalDetails;
            cmbTerminalID.DisplayMember = "eft_termid";
            cmbTerminalID.ValueMember = "eft_termid";


        }
    }
}

