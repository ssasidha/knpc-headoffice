﻿namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    partial class STDriveoffCommand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.eODGetAccountDateDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbShiftNumber = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dpAccountDate = new System.Windows.Forms.DateTimePicker();
            this.btnExecuteReport = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbStation = new System.Windows.Forms.ComboBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.eODGetAccountDateDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // eODGetAccountDateDetailsBindingSource
            // 
            this.eODGetAccountDateDetailsBindingSource.DataMember = "EOD_GetAccountDateDetails";
            // 
            // cmbShiftNumber
            // 
            this.cmbShiftNumber.DisplayMember = "eodno";
            this.cmbShiftNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShiftNumber.FormattingEnabled = true;
            this.cmbShiftNumber.Location = new System.Drawing.Point(9, 102);
            this.cmbShiftNumber.Name = "cmbShiftNumber";
            this.cmbShiftNumber.Size = new System.Drawing.Size(182, 21);
            this.cmbShiftNumber.TabIndex = 45;
            this.cmbShiftNumber.ValueMember = "eodno";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 44;
            this.label3.Text = "Shift Number";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(231, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "Account Date";
            // 
            // dpAccountDate
            // 
            this.dpAccountDate.Checked = false;
            this.dpAccountDate.CustomFormat = "dd/MM/yyyy";
            this.dpAccountDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpAccountDate.Location = new System.Drawing.Point(234, 42);
            this.dpAccountDate.Name = "dpAccountDate";
            this.dpAccountDate.Size = new System.Drawing.Size(127, 20);
            this.dpAccountDate.TabIndex = 56;
            this.dpAccountDate.ValueChanged += new System.EventHandler(this.OnStationChanged);
            // 
            // btnExecuteReport
            // 
            this.btnExecuteReport.Location = new System.Drawing.Point(395, 37);
            this.btnExecuteReport.Name = "btnExecuteReport";
            this.btnExecuteReport.Size = new System.Drawing.Size(93, 28);
            this.btnExecuteReport.TabIndex = 53;
            this.btnExecuteReport.Text = "Show Report";
            this.btnExecuteReport.UseVisualStyleBackColor = true;
            this.btnExecuteReport.Click += new System.EventHandler(this.btnExecuteReport_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 55;
            this.label6.Text = "Station";
            // 
            // cmbStation
            // 
            this.cmbStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStation.FormattingEnabled = true;
            this.cmbStation.Location = new System.Drawing.Point(9, 41);
            this.cmbStation.Name = "cmbStation";
            this.cmbStation.Size = new System.Drawing.Size(182, 21);
            this.cmbStation.TabIndex = 54;
            this.cmbStation.SelectedIndexChanged += new System.EventHandler(this.OnStationChanged);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbShiftNumber);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.btnExecuteReport);
            this.groupBox2.Controls.Add(this.dpAccountDate);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cmbStation);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(513, 153);
            this.groupBox2.TabIndex = 59;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Drive Off Report";
            // 
            // STDriveoffCommand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox2);
            this.Name = "STDriveoffCommand";
            this.Size = new System.Drawing.Size(513, 153);
            ((System.ComponentModel.ISupportInitialize)(this.eODGetAccountDateDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExecuteReport;
        private System.Windows.Forms.BindingSource eODGetAccountDateDetailsBindingSource;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbStation;
        private System.Windows.Forms.ComboBox cmbShiftNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dpAccountDate;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}
