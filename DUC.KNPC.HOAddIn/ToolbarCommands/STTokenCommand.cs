﻿using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.ToolbarCommands
{
    public partial class STTokenCommand : DUC.KNPC.HOAddIn.SupportToolbarControlBase
    {
        public STTokenCommand()
        {
            InitializeComponent();
            dpAccountDate.Value = DateTime.Today;
            RefreshState();
        }

        protected override void RefreshState()
        {
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";
            //groupBox1.Enabled = false;
            PopulatePayTypes();
            LoadTerminals();
            PopulateShifts();
            populateFillingPoint();
        }

        private void OnExecuteClicked(object sender, System.EventArgs e)
        {
            if (!dpAccountDate.Checked)
            {
                errorProvider.SetError(dpAccountDate, "Please select an account date");
                return;
            }
            var selectedStation = cmbStation.SelectedItem as StationDTO;
            var selectedPayment = cmbToken.SelectedItem as PaymentTypeDTO;
            var selectedShift = cmbShiftNumber .SelectedItem as ShiftDetailsDTO;
            string  terminalID = cmbTerminalID.SelectedValue.ToString();
            var fillingObj = cbFilling.SelectedItem as FillingDTO;
            string fillingpoint = fillingObj.Name;

            FilterDTO filter = new FilterDTO();
            filter.Station = selectedStation;
            filter.AccountDate = dpAccountDate.Value;
            filter.PaymentType = selectedPayment;
            filter.Shift = selectedShift;
            filter.TerminalID = terminalID;
            filter.Filling = fillingObj;

            var reportViewer = new Viewer.vwrToken(filter);
            reportViewer.ShowDialog();
        }

        private void cmbStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbStation.SelectedIndex > 0 & dpAccountDate.Checked)
            {
                //groupBox1.Enabled = true;
                PopulateShifts();
                PopulatePayTypes();
                LoadTerminals();
                populateFillingPoint();
            }
            else
            {
                //groupBox1.Enabled = false;
                PopulateEmptyShifts();
            }
        }

        private void PopulateEmptyShifts()
        {
            try
            {
                cmbShiftNumber.DataSource = this.GetEmptyShiftDetails();
                cmbShiftNumber.DisplayMember = "ShiftName";
                cmbShiftNumber.ValueMember = "EODNo";
            }
            catch (Exception exception)
            {
                errorProvider.SetError(cmbShiftNumber, exception.Message);
            }
        }

        private void PopulatePayTypes()
        {
            int selectedStation = -1;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = int.Parse(cmbStation.SelectedValue.ToString());
            cmbToken.DataSource = this.GetAllOnlineTokenTypes(selectedStation);
            cmbToken.DisplayMember = "PayType";
            cmbToken.ValueMember = "PayTNo";

        }

        private void LoadTerminals()
        {
            Repository rep = new Repository();
            int selectedStation = -1;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = int.Parse(cmbStation.SelectedValue.ToString());
            StationData.v_TerminalIDDataTable terminalDetails = rep.GetAllTerminals(selectedStation);
            var row = terminalDetails.Newv_TerminalIDRow();
            row.eft_termid = "Select All";
            terminalDetails.Rows.InsertAt(row, 0);
            cmbTerminalID.DataSource = terminalDetails;
            cmbTerminalID.DisplayMember = "eft_termid";
            cmbTerminalID.ValueMember = "eft_termid";
        }

        private void PopulateShifts()
        {
            int? selectedStation = null;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = (int?)int.Parse(cmbStation.SelectedValue.ToString());
            cmbShiftNumber.DataSource = this.GetShiftDetails(dpAccountDate.Value, selectedStation);
            cmbShiftNumber.DisplayMember = "ShiftName";
            cmbShiftNumber.ValueMember = "EODNo";
        }

        private void populateFillingPoint()
        {
            Repository rep = new Repository();
            int stationId = -1;
            try
            {
                stationId = cmbStation.SelectedValue.ToString().Equals("Select All") ? -1 : int.Parse(cmbStation.SelectedValue.ToString());
            }
            catch (Exception e)
            {
                
            }
            StationData.GetFuelPointsDataTable fuelPoints = rep.GetFuelPoints(stationId);
            cbFilling.Items.Clear();
            cbFilling.Items.Add(new FillingDTO { ID = -1, Name = "Select All" });
            foreach (DataRow r in fuelPoints.Rows)
            {
                cbFilling.Items.Add(new FillingDTO { ID = int.Parse(r["fuelpoint"].ToString()), Name = r["fuelpoint"].ToString() });
            }
            cbFilling.DisplayMember = "Name";
            cbFilling.ValueMember = "ID";
            cbFilling.SelectedIndex = 0;
        }

        private void cmbToken_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
