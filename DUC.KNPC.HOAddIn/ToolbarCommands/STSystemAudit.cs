﻿using DUC.ADNOC.HOAddIn.DataAccess;
using DUC.ADNOC.HOAddIn.DataAccess.Datasets;
using DUC.ADNOC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.ADNOC.HOAddIn.ToolbarCommands
{
    public partial class STSystemAudit : DUC.ADNOC.HOAddIn.SupportToolbarControlBase
    {
        public STSystemAudit()
        {
            InitializeComponent();
            dpAccountDateFrom.Value = DateTime.Today;
            dpAccountDateTo.Value = DateTime.Today;
            RefreshState();
            groupBox1.Enabled = false;
        }

        protected override void RefreshState()
        {
            cmbStation.DataSource = this.GetAllStations();
            cmbStation.ValueMember = "StationId";
            cmbStation.DisplayMember = "StationName";
            PopulatePayTypes();
            LoadTerminals();
            LoadLevel();
        }

        private void LoadLevel()
        {

            Repository rep = new Repository();
            StationData.sp_getLogLevelDataTable details = rep.GetLogLevel();
            var row = details.Newsp_getLogLevelRow();
            row.Id = -1;
            row.Description = "Select All";
            details.Rows.InsertAt(row, 0);

            cmbLevel.DataSource = details;
            cmbLevel.DisplayMember = "Description";
            cmbLevel.ValueMember = "Id";
        }

        private void OnExecuteClicked(object sender, EventArgs e)
        {
            
            errorProvider.Clear();

            if (!(cmbStation.SelectedIndex > 0))
            {
                errorProvider.SetError(cmbStation, "Please select a station");
                return;
            }

            if (!dpAccountDateFrom.Checked)
            {
                errorProvider.SetError(dpAccountDateFrom, "Please select an from Date");
                return;
            }

            if (!dpAccountDateTo.Checked)
            {
                errorProvider.SetError(dpAccountDateTo, "Please select an to Date");
                return;
            }

            var selectedStation = cmbStation.SelectedItem as StationDTO;
          
            DataRowView device = (DataRowView)cmbToken.SelectedItem;
            DataRowView user = (DataRowView)cmbTerminalID.SelectedItem;
            DataRowView logLevel = (DataRowView)cmbLevel.SelectedItem;

            int nbr = int.TryParse(device.Row["Device_Nbr"].ToString(), out nbr) ? nbr : 0;
            String desc = device.Row["Description"].ToString();

            int userno = int.TryParse(user.Row["persno"].ToString(), out userno) ? userno : 0;
            String username = user.Row["persname"].ToString();

            int levelno = int.TryParse(logLevel.Row["id"].ToString(), out levelno) ? levelno : 0;
            String levelName = logLevel.Row["Description"].ToString();

            var reportViewer = new Viewer.vwrSystemAudit(selectedStation, dpAccountDateFrom.Value, dpAccountDateTo.Value, nbr, userno, desc, username, levelno, levelName);
            reportViewer.ShowDialog();
        }

        private void OnStationChanged(object sender, EventArgs e)
        {
            if (cmbStation.SelectedIndex > 0)
            {
                groupBox1.Enabled = true;
                LoadTerminals();

            }
            else
            {
                groupBox1.Enabled = false;
            }
        }

        private void PopulatePayTypes()
        {
             Repository rep = new Repository();
            StationData.sp_getDeviceListDataTable terminalDetails = rep.GetDevicess();
            var row = terminalDetails.Newsp_getDeviceListRow();
            row.Description = "Select All";
            row.Device_Nbr = -1;
            terminalDetails.Rows.InsertAt(row, 0);
            cmbToken.DataSource = terminalDetails;
            cmbToken.DisplayMember = "Description";
            cmbToken.ValueMember = "Device_Nbr";
        }

        private void LoadTerminals()
        {
            Repository rep = new Repository();
            int selectedStation = -1;
            if (cmbStation.SelectedIndex > 0)
                selectedStation = int.Parse(cmbStation.SelectedValue.ToString());
            StationData.sp_getEmpDataTable terminalDetails = rep.GetEmps(selectedStation);
            var row = terminalDetails.Newsp_getEmpRow();
            row.persname = "Select All";
            row.persno = -1;
            terminalDetails.Rows.InsertAt(row, 0);

            var row1 = terminalDetails.Newsp_getEmpRow();
            row1.persname = "System";
            row1.persno = 0;
            terminalDetails.Rows.InsertAt(row1, 1);

            cmbTerminalID.DataSource = terminalDetails;
            cmbTerminalID.DisplayMember = "persname";
            cmbTerminalID.ValueMember = "persno";
        }
    }
}

