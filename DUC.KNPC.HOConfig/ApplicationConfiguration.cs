﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;


namespace DUC.KNPC.HOAddIn.HOConfig
{
    public static class ApplicationConfiguration
    {
        private static object _syncRoot = new object();
        private static SortedDictionary<string, string> _configurations;

        static ApplicationConfiguration()
        {
            _configurations = new SortedDictionary<string, string>();
            LoadConfiguration();
            //CheckFileUpdateAndLoad();
        }

        public static T GetConfigurationOrDetault<T>(string key, T defaultValue = default(T))
        {
            lock (_syncRoot)
            {
                if (_configurations.ContainsKey(key))
                {
                    var value = _configurations[key];
                    if (!string.IsNullOrEmpty(value))
                    {
                        return (T)Convert.ChangeType(value, typeof(T));
                    }
                }

                return defaultValue;
            }
        }

        private static void CheckFileUpdateAndLoad()
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            var path = @"c:\DUC";
#if DEBUG
            path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
#endif
            watcher.Path = path;
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "AppConfiguration.xml";
            watcher.Changed -= new FileSystemEventHandler(OnChanged);
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.EnableRaisingEvents = true;
        }

        private static void OnChanged(object sender, FileSystemEventArgs e)
        {
            LoadConfiguration();
        }

        private static void LoadConfiguration()
        {
            XmlDocument doc = new XmlDocument();
            var xmlPath = Path.Combine(@"c:\DUC", "AppConfiguration.xml");
#if DEBUG
            xmlPath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "AppConfiguration.xml");
#endif
            if (File.Exists(xmlPath))
            {
                doc.Load(xmlPath);
                _configurations.Clear();

                foreach (XmlNode node in doc.SelectNodes("/configuration/connections"))
                {
                    foreach (XmlNode configNode in node.ChildNodes)
                    {
                        _configurations.Add(configNode.Attributes["key"].Value, configNode.Attributes["value"].Value);
                    }
                }
            }
        }
    }
}
