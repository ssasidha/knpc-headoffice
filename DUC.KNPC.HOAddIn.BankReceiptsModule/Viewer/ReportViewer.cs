﻿using DUC.KNPC.HOAddIn.BankReceiptsModule.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.Viewer
{
    public partial class ReportViewer : Form
    {
        public ReportViewer()
        {
            InitializeComponent();
        }

        public void ShowReport(string reportName, object reportParams)
        {
            var builder = new BRMReportBuilder();
            builder.BuildReport(reportName, reportParams, this.reportViewerControl.LocalReport);
            this.reportViewerControl.RefreshReport();
            this.Show();
        }
    }
}
