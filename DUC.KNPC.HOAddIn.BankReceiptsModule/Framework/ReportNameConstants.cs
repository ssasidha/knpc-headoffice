﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.Framework
{
    public class ReportNameConstants
    {
        public const string DAILY_CASH_BANKED_REPORT = "Daily Banked Cash Summary Report";
        public const string MONTHLY_CASH_BANKED_REPORT = "Monthly Cash Banked Summary Report";
        public const string BY_ACCOUNT_DATE = "Banked Cash Details by Account Date Report";
        public const string MONTHLY_CLOSED = "Monthly Closed Bank Receipt Detail Report";
    }
}
