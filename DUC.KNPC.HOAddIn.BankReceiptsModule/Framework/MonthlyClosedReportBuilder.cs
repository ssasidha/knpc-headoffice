﻿using DUC.KNPC.HOAddIn.BankReceiptsModule.ToolbarCommands;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.BankDataTableAdapters;
using DUC.KNPC.HOAddIn.HOConfig;
using Microsoft.Reporting.WinForms;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.Framework
{
    public class MonthlyClosedReportBuilder
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("BankData");

        public void GetReport(object reportParams, LocalReport localReport)
        {
            var monthData = reportParams as MonthData;
            localReport.ReportEmbeddedResource = "DUC.KNPC.HOAddIn.BankReceiptsModule.Reports.rptMonthlyClosedBankReceiptDetail.rdlc";
            localReport.SetParameters(new ReportParameter("rpMonthYear", monthData.Name));

            BankData.rptMonthlyClosedBRDetailsDataTable data = new BankData.rptMonthlyClosedBRDetailsDataTable();
            rptMonthlyClosedBRDetailsTableAdapter adapter = new rptMonthlyClosedBRDetailsTableAdapter();
            adapter.Connection = new System.Data.SqlClient.SqlConnection(CONNECTION_STRING);
            adapter.Fill(data, monthData.StartDate, monthData.EndDate);

            ReportDataSource ds = new ReportDataSource("rptMonthlyClosedBRDetails", data.Rows);
            localReport.DataSources.Add(ds);
        }

        public string ReportName
        {
            get
            {
                return ReportNameConstants.MONTHLY_CLOSED;
            }
        }
    }
}
