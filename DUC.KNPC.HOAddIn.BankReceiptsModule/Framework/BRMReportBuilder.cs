﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.Framework
{
    internal class BRMReportBuilder
    {
        public void BuildReport(string reportName, object reportParams, LocalReport report)
        {
            switch(reportName)
            {
                case ReportNameConstants.DAILY_CASH_BANKED_REPORT:
                    var reportBuilder = new DailyCashBankedReportBuilder();
                    reportBuilder.GetReport(reportParams, report);
                    break;
                case ReportNameConstants.MONTHLY_CASH_BANKED_REPORT:
                     var reportMonthlyBuilder = new MonthlyCashBankedReportBuilder();
                     reportMonthlyBuilder.GetReport(reportParams, report);
                    break;
                case ReportNameConstants.BY_ACCOUNT_DATE:
                    var reportByAccountDateBuilder = new ByAccountDateReportBuilder();
                    reportByAccountDateBuilder.GetReport(reportParams, report);
                    break;
                case ReportNameConstants.MONTHLY_CLOSED:
                    var reportMonthlyClosedBuilder = new MonthlyClosedReportBuilder();
                    reportMonthlyClosedBuilder.GetReport(reportParams, report);
                    break;
            }
        }
    }
}
