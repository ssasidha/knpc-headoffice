﻿using DUC.KNPC.HOAddIn.BankReceiptsModule.ToolbarCommands;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.BankDataTableAdapters;
using DUC.KNPC.HOAddIn.HOConfig;
using Microsoft.Reporting.WinForms;
using System;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.Framework
{
    public class ByAccountDateReportBuilder
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("BankData");

        public void GetReport(object reportParams, LocalReport localReport)
        {
            var accountDate = (DateTime)reportParams;
            localReport.ReportEmbeddedResource = "DUC.KNPC.HOAddIn.BankReceiptsModule.Reports.rptBankCashDetailsByAccountDate.rdlc";
            localReport.SetParameters(new ReportParameter("rpAccountDate", accountDate.ToString("dd-MM-yyyy")));

            BankData.rptBankCashDetailsByAccountDateDataTable data = new BankData.rptBankCashDetailsByAccountDateDataTable();
            rptBankCashDetailsByAccountDateTableAdapter adapter = new rptBankCashDetailsByAccountDateTableAdapter();
            adapter.Connection = new System.Data.SqlClient.SqlConnection(CONNECTION_STRING);
            adapter.Fill(data, accountDate);

            ReportDataSource ds = new ReportDataSource("rptBankCashDetailsByAccountDate", data.Rows);
            localReport.DataSources.Add(ds);
        }

        public string ReportName
        {
            get
            {
                return ReportNameConstants.BY_ACCOUNT_DATE;
            }
        }
    }
}
