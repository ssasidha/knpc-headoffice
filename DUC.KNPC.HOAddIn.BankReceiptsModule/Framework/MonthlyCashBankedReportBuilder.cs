﻿using DUC.KNPC.HOAddIn.BankReceiptsModule.ToolbarCommands;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.BankDataTableAdapters;
using DUC.KNPC.HOAddIn.HOConfig;
using Microsoft.Reporting.WinForms;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.Framework
{
    public class MonthlyCashBankedReportBuilder
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("BankData");

        public void GetReport(object reportParams, LocalReport localReport)
        {
            var monthData = reportParams as MonthData;
            localReport.ReportEmbeddedResource = "DUC.KNPC.HOAddIn.BankReceiptsModule.Reports.rptMonthlyBankedCashSummary.rdlc";
            localReport.SetParameters(new ReportParameter("MonthData", monthData.Name));

            BankData.rptMonthlyBankedCashSummaryDataTable data = new BankData.rptMonthlyBankedCashSummaryDataTable();
            rptMonthlyBankedCashSummaryTableAdapter adapter = new rptMonthlyBankedCashSummaryTableAdapter();
            adapter.Connection = new System.Data.SqlClient.SqlConnection(CONNECTION_STRING);
            adapter.Fill(data, monthData.StartDate, monthData.EndDate);

            ReportDataSource ds = new ReportDataSource("rptMonthlyBankedCashData", data.Rows);
            localReport.DataSources.Add(ds);
        }

        public string ReportName
        {
            get
            {
                return ReportNameConstants.MONTHLY_CASH_BANKED_REPORT;
            }
        }
    }
}
