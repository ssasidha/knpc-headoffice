﻿using DUC.KNPC.HOAddIn.BankReceiptsModule.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.ToolbarCommands
{
    public class STBankedCashDetailsbyAccountDate : SupportToolbarControlBase
    {
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.DateTimePicker dpAccountDate;
        private System.Windows.Forms.Label label1;

        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dpAccountDate = new System.Windows.Forms.DateTimePicker();
            this.btnShow = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dpAccountDate);
            this.groupBox1.Controls.Add(this.btnShow);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(310, 137);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Banked Cash Details by Account Date Report";
            // 
            // dpAccountDate
            // 
            this.dpAccountDate.CustomFormat = "dd-MM-yyyy";
            this.dpAccountDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpAccountDate.Location = new System.Drawing.Point(13, 54);
            this.dpAccountDate.Name = "dpAccountDate";
            this.dpAccountDate.Size = new System.Drawing.Size(129, 20);
            this.dpAccountDate.TabIndex = 6;
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(181, 52);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(97, 23);
            this.btnShow.TabIndex = 5;
            this.btnShow.Text = "Show Report";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.OnShowReportClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Account Date:";
            // 
            // STBankedCashDetailsbyAccountDate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox1);
            this.Name = "STBankedCashDetailsbyAccountDate";
            this.Size = new System.Drawing.Size(310, 137);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        public STBankedCashDetailsbyAccountDate()
        {
            InitializeComponent();
        }

        protected override void InitCommandStates()
        {
            _CommandStates[_ADDIN_COMMAND_DELETE] = false;
            _CommandStates[_ADDIN_COMMAND_NEW] = false;
            _CommandStates[_ADDIN_COMMAND_REFRESH] = false;
            _CommandStates[_ADDIN_COMMAND_SAVE] = false;
            _CommandStates[_ADDIN_COMMAND_SHOW] = true;
        }

        public override void DoCommand(string command)
        {
            switch (command)
            {
                case _ADDIN_COMMAND_SHOW:
                    ShowReport();
                    break;
            }
        }

        private void ShowReport()
        {
            var reportViewer = new Viewer.ReportViewer();
            reportViewer.ShowReport(ReportNameConstants.BY_ACCOUNT_DATE, dpAccountDate.Value);
        }

        private void OnShowReportClicked(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}
