﻿using DUC.KNPC.HOAddIn.BankReceiptsModule.Framework;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.ToolbarCommands
{
    public class STDailyBankedCashSummary : SupportToolbarControlBase
    {
        private System.Windows.Forms.ComboBox cmbMonths;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.GroupBox groupBox1;
    
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnShow = new System.Windows.Forms.Button();
            this.cmbMonths = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnShow);
            this.groupBox1.Controls.Add(this.cmbMonths);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(325, 112);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Daily Banked Cash Summary Report";
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(181, 52);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(97, 23);
            this.btnShow.TabIndex = 5;
            this.btnShow.Text = "Show Report";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.OnShowReportClicked);
            // 
            // cmbMonths
            // 
            this.cmbMonths.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMonths.FormattingEnabled = true;
            this.cmbMonths.Location = new System.Drawing.Point(8, 52);
            this.cmbMonths.Name = "cmbMonths";
            this.cmbMonths.Size = new System.Drawing.Size(143, 21);
            this.cmbMonths.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Month:";
            // 
            // STDailyBankedCashSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox1);
            this.Name = "STDailyBankedCashSummary";
            this.Size = new System.Drawing.Size(325, 112);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        public STDailyBankedCashSummary()
        {
            InitializeComponent();
            RefreshState();
        }

        protected override void InitCommandStates()
        {
            _CommandStates[_ADDIN_COMMAND_DELETE] = false;
            _CommandStates[_ADDIN_COMMAND_NEW] = false;
            _CommandStates[_ADDIN_COMMAND_REFRESH] = true;
            _CommandStates[_ADDIN_COMMAND_SAVE] = false;
            _CommandStates[_ADDIN_COMMAND_SHOW] = true;
        }

        public override void DoCommand(string command)
        {
            switch (command)
            {
                case _ADDIN_COMMAND_SHOW:
                    ShowReport();
                    break;
                case _ADDIN_COMMAND_REFRESH:
                    RefreshState();
                    break;
            }
        }

        protected override void RefreshState()
        {
            var monthDate = new List<MonthData>();
            for (var date = DateTime.Now.AddMonths(-5); date <= DateTime.Now; date = date.AddMonths(1))
            {
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                monthDate.Add(new MonthData
                {
                    Name = date.ToString("MMM yyyy"),
                    StartDate = firstDayOfMonth,
                    EndDate = firstDayOfMonth.AddMonths(1).AddDays(-1)
                });
            }

            cmbMonths.DataSource = monthDate;
            cmbMonths.DisplayMember = "Name";
        }

        private void ShowReport()
        {
            var selectedMonth = cmbMonths.SelectedItem as MonthData;
            var reportViewer = new Viewer.ReportViewer();
            reportViewer.ShowReport(ReportNameConstants.DAILY_CASH_BANKED_REPORT, selectedMonth);
        }

        private void OnShowReportClicked(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}
