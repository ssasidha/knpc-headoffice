﻿using DUC.KNPC.HOAddIn.BankReceiptsModule.Dialogs;
using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
namespace DUC.KNPC.HOAddIn.BankReceiptsModule.ToolbarCommands
{
    public partial class STBankReceiptsManagement : SupportToolbarControlBase
    {
        #region "Designer Generated"

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbMonths;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.DataGridView dgvBankReceipts;
        private System.Windows.Forms.GroupBox groupBox1; 

        #endregion
    
        public STBankReceiptsManagement()
        {
            InitializeComponent();
            InitCommandStates();
            RefreshState();
        }

        #region "Designer Generated"

        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvBankReceipts = new System.Windows.Forms.DataGridView();
            this.btnShow = new System.Windows.Forms.Button();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbMonths = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBankReceipts)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvBankReceipts);
            this.groupBox1.Controls.Add(this.btnShow);
            this.groupBox1.Controls.Add(this.cmbStatus);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbMonths);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(755, 419);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bank Receipt Management";
            // 
            // dgvBankReceipts
            // 
            this.dgvBankReceipts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBankReceipts.Location = new System.Drawing.Point(6, 94);
            this.dgvBankReceipts.MultiSelect = false;
            this.dgvBankReceipts.Name = "dgvBankReceipts";
            this.dgvBankReceipts.Size = new System.Drawing.Size(743, 312);
            this.dgvBankReceipts.TabIndex = 5;
            this.dgvBankReceipts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellClick);
            this.dgvBankReceipts.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.OnDataBindingComplete);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(384, 47);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 23);
            this.btnShow.TabIndex = 4;
            this.btnShow.Text = "Display";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.OnDisplayClicked);
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "Open",
            "Closed"});
            this.cmbStatus.Location = new System.Drawing.Point(198, 47);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(143, 21);
            this.cmbStatus.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(198, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Status";
            // 
            // cmbMonths
            // 
            this.cmbMonths.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMonths.FormattingEnabled = true;
            this.cmbMonths.Location = new System.Drawing.Point(18, 47);
            this.cmbMonths.Name = "cmbMonths";
            this.cmbMonths.Size = new System.Drawing.Size(143, 21);
            this.cmbMonths.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Month";
            // 
            // STBankReceiptsManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox1);
            this.Name = "STBankReceiptsManagement";
            this.Size = new System.Drawing.Size(755, 419);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBankReceipts)).EndInit();
            this.ResumeLayout(false);

        } 

        #endregion

        protected override void RefreshState()
        {
            var monthDate = new List<MonthData>();
            for(var date = DateTime.Now.AddMonths(-5); date <= DateTime.Now; date = date.AddMonths(1))
            {
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                monthDate.Add(new MonthData
                    {
                        Name = date.ToString("MMM yyyy"),
                        StartDate = firstDayOfMonth,
                        EndDate = firstDayOfMonth.AddMonths(1).AddDays(-1)
                    });
            }

            cmbMonths.DataSource = monthDate;
            cmbMonths.DisplayMember = "Name";

            cmbStatus.DataSource = Enum.GetNames(typeof(ReceiptStatus));
        }

        protected override void InitCommandStates()
        {
            _CommandStates[_ADDIN_COMMAND_DELETE] = true;
            _CommandStates[_ADDIN_COMMAND_NEW] = true;
            _CommandStates[_ADDIN_COMMAND_REFRESH] = true;
            _CommandStates[_ADDIN_COMMAND_SAVE] = true;
            _CommandStates[_ADDIN_COMMAND_SHOW] = true;
        }

        private void OnDisplayClicked(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void FillGrid()
        {
            var selectedMonth = cmbMonths.SelectedItem as MonthData;
            var selectedStatus = (ReceiptStatus)Enum.Parse(typeof(ReceiptStatus), cmbStatus.Text);
            var repository = new Repository();
            dgvBankReceipts.DataSource = repository.GetBankReceipts(selectedMonth.StartDate, selectedMonth.EndDate, (short)selectedStatus);
        }

        private void OnDataBindingComplete(object sender, System.Windows.Forms.DataGridViewBindingCompleteEventArgs e)
        {
            if (dgvBankReceipts.Columns.Count == 5)
            {
                DataGridViewButtonColumn editButton = new DataGridViewButtonColumn();
                editButton.UseColumnTextForButtonValue = true;
                editButton.Text = "Edit";
                editButton.Name = "btnEdit";
                editButton.HeaderText = "Edit";
                dgvBankReceipts.Columns.Add(editButton);

                DataGridViewButtonColumn assignButton = new DataGridViewButtonColumn();
                assignButton.UseColumnTextForButtonValue = true;
                assignButton.Text = "Assign Shifts";
                assignButton.Name = "btnAssign";
                assignButton.HeaderText = "Assign Shifts";
                dgvBankReceipts.Columns.Add(assignButton);
            }
        }

        private void OnCellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1)
                return;

            if (dgvBankReceipts.Columns[e.ColumnIndex].Name == "btnEdit" || dgvBankReceipts.Columns[e.ColumnIndex].Name == "btnAssign")
            {
                var dataSource = dgvBankReceipts.DataSource as List<BankReceiptListDTO>;
                var selectedData = dataSource[e.RowIndex];

                if (selectedData.Status == ReceiptStatus.Closed)
                {
                    MessageBox.Show("Selected bank receipt is closed and no further corrections or shift assignment can be made.", "Closed Receipt", MessageBoxButtons.OK);
                    return;
                }
                if (dgvBankReceipts.Columns[e.ColumnIndex].Name == "btnEdit")
                {
                    ShowEditDialog(selectedData);
                }
                else
                {
                    ShowAssignDialog(selectedData);
                }
            }
        }

        public override void DoCommand(string command)
        {
            switch (command)
            {
                case _ADDIN_COMMAND_NEW:
                    ShowAddDialog();
                    break;
                case _ADDIN_COMMAND_SAVE:
                    CloseBankReceipt();
                    break;
                case _ADDIN_COMMAND_SHOW:
                    if (dgvBankReceipts.SelectedRows.Count > 0)
                    {
                        var data = dgvBankReceipts.SelectedRows[0].DataBoundItem as BankReceiptListDTO;
                        ShowEditDialog(data);
                    }
                    break;
                case _ADDIN_COMMAND_REFRESH:
                    RefreshState();
                    break;
                case _ADDIN_COMMAND_DELETE:
                    DeleteBankReceipt();
                    break;
            }
        }

        private void ShowAddDialog()
        {
            var addDialog = new AddBankReceiptDialog(this.GetUserName());
            if(addDialog.ShowDialog() == DialogResult.OK)
            {
                this.FillGrid();
            }
        }

        private void ShowEditDialog(BankReceiptListDTO selectedData)
        {
            var editDialog = new EditBankReceiptDialog(selectedData, this.GetUserName());
            if (editDialog.ShowDialog() == DialogResult.OK)
            {
                this.FillGrid();
            }
        }

        private void ShowAssignDialog(BankReceiptListDTO selectedData)
        {
            var assignDialog = new BankReceiptShiftAssignmentDialog(selectedData.BankReceiptNo);
            assignDialog.ShowDialog();
        }

        private void CloseBankReceipt()
        {
            var repository = new Repository();
            if (dgvBankReceipts.SelectedRows.Count > 0)
            {
                var data = dgvBankReceipts.SelectedRows[0].DataBoundItem as BankReceiptListDTO;
                if(repository.CanCloseBankReceipt(data.BankReceiptNo))
                {
                    repository.SetBankReceiptClosed(data.BankReceiptNo, this.GetUserName());
                    this.FillGrid();
                    return;
                }
                MessageBox.Show("No shifts assigned to the selected bank receipt.", "No Shift Assigned", MessageBoxButtons.OK);
            }
        }

        private void DeleteBankReceipt()
        {
            if (dgvBankReceipts.SelectedRows.Count > 0)
            {
                var data = dgvBankReceipts.SelectedRows[0].DataBoundItem as BankReceiptListDTO;
               if(data != null)
               {
                   var repository = new Repository();
                   if(!repository.CanCloseBankReceipt(data.BankReceiptNo))
                   {
                       repository.DeleteBankReceiptRecord(data.BankReceiptNo);
                       this.FillGrid();
                       return;
                   }

                   MessageBox.Show("Selected bank receipt cannot be deleted as shifts are assigned.", "Deletion not permitted", MessageBoxButtons.OK);
               }
            }
        }
    }

    class MonthData
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
