﻿using DUC.KNPC.HOAddIn.BankReceiptsModule.Dialogs;
using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.ToolbarCommands
{
    public class STBankReceiptDetailsView : SupportToolbarControlBase
    {
        #region "Designer Generated"

        private System.Windows.Forms.TextBox txtBankReceiptNumber;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dpBankDate;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBranch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBankID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBankName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDifference;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSafeDropTotal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvShiftDetails;
        private System.Windows.Forms.Button btnAssignShift;
        private System.Windows.Forms.DataGridViewTextBoxColumn StationNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShiftNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CashSales;
        private System.Windows.Forms.DataGridViewTextBoxColumn SafeDropTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Difference;
        private System.Windows.Forms.GroupBox groupBox1;

        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAssignShift = new System.Windows.Forms.Button();
            this.dgvShiftDetails = new System.Windows.Forms.DataGridView();
            this.StationNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShiftNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CashSales = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SafeDropTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Difference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDifference = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSafeDropTotal = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dpBankDate = new System.Windows.Forms.DateTimePicker();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBranch = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBankID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBankName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBankReceiptNumber = new System.Windows.Forms.TextBox();
            this.btnShow = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShiftDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAssignShift);
            this.groupBox1.Controls.Add(this.dgvShiftDetails);
            this.groupBox1.Controls.Add(this.txtDifference);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtSafeDropTotal);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.dpBankDate);
            this.groupBox1.Controls.Add(this.txtTotal);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtBranch);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtBankID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtBankName);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtBankReceiptNumber);
            this.groupBox1.Controls.Add(this.btnShow);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(650, 517);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bank Receipt Detail View";
            // 
            // btnAssignShift
            // 
            this.btnAssignShift.Enabled = false;
            this.btnAssignShift.Location = new System.Drawing.Point(514, 449);
            this.btnAssignShift.Name = "btnAssignShift";
            this.btnAssignShift.Size = new System.Drawing.Size(111, 23);
            this.btnAssignShift.TabIndex = 37;
            this.btnAssignShift.Text = "Assign Shift";
            this.btnAssignShift.UseVisualStyleBackColor = true;
            this.btnAssignShift.Click += new System.EventHandler(this.OnAssignDataClicked);
            // 
            // dgvShiftDetails
            // 
            this.dgvShiftDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShiftDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StationNo,
            this.AccountDate,
            this.ShiftNo,
            this.CashSales,
            this.SafeDropTotal,
            this.Difference});
            this.dgvShiftDetails.Location = new System.Drawing.Point(21, 249);
            this.dgvShiftDetails.Name = "dgvShiftDetails";
            this.dgvShiftDetails.RowHeadersVisible = false;
            this.dgvShiftDetails.Size = new System.Drawing.Size(604, 182);
            this.dgvShiftDetails.TabIndex = 36;
            this.dgvShiftDetails.Click += new System.EventHandler(this.OnAssignDataClicked);
            // 
            // StationNo
            // 
            this.StationNo.DataPropertyName = "StationNo";
            this.StationNo.HeaderText = "Station Number";
            this.StationNo.Name = "StationNo";
            this.StationNo.ReadOnly = true;
            // 
            // AccountDate
            // 
            this.AccountDate.DataPropertyName = "AccountDate";
            this.AccountDate.HeaderText = "Account Date";
            this.AccountDate.Name = "AccountDate";
            this.AccountDate.ReadOnly = true;
            // 
            // ShiftNo
            // 
            this.ShiftNo.DataPropertyName = "ShiftNo";
            this.ShiftNo.HeaderText = "Shift No";
            this.ShiftNo.Name = "ShiftNo";
            this.ShiftNo.ReadOnly = true;
            // 
            // CashSales
            // 
            this.CashSales.DataPropertyName = "CashSales";
            this.CashSales.HeaderText = "Cash Sales";
            this.CashSales.Name = "CashSales";
            this.CashSales.ReadOnly = true;
            // 
            // SafeDropTotal
            // 
            this.SafeDropTotal.DataPropertyName = "SafeDropTotal";
            this.SafeDropTotal.HeaderText = "Safe Drop Total";
            this.SafeDropTotal.Name = "SafeDropTotal";
            this.SafeDropTotal.ReadOnly = true;
            // 
            // Difference
            // 
            this.Difference.DataPropertyName = "Difference";
            this.Difference.HeaderText = "Difference";
            this.Difference.Name = "Difference";
            this.Difference.ReadOnly = true;
            // 
            // txtDifference
            // 
            this.txtDifference.Enabled = false;
            this.txtDifference.Location = new System.Drawing.Point(446, 209);
            this.txtDifference.Name = "txtDifference";
            this.txtDifference.Size = new System.Drawing.Size(153, 20);
            this.txtDifference.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(446, 193);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 34;
            this.label8.Text = "Difference";
            // 
            // txtSafeDropTotal
            // 
            this.txtSafeDropTotal.Enabled = false;
            this.txtSafeDropTotal.Location = new System.Drawing.Point(232, 209);
            this.txtSafeDropTotal.Name = "txtSafeDropTotal";
            this.txtSafeDropTotal.Size = new System.Drawing.Size(153, 20);
            this.txtSafeDropTotal.TabIndex = 33;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(233, 193);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Safe Drop Total";
            // 
            // dpBankDate
            // 
            this.dpBankDate.CustomFormat = "dd/MM/yyyy";
            this.dpBankDate.Enabled = false;
            this.dpBankDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpBankDate.Location = new System.Drawing.Point(230, 154);
            this.dpBankDate.Name = "dpBankDate";
            this.dpBankDate.Size = new System.Drawing.Size(150, 20);
            this.dpBankDate.TabIndex = 31;
            // 
            // txtTotal
            // 
            this.txtTotal.Enabled = false;
            this.txtTotal.Location = new System.Drawing.Point(18, 209);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(153, 20);
            this.txtTotal.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Bank Total";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(230, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Bank Date";
            // 
            // txtBranch
            // 
            this.txtBranch.Enabled = false;
            this.txtBranch.Location = new System.Drawing.Point(18, 154);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.Size = new System.Drawing.Size(153, 20);
            this.txtBranch.TabIndex = 27;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Branch";
            // 
            // txtBankID
            // 
            this.txtBankID.Enabled = false;
            this.txtBankID.Location = new System.Drawing.Point(230, 103);
            this.txtBankID.Name = "txtBankID";
            this.txtBankID.Size = new System.Drawing.Size(153, 20);
            this.txtBankID.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(230, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Bank ID";
            // 
            // txtBankName
            // 
            this.txtBankName.Enabled = false;
            this.txtBankName.Location = new System.Drawing.Point(18, 103);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(153, 20);
            this.txtBankName.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Bank Name";
            // 
            // txtBankReceiptNumber
            // 
            this.txtBankReceiptNumber.Location = new System.Drawing.Point(21, 44);
            this.txtBankReceiptNumber.Name = "txtBankReceiptNumber";
            this.txtBankReceiptNumber.Size = new System.Drawing.Size(143, 20);
            this.txtBankReceiptNumber.TabIndex = 7;
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(189, 42);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 23);
            this.btnShow.TabIndex = 6;
            this.btnShow.Text = "Display";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.OnDisplayClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Bank Receipt Number";
            // 
            // STBankReceiptDetailsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox1);
            this.Name = "STBankReceiptDetailsView";
            this.Size = new System.Drawing.Size(650, 517);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShiftDetails)).EndInit();
            this.ResumeLayout(false);

        } 

        #endregion

        public STBankReceiptDetailsView()
        {
            InitializeComponent();
        }

        protected override void InitCommandStates()
        {
            _CommandStates[_ADDIN_COMMAND_DELETE] = false;
            _CommandStates[_ADDIN_COMMAND_NEW] = false;
            _CommandStates[_ADDIN_COMMAND_REFRESH] = false;
            _CommandStates[_ADDIN_COMMAND_SAVE] = true;
            _CommandStates[_ADDIN_COMMAND_SHOW] = true;
        }

        public override void DoCommand(string command)
        {
            switch (command)
            {
                case _ADDIN_COMMAND_SAVE:
                    // CloseBankReceipt();
                    break;
                case _ADDIN_COMMAND_SHOW:
                    ShowAssignDialog();
                    break;
            }
        }

        public void OnDisplayClicked(object sender, EventArgs e)
        {
            FillDetails();
        }

        public void OnAssignDataClicked(object sender, EventArgs e)
        {
            ShowAssignDialog();
        }

        private void FillDetails()
        {
            long bankReceiptNo;
            BankReceiptDetailsDTO brDetails;
            if (ValidateReceiptNo(out bankReceiptNo, out brDetails, false))
            {
                txtBankID.Text = brDetails.BankID;
                txtBankName.Text = brDetails.BankName;
                txtBankReceiptNumber.Text = brDetails.BankReceiptNo.ToString();
                txtBranch.Text = brDetails.Branch;
                txtDifference.Text = brDetails.Difference.ToString("F3");
                txtSafeDropTotal.Text = brDetails.SafeDrop.ToString("F3");
                txtTotal.Text = brDetails.Total.ToString("F3");

                dgvShiftDetails.AutoGenerateColumns = false;
                dgvShiftDetails.DataSource = brDetails.ShiftDetails;

                btnAssignShift.Enabled = true;
            }
        }

        private void ShowAssignDialog()
        {
            long bankReceiptNo;
            BankReceiptDetailsDTO brDetails;
            if (ValidateReceiptNo(out bankReceiptNo, out brDetails, true))
            {
                var assignDialog = new BankReceiptShiftAssignmentDialog(bankReceiptNo);
                if(assignDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.FillDetails();
                }
            }
        }

        private bool ValidateReceiptNo(out long bankReceiptNo, out BankReceiptDetailsDTO brDetails, bool checkForClosure)
        {
            bankReceiptNo = 0;
            brDetails = new BankReceiptDetailsDTO();
            ClearValidationError();
            if (string.IsNullOrEmpty(txtBankReceiptNumber.Text.Trim()))
            {
                ShowValidationError("Bank receipt number is required.", txtBankReceiptNumber);
                return false;
            }
            if (!long.TryParse(txtBankReceiptNumber.Text.Trim().Trim(), out bankReceiptNo))
            {
                ShowValidationError("Bank receipt number is invalid.", txtBankReceiptNumber);
                return false;
            }
            var repository = new Repository();
            brDetails = repository.GetBRDetailsFromReceiptNo(bankReceiptNo);
            if (brDetails == null || brDetails.BankReceiptNo == 0)
            {
                ShowErrorNotification("Invalid Bank receipt no.");
                return false;
            }
            if (checkForClosure && brDetails.Status == ReceiptStatus.Closed)
            {
                MessageBox.Show("Selected bank receipt is closed and no further corrections or shift assignment can be made.", "Closed Receipt", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }
    }
}
