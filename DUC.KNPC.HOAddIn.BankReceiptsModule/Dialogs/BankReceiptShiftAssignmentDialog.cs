﻿using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.Dialogs
{
    public partial class BankReceiptShiftAssignmentDialog : Form
    {
        private long _bankReceiptId;
        public BankReceiptShiftAssignmentDialog(long receiptId)
        {
            InitializeComponent();
            _bankReceiptId = receiptId;
            lblShiftAssignment.Text = string.Format("Assign Station Shift To Bank Receipt: {0}", _bankReceiptId.ToString());
            var repository = new Repository();
            cmbStation.DataSource = repository.GetAllSites();
            cmbStation.DisplayMember = "Site_Name";
            cmbStation.ValueMember = "Site_ID";
        }

        private void OnSearchClicked(object sender, EventArgs e)
        {
            var repository = new Repository();
            dgvAssignments.AutoGenerateColumns = false;
            dgvAssignments.DataSource = repository.GetShiftsForBRAssignment(_bankReceiptId, new ShiftDetailsDTO
                {
                    AccountDate = dpAccountDate.Value,
                    StationId = int.Parse(cmbStation.SelectedValue.ToString()),
                }, chkUnassignedShiftsOnly.Checked);
        }

        private void OnSaveClicked(object sender, EventArgs e)
        {
            var repo = new Repository();
            repo.SaveBRShiftAssignmentMapping(dgvAssignments.DataSource as List<BankReceiptShiftAssignmentDTO>, _bankReceiptId);
            MessageBox.Show("Shifts successfully assigned to bank receipt.");
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
