﻿namespace DUC.KNPC.HOAddIn.BankReceiptsModule.Dialogs
{
    partial class BankReceiptShiftAssignmentDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblShiftAssignment = new System.Windows.Forms.Label();
            this.cmbStation = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dpAccountDate = new System.Windows.Forms.DateTimePicker();
            this.btnSearch = new System.Windows.Forms.Button();
            this.chkUnassignedShiftsOnly = new System.Windows.Forms.CheckBox();
            this.dgvAssignments = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.StationId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShiftNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EODNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AssignedBR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Assign = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssignments)).BeginInit();
            this.SuspendLayout();
            // 
            // lblShiftAssignment
            // 
            this.lblShiftAssignment.AutoSize = true;
            this.lblShiftAssignment.Location = new System.Drawing.Point(12, 19);
            this.lblShiftAssignment.Name = "lblShiftAssignment";
            this.lblShiftAssignment.Size = new System.Drawing.Size(182, 13);
            this.lblShiftAssignment.TabIndex = 0;
            this.lblShiftAssignment.Text = "Assign Station Shift To Bank Receipt";
            // 
            // cmbStation
            // 
            this.cmbStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStation.FormattingEnabled = true;
            this.cmbStation.Location = new System.Drawing.Point(178, 64);
            this.cmbStation.Name = "cmbStation";
            this.cmbStation.Size = new System.Drawing.Size(182, 21);
            this.cmbStation.TabIndex = 45;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(175, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 46;
            this.label6.Text = "Station Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Account Date";
            // 
            // dpAccountDate
            // 
            this.dpAccountDate.Checked = false;
            this.dpAccountDate.CustomFormat = "dd/MM/yyyy";
            this.dpAccountDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpAccountDate.Location = new System.Drawing.Point(15, 65);
            this.dpAccountDate.Name = "dpAccountDate";
            this.dpAccountDate.Size = new System.Drawing.Size(115, 20);
            this.dpAccountDate.TabIndex = 44;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(396, 64);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 47;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.OnSearchClicked);
            // 
            // chkUnassignedShiftsOnly
            // 
            this.chkUnassignedShiftsOnly.AutoSize = true;
            this.chkUnassignedShiftsOnly.Location = new System.Drawing.Point(15, 103);
            this.chkUnassignedShiftsOnly.Name = "chkUnassignedShiftsOnly";
            this.chkUnassignedShiftsOnly.Size = new System.Drawing.Size(135, 17);
            this.chkUnassignedShiftsOnly.TabIndex = 48;
            this.chkUnassignedShiftsOnly.Text = "Unassigned Shifts Only";
            this.chkUnassignedShiftsOnly.UseVisualStyleBackColor = true;
            // 
            // dgvAssignments
            // 
            this.dgvAssignments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAssignments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StationId,
            this.AccountDate,
            this.ShiftNo,
            this.EODNo,
            this.AssignedBR,
            this.Assign});
            this.dgvAssignments.Location = new System.Drawing.Point(15, 140);
            this.dgvAssignments.Name = "dgvAssignments";
            this.dgvAssignments.RowHeadersVisible = false;
            this.dgvAssignments.Size = new System.Drawing.Size(565, 296);
            this.dgvAssignments.TabIndex = 49;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(587, 410);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 50;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.OnSaveClicked);
            // 
            // StationId
            // 
            this.StationId.DataPropertyName = "StationId";
            this.StationId.HeaderText = "Station Number";
            this.StationId.Name = "StationId";
            this.StationId.ReadOnly = true;
            // 
            // AccountDate
            // 
            this.AccountDate.DataPropertyName = "AccountDate";
            this.AccountDate.HeaderText = "Account Date";
            this.AccountDate.Name = "AccountDate";
            this.AccountDate.ReadOnly = true;
            // 
            // ShiftNo
            // 
            this.ShiftNo.DataPropertyName = "ShiftNo";
            this.ShiftNo.HeaderText = "ShiftNo";
            this.ShiftNo.Name = "ShiftNo";
            this.ShiftNo.ReadOnly = true;
            // 
            // EODNo
            // 
            this.EODNo.DataPropertyName = "EODNo";
            this.EODNo.HeaderText = "EODNo";
            this.EODNo.Name = "EODNo";
            this.EODNo.ReadOnly = true;
            this.EODNo.Visible = false;
            // 
            // AssignedBR
            // 
            this.AssignedBR.DataPropertyName = "AssignedBR";
            this.AssignedBR.HeaderText = "Assigned BR";
            this.AssignedBR.Name = "AssignedBR";
            this.AssignedBR.ReadOnly = true;
            // 
            // Assign
            // 
            this.Assign.DataPropertyName = "IsAssigned";
            this.Assign.HeaderText = "Assign";
            this.Assign.Name = "Assign";
            // 
            // BankReceiptShiftAssignmentDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 445);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgvAssignments);
            this.Controls.Add(this.chkUnassignedShiftsOnly);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.cmbStation);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dpAccountDate);
            this.Controls.Add(this.lblShiftAssignment);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "BankReceiptShiftAssignmentDialog";
            this.Text = "Shift Assignment";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssignments)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblShiftAssignment;
        private System.Windows.Forms.ComboBox cmbStation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dpAccountDate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox chkUnassignedShiftsOnly;
        private System.Windows.Forms.DataGridView dgvAssignments;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn StationId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShiftNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn EODNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AssignedBR;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Assign;
    }
}