﻿using DUC.KNPC.HOAddIn.DataAccess;
using System;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.Dialogs
{
    public partial class AddBankReceiptDialog : Form
    {
        private string _userName;
        public AddBankReceiptDialog(string userName)
        {
            InitializeComponent();
            _userName = userName;
            errProValidation.Clear();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            errProValidation.Clear();
            if (ValidateData())
            {
                var repo = new Repository();
                repo.AddbankReceipt(new DataAccess.DTOs.BankReceiptDTO
                    {
                        CreatedBy = _userName,
                        BankDate = dpBankDate.Value.Date,
                        BankID = txtBankID.Text.Trim(),
                        BankName = txtBankName.Text.Trim(),
                        Branch = txtBranch.Text.Trim(),
                        Total = double.Parse(txtTotal.Text.Trim())
                    });
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private bool ValidateData()
        {
            if(string.IsNullOrEmpty(txtBankName.Text.Trim()))
            {
                errProValidation.SetError(txtBankName, "Enter Bank Name Value");
                return false;
            }
            if (string.IsNullOrEmpty(txtBankID.Text.Trim()))
            {
                errProValidation.SetError(txtBankID, "Enter Bank ID Value");
                return false;
            }
            if (string.IsNullOrEmpty(txtBranch.Text.Trim()))
            {
                errProValidation.SetError(txtBranch, "Enter Branch No Value");
                return false;
            }
            if (string.IsNullOrEmpty(txtTotal.Text.Trim()))
            {
                errProValidation.SetError(txtTotal, "Enter Total Value");
                return false;
            }
            double total;
            if(!double.TryParse(txtTotal.Text.Trim(), out total))
            {
                errProValidation.SetError(txtTotal, "Total value should be numeric");
                return false;
            }
            if(txtBankName.Text.Trim().Length > 200)
            {
                errProValidation.SetError(txtBankName, "Bank Name should be less than 200 characters");
                return false;
            }
            if (txtBankID.Text.Trim().Length > 200)
            {
                errProValidation.SetError(txtBankID, "Bank ID should be less than 200 characters");
                return false;
            }
            if (txtBranch.Text.Trim().Length > 200)
            {
                errProValidation.SetError(txtBranch, "Branch No should be less than 200 characters");
                return false;
            }
            return true;
        }
    }
}
