﻿using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DUC.KNPC.HOAddIn.BankReceiptsModule.Dialogs
{
    public partial class EditBankReceiptDialog : Form
    {
        BankReceiptDTO _editedItem;
        string _username;
        public EditBankReceiptDialog(BankReceiptListDTO itemToEdit, string username)
        {
            InitializeComponent();
            _username = username;
            var repository = new Repository();
            _editedItem = repository.GetBankReceiptDetailsById(itemToEdit.BankReceiptNo);
            txtBankID.Text = _editedItem.BankID;
            txtBankName.Text = _editedItem.BankName;
            txtBranch.Text = _editedItem.Branch;
            txtTotal.Text = _editedItem.Total.ToString("F3");
            errProValidation.Clear();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            errProValidation.Clear();
            if (ValidateData())
            {
                var repo = new Repository();
                if (!string.IsNullOrEmpty(txtCorrection.Text))
                {
                    repo.SaveBankReceiptCorrection(new BankReceiptCorrectionDTO
                    {
                        CreatedBy = _username,
                        ReceiptDetails = _editedItem,
                        BankReceiptID = _editedItem.BankReceiptId,
                        CorrectedTotal = double.Parse(txtCorrection.Text.Trim()),
                        CorrectionDate = DateTime.Now,
                        CorrectionReason = txtCorrectionReason.Text.Trim()
                    });
                }
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private bool ValidateData()
        {
            if (!string.IsNullOrEmpty(txtCorrection.Text.Trim()) && string.IsNullOrEmpty(txtCorrectionReason.Text.Trim()))
            {
                errProValidation.SetError(txtCorrectionReason, "Correction reason is required when correction made on total.");
                return false;
            }
            double total;
            if (!double.TryParse(txtCorrection.Text.Trim(), out total))
            {
                errProValidation.SetError(txtCorrection, "Correction total value should be numeric");
                return false;
            }
            return true;
        }
    }
}
