﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class BRDetailsShiftDetailsDTO
    {
        public int StationNo { get; set; }
        public DateTime AccountDate { get; set; }
        public int ShiftNo { get; set; }
        public double CashSales { get; set; }
        public double SafeDropTotal { get; set; }
        public double Difference { get; set; }
    }
}
