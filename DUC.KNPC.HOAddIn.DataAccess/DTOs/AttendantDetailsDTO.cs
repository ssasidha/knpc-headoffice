﻿
namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class AttendantDetailsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EmployeeID { get; set; }
        public string IdDisplay
        {
            get
            {
                if (Id > 0)
                    return Id.ToString();
                return "Select All";
            }
        }
    }
}
