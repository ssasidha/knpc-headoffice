﻿using System;
namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class ShiftDetailsDTO
    {
        public string ShiftName { get; set; }
        public int ShiftNo { get; set; }
        public int EODNo { get; set; }
        public int StationId { get; set; }
        public DateTime AccountDate { get; set; }
    }
}
