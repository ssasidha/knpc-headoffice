﻿
namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class AttendantStatusDTO
    {
        public short Id { get; set; }
        public string Name { get; set; }
    }
}
