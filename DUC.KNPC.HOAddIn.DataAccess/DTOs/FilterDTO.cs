﻿using System;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class FilterDTO
    {
        public DateTime AccountDate { get; set; }
        public ShiftDetailsDTO Shift { get; set; }
        public AttendantDetailsDTO Attendant { get; set; }
        public StationDTO Station { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Product { get; set; }
        public PaymentTypeDTO PaymentType {get; set;}
        public ProductGroupDTO ProductType { get; set; }
        public string TerminalID { get; set; }
        public FillingDTO Filling { get; set; }
    }
}