﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class BankReceiptListDTO
    {
        public long BankReceiptNo { get; set; }
        public DateTime ReceiptDate { get; set; }
        public double BankReceiptTotal { get; set; }
        public double ShiftTotal { get; set; }
        public ReceiptStatus Status { get; set; }
    }

    public enum ReceiptStatus : short
    {
        Open = 0,
        Closed = 1
    }
}
