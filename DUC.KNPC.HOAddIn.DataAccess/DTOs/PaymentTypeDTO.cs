﻿
namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class PaymentTypeDTO
    {
        public int PayTNo { get; set; }
        public string PayType { get; set; }
    }
}
