﻿using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class StationDTO
    {
        public int StationId { get; set; }
        public string StationName { get; set; }
    }
}
