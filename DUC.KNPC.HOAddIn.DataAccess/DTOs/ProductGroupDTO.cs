﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
   public class ProductGroupDTO
    {
        public string GroupName { get; set; }
    }
}
