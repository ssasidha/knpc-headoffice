﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class BankReceiptDTO
    {
        public long BankReceiptId { get; set; }
        public string BankName { get; set; }
        public string BankID { get; set; }
        public string Branch { get; set; }
        public DateTime BankDate { get; set; }
        public double Total { get; set; }
        public string CreatedBy { get; set; }
    }
}
