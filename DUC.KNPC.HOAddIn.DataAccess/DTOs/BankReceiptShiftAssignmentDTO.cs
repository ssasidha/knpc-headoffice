﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class BankReceiptShiftAssignmentDTO
    {
        public int ShiftNo { get; set; }
        public int EODNo { get; set; }
        public DateTime AccountDate { get; set; }
        public int StationId { get; set; }
        public string AssignedBR { get; set; }
        public bool IsAssigned { get; set; }
    }
}
