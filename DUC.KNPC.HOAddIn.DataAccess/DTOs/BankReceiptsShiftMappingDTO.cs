﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class BankReceiptsShiftMappingDTO
    {
        public long BankReceiptId { get; set; }
        public List<ShiftDetailsDTO> AssignedShifts { get; set; }
    }
}
