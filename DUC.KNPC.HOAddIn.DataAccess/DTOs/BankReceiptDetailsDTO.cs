﻿using System;
using System.Collections.Generic;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class BankReceiptDetailsDTO
    {
        public long BankReceiptNo { get; set; }
        public string BankName { get; set; }
        public string BankID { get; set; }
        public string Branch { get; set; }
        public double Total { get; set; }
        public double SafeDrop { get; set; }
        public double Difference { get; set; }
        public ReceiptStatus Status { get; set; }
        public DateTime ReceiptDate { get; set; }
        public List<BRDetailsShiftDetailsDTO> ShiftDetails { get; set; }
    }
}
