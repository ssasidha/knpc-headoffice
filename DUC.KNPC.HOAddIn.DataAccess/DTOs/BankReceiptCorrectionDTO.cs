﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class BankReceiptCorrectionDTO
    {
        public long BankReceiptID { get; set; }
        public long CorrectionID { get; set; }
        public double CorrectedTotal { get; set; }
        public string CorrectionReason { get; set; }
        public DateTime? CorrectionDate { get; set; }
        public BankReceiptDTO ReceiptDetails { get; set; }
        public string CreatedBy { get; set; }
    }
}
