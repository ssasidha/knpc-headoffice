﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.KNPC.HOAddIn.DataAccess.DTOs
{
    public class FillingDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
