﻿using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.BankDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using DUC.KNPC.HOAddIn.HOConfig;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DUC.KNPC.HOAddIn.DataAccess
{
    public class Repository
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");
        private string BANK_CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("BankData");

        public StationData.EOD_GetAccountDateDetailsDataTable GetAccountDate(DateTime accountDate, int? stationId)
        {
            var adapter = new EOD_GetAccountDateDetailsTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);
            return adapter.GetData(accountDate, stationId);
        }

        public StationData.EOD_GetAccountDateStartDetailsDataTable GetAccountDateDetails(DateTime accountDate, int? stationId)
        {
            var adapter = new EOD_GetAccountDateStartDetailsTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);
            return adapter.GetData(DateTime.Parse(accountDate.ToShortDateString()), stationId);
        }

        public StationData.AttendantDefDataTable GetAttendants(int stationId)
        {
            var adapter = new AttendantDefTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);
            return adapter.GetAttendantsForStation(stationId);
        }

        public StationData.StationDataTable GetAllSites()
        {
            var adapter = new StationTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);
            return adapter.GetSites();
        }

        public StationData.StationDataTable GetSiteById(int stationId)
        {
             var adapter = new StationTableAdapter();
             adapter.Connection = new SqlConnection(CONNECTION_STRING);
            return adapter.GetSiteById(stationId.ToString());
        }

        public StationData.sp_GetAllTokenTypesDataTable GetOnlineTokenTypes()
        {
            var adapter = new sp_GetAllTokenTypesTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);
            return adapter.GetData();
        }

        public StationData.v_TerminalIDDataTable GetAllTerminals(int stationID)
        {
            var adapter = new v_TerminalIDTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);
            return adapter.GetData(stationID);
        }
       
        public StationData.GetShiftDetailsDataTable GetShiftDetails(DateTime AccountDate, int ShiftNo, int EodNo, int StationId)
        {
            var adapter = new GetShiftDetailsTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);
            return adapter.GetData(AccountDate, ShiftNo, EodNo, StationId);
        }

        public StationData.GetFuelPointsDataTable GetFuelPoints(int stationId)
        {
            var adapter = new GetFuelPointsTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);
            return adapter.GetData(stationId);
        }
        public StationData.Lookup_WetStockDataTable GetProducts(int stationId)
        {
            var adapter = new Lookup_WetStockTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);
            return adapter.GetData(stationId);
        }

        public StationData.sp_getDeviceListDataTable LoadDevices()
        {
            var details = new StationData.sp_getDeviceListDataTable();
            var adapter = new DUC.KNPC.HOAddIn.DataAccess.Datasets.StationDataTableAdapters.sp_getDeviceListTableAdapter();
            adapter.Connection = new SqlConnection(CONNECTION_STRING);
            adapter.Fill(details);
            return details;
        }

        public List<BankReceiptListDTO> GetBankReceipts(DateTime startDate, DateTime endDate, short status)
        {
            var returnData = new List<BankReceiptListDTO>();
            var receipts = new BankData.sp_getBankReceiptsDataTable();
            var adapter = new DUC.KNPC.HOAddIn.DataAccess.Datasets.BankDataTableAdapters.sp_getBankReceiptsTableAdapter();
            adapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
            adapter.Fill(receipts, startDate, endDate, status);
            if (receipts == null || receipts.Rows.Count == 0)
                return returnData;
            foreach (BankData.sp_getBankReceiptsRow row in receipts.Rows)
            {
                returnData.Add(new BankReceiptListDTO
                    {
                        BankReceiptNo = row.ReceiptId,
                        BankReceiptTotal = row.Total,
                        ReceiptDate = row.ReceiptDate,
                        Status = (ReceiptStatus)row.Status,
                        ShiftTotal = row.ShiftTotal
                    });
            }

            return returnData;
        }

        public BankReceiptDTO GetBankReceiptDetailsById(long receiptNo)
        {
            var adapter = new BankReceiptsTableAdapter();
            adapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
            var data = adapter.GetDataBy(receiptNo);
            if(data != null && data.Rows.Count > 0)
            {
                foreach (BankData.BankReceiptsRow row in data.Rows)
                {
                    return new BankReceiptDTO
                    {
                        BankReceiptId = row.ReceiptId,
                        BankDate = row.ReceiptDate,
                        BankID = row.BankID,
                        BankName = row.BankName,
                        Branch = row.Branch,
                        Total = row.Total
                    };
                }
            }
            return new BankReceiptDTO();
        }

        public void AddbankReceipt(BankReceiptDTO dto)
        {
            var adapter = new BankReceiptsTableAdapter();
            adapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
            var data = new BankData.BankReceiptsDataTable();
            adapter.Fill(data);
            var newRow = data.NewBankReceiptsRow();
            newRow.BankID = dto.BankID;
            newRow.BankName = dto.BankName;
            newRow.ReceiptDate = dto.BankDate;
            newRow.Status = (short)ReceiptStatus.Open;
            newRow.Total = dto.Total;
            newRow.Branch = dto.Branch;
            newRow.CreatedBy = dto.CreatedBy;
            data.AddBankReceiptsRow(newRow);
            adapter.Update(data);
        }

        public void SetBankReceiptClosed(long bankReceiptId, string user)
        {
            var adapter = new BankReceiptsTableAdapter();
            adapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
            var data = new BankData.BankReceiptsDataTable();
            data = adapter.GetDataBy(bankReceiptId);
            foreach (BankData.BankReceiptsRow row in data.Rows)
            {
                row.Status = (short)ReceiptStatus.Closed;
                row.ClosedBy = user;
                row.ClosedDate = DateTime.Now.Date;
            }
            adapter.Update(data);
        }

        public bool CanCloseBankReceipt(long bankReceiptId)
        {
            var adapter = new BankReceiptShiftsMappingTableAdapter();
            adapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
            var data = new BankData.BankReceiptShiftsMappingDataTable();
            data = adapter.GetDataById(bankReceiptId);
            return data != null && data.Rows.Count > 0;
        }

        public void SaveBankReceiptCorrection(BankReceiptCorrectionDTO dto)
        {
             var adapter = new BankReceiptsTableAdapter();
            adapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
            var data = new BankData.BankReceiptsDataTable();
            adapter.Fill(data);

            var correctionAdapter = new BankReceiptCorrectionsTableAdapter();
            correctionAdapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
            var correctionData = new BankData.BankReceiptCorrectionsDataTable();
            correctionAdapter.Fill(correctionData);
            var newCorrectionRow = correctionData.NewBankReceiptCorrectionsRow();
            var dataToUpdate = adapter.GetDataBy(dto.BankReceiptID);
            foreach (BankData.BankReceiptsRow row in dataToUpdate.Rows)
            {
                row.Total = dto.CorrectedTotal;
                break;
            }
            var bankReceiptsRow = data.Rows.Find(dto.BankReceiptID) as BankData.BankReceiptsRow;
            bankReceiptsRow.Total = dto.CorrectedTotal;
            newCorrectionRow.BankReceiptsRow = bankReceiptsRow;
            newCorrectionRow.CorrectedTotal = dto.CorrectedTotal;
            newCorrectionRow.CorrectionDate = dto.CorrectionDate.Value;
            newCorrectionRow.CorrectionReason = dto.CorrectionReason;
            newCorrectionRow.OldTotal = dto.ReceiptDetails.Total;
            newCorrectionRow.ReceiptId = dto.BankReceiptID;
            newCorrectionRow.CreatedBy = dto.CreatedBy;
            correctionData.Rows.Add(newCorrectionRow);
            correctionAdapter.Update(correctionData);
            adapter.Update(dataToUpdate);
        }

        public List<BankReceiptShiftAssignmentDTO> GetShiftsForBRAssignment(long receiptNo, ShiftDetailsDTO dto, bool checkUnAssignedOnly)
        {
            var returnData = new List<BankReceiptShiftAssignmentDTO>();
            var adapter = new sp_GetShiftsForBRAssignmentTableAdapter();
            adapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);

            var data = new BankData.sp_GetShiftsForBRAssignmentDataTable();
            adapter.Fill(data, receiptNo, dto.AccountDate, dto.StationId, checkUnAssignedOnly);

            if(data == null || data.Rows.Count <= 0)
            {
                return returnData;
            }

            foreach (BankData.sp_GetShiftsForBRAssignmentRow row in data.Rows)
            {
                bool alreadyAssigned = false;
                var rowDto = new BankReceiptShiftAssignmentDTO
                    {
                        AccountDate = row.accountDate,
                        AssignedBR = row.assignedBR,
                        EODNo = row.eodNo,
                        ShiftNo = row.shiftNo,
                        StationId = row.stationId
                    };
                if (!string.IsNullOrEmpty(row.assignedBR))
                {
                    var splitReceiptNos = row.assignedBR.Split(',');
                    foreach (var rcptNo in splitReceiptNos)
                    {
                        if (rcptNo.Trim() == receiptNo.ToString())
                        {
                            rowDto.IsAssigned = true;
                            alreadyAssigned = true;
                            break;
                        }
                    }
                }
                if (checkUnAssignedOnly && alreadyAssigned)
                    continue;
                returnData.Add(rowDto);
            }

            return returnData;
        }

        public void SaveBRShiftAssignmentMapping(List<BankReceiptShiftAssignmentDTO> shiftAssignments, long receiptNo)
        {
            var adapter = new BankReceiptShiftsMappingTableAdapter();
            adapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
            var data = new BankData.BankReceiptShiftsMappingDataTable();
            data = adapter.GetDataById(receiptNo);
           
            foreach (var shiftAssigment in shiftAssignments)
            {
                bool isRowAlreadyAssigned = false;
                if (!string.IsNullOrEmpty(shiftAssigment.AssignedBR))
                {
                    var splitData = shiftAssigment.AssignedBR.Split(',');
                    foreach (var rcptNo in splitData)
                    {
                        if(rcptNo.Trim() == receiptNo.ToString())
                        {
                            isRowAlreadyAssigned = true;
                            break;
                        }
                    }
                }

                if (shiftAssigment.IsAssigned && !isRowAlreadyAssigned)
                {
                    BankData.BankReceiptShiftsMappingRow newRow = data.NewBankReceiptShiftsMappingRow();
                    newRow.AccountDate = shiftAssigment.AccountDate;
                    newRow.EODNo = shiftAssigment.EODNo;
                    newRow.ShiftNo = shiftAssigment.ShiftNo;
                    newRow.StationId = shiftAssigment.StationId;
                    newRow.ReceiptId = receiptNo;
                    data.Rows.Add(newRow);
                }
                else if (!shiftAssigment.IsAssigned && isRowAlreadyAssigned)
                {
                    foreach (BankData.BankReceiptShiftsMappingRow row in data.Rows)
                    {
                        if(row.AccountDate == shiftAssigment.AccountDate && row.EODNo == shiftAssigment.EODNo && row.ShiftNo == shiftAssigment.ShiftNo && row.StationId == shiftAssigment.StationId)
                        {
                            row.Delete();
                        }
                    }
                }
            }

            adapter.Update(data);
        }

        public void DeleteBankReceiptRecord(long bankReceiptNo)
        {
            var adapter = new BankReceiptsTableAdapter();
            adapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
            var data = new BankData.BankReceiptsDataTable();
            data = adapter.GetDataBy(bankReceiptNo);
            foreach (BankData.BankReceiptsRow row in data.Rows)
            {
                row.Delete();
            }
            adapter.Update(data);
        }

        public BankReceiptDetailsDTO GetBRDetailsFromReceiptNo(long receiptNo)
        {
            var bankReceiptDetails = new BankReceiptDetailsDTO();
            var adapter = new sp_GetBankReceiptDetailsByNoTableAdapter();
            adapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
            var data = adapter.GetData(receiptNo);
            if(data == null || data.Rows.Count <= 0)
            {
                return bankReceiptDetails;
            }

            foreach (BankData.sp_GetBankReceiptDetailsByNoRow row in data.Rows)
            {
                bankReceiptDetails.BankID = row.BankID;
                bankReceiptDetails.BankName = row.BankName;
                bankReceiptDetails.BankReceiptNo = row.ReceiptId;
                bankReceiptDetails.Branch = row.Branch;
                bankReceiptDetails.Difference = row.Difference;
                bankReceiptDetails.ReceiptDate = row.ReceiptDate;
                bankReceiptDetails.SafeDrop = row.SafeDrop;
                bankReceiptDetails.Total = row.Total;
                bankReceiptDetails.Status = (ReceiptStatus)row.Status;
                // get shiftDetails
                var shiftAdapter = new sp_GetBRShiftDetailsByNoTableAdapter();
                shiftAdapter.Connection = new SqlConnection(BANK_CONNECTION_STRING);
                var shiftDetails = shiftAdapter.GetData(receiptNo);
                if(shiftDetails != null && shiftDetails.Count > 0)
                {
                    bankReceiptDetails.ShiftDetails = new List<BRDetailsShiftDetailsDTO>();
                    foreach (BankData.sp_GetBRShiftDetailsByNoRow shiftRow in shiftDetails.Rows)
                    {
                        bankReceiptDetails.ShiftDetails.Add(new BRDetailsShiftDetailsDTO
                            {
                                AccountDate = shiftRow.accountDate,
                                CashSales = shiftRow.CashSales,
                                Difference = shiftRow.Diference,
                                SafeDropTotal = shiftRow.SafeDropTotal,
                                ShiftNo = shiftRow.shiftNo,
                                StationNo = shiftRow.stationId
                            });
                    }
                }
                break;
            }

            return bankReceiptDetails;
        }

    }
}