﻿using System.ComponentModel;
using System.Windows.Forms;
using DUC.KNPC.AttendantModule.DTO;

namespace DUC.KNPC.AttendantModule.UserControls
{
    partial class AttendantModuleHome
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.vacationStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.attednantStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.MainViewGridControl = new System.Windows.Forms.DataGridView();
            this.spGetAttendantDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.attendantLookup = new DUC.KNPC.AttendantModule.DAL.AttendantLookUpxsd();
            this.sp_GetAttendantDetailsTableAdapter = new DUC.KNPC.AttendantModule.DAL.AttendantLookUpxsdTableAdapters.sp_GetAttendantDetailsTableAdapter();
            this.stn00BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stn00TableAdapter = new DUC.KNPC.AttendantModule.DAL.AttendantLookUpxsdTableAdapters.Stn00TableAdapter();
            this.StationID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.attendantIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.attendantFirstNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPLCIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pumplist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.vacationStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attednantStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainViewGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spGetAttendantDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attendantLookup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stn00BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // vacationStatusBindingSource
            // 
            this.vacationStatusBindingSource.DataSource = typeof(DUC.KNPC.AttendantModule.DTO.VacationStatus);
            // 
            // attednantStatusBindingSource
            // 
            this.attednantStatusBindingSource.DataSource = typeof(DUC.KNPC.AttendantModule.DTO.AttednantStatus);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // MainViewGridControl
            // 
            this.MainViewGridControl.AllowDrop = true;
            this.MainViewGridControl.AllowUserToAddRows = false;
            this.MainViewGridControl.AllowUserToOrderColumns = true;
            this.MainViewGridControl.AutoGenerateColumns = false;
            this.MainViewGridControl.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.MainViewGridControl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MainViewGridControl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StationID,
            this.attendantIDDataGridViewTextBoxColumn,
            this.attendantFirstNameDataGridViewTextBoxColumn,
            this.cPLCIDDataGridViewTextBoxColumn,
            this.Pumplist});
            this.MainViewGridControl.DataSource = this.spGetAttendantDetailsBindingSource;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MainViewGridControl.DefaultCellStyle = dataGridViewCellStyle1;
            this.MainViewGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainViewGridControl.Location = new System.Drawing.Point(0, 0);
            this.MainViewGridControl.MultiSelect = false;
            this.MainViewGridControl.Name = "MainViewGridControl";
            this.MainViewGridControl.ReadOnly = true;
            this.MainViewGridControl.RowHeadersVisible = false;
            this.MainViewGridControl.Size = new System.Drawing.Size(1032, 437);
            this.MainViewGridControl.TabIndex = 14;
            this.MainViewGridControl.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MainViewGridControl_CellContentClick);
            // 
            // spGetAttendantDetailsBindingSource
            // 
            this.spGetAttendantDetailsBindingSource.DataMember = "sp_GetAttendantDetails";
            this.spGetAttendantDetailsBindingSource.DataSource = this.attendantLookup;
            // 
            // attendantLookup
            // 
            this.attendantLookup.DataSetName = "AttendantLookup";
            this.attendantLookup.EnforceConstraints = false;
            this.attendantLookup.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_GetAttendantDetailsTableAdapter
            // 
            this.sp_GetAttendantDetailsTableAdapter.ClearBeforeFill = true;
            // 
            // stn00BindingSource
            // 
            this.stn00BindingSource.DataMember = "Stn00";
            this.stn00BindingSource.DataSource = this.attendantLookup;
            // 
            // stn00TableAdapter
            // 
            this.stn00TableAdapter.ClearBeforeFill = true;
            // 
            // StationID
            // 
            this.StationID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StationID.DataPropertyName = "StationID";
            this.StationID.FillWeight = 6.285794F;
            this.StationID.HeaderText = "Station";
            this.StationID.Name = "StationID";
            this.StationID.ReadOnly = true;
            // 
            // attendantIDDataGridViewTextBoxColumn
            // 
            this.attendantIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.attendantIDDataGridViewTextBoxColumn.DataPropertyName = "AttendantID";
            this.attendantIDDataGridViewTextBoxColumn.FillWeight = 10.285794F;
            this.attendantIDDataGridViewTextBoxColumn.HeaderText = "Attendant ID";
            this.attendantIDDataGridViewTextBoxColumn.MinimumWidth = 50;
            this.attendantIDDataGridViewTextBoxColumn.Name = "attendantIDDataGridViewTextBoxColumn";
            this.attendantIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // attendantFirstNameDataGridViewTextBoxColumn
            // 
            this.attendantFirstNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.attendantFirstNameDataGridViewTextBoxColumn.DataPropertyName = "AttendantFirstName";
            this.attendantFirstNameDataGridViewTextBoxColumn.FillWeight = 15.71449F;
            this.attendantFirstNameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.attendantFirstNameDataGridViewTextBoxColumn.Name = "attendantFirstNameDataGridViewTextBoxColumn";
            this.attendantFirstNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cPLCIDDataGridViewTextBoxColumn
            // 
            this.cPLCIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cPLCIDDataGridViewTextBoxColumn.DataPropertyName = "cPLCID";
            this.cPLCIDDataGridViewTextBoxColumn.FillWeight = 35.71449F;
            this.cPLCIDDataGridViewTextBoxColumn.HeaderText = "Attendant Card";
            this.cPLCIDDataGridViewTextBoxColumn.Name = "cPLCIDDataGridViewTextBoxColumn";
            this.cPLCIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Pumplist
            // 
            this.Pumplist.DataPropertyName = "Pumplist";
            this.Pumplist.FillWeight = 42.85794F;
            this.Pumplist.HeaderText = "Pump Assignment";
            this.Pumplist.Name = "Pumplist";
            this.Pumplist.ReadOnly = true;
            // 
            // AttendantModuleHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainViewGridControl);
            this.Name = "AttendantModuleHome";
            this.Size = new System.Drawing.Size(1032, 437);
            this.Load += new System.EventHandler(this.AttendantModuleHome_Load);
            ((System.ComponentModel.ISupportInitialize)(this.vacationStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attednantStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainViewGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spGetAttendantDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attendantLookup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stn00BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BindingSource spGetAttendantDetailsBindingSource;
        private DUC.KNPC.AttendantModule.DAL.AttendantLookUpxsd attendantLookup;
        private DUC.KNPC.AttendantModule.DAL.AttendantLookUpxsdTableAdapters.sp_GetAttendantDetailsTableAdapter sp_GetAttendantDetailsTableAdapter;
        private BindingSource stn00BindingSource;
        private DAL.AttendantLookUpxsdTableAdapters.Stn00TableAdapter stn00TableAdapter;
        private BindingSource vacationStatusBindingSource;
        private BindingSource attednantStatusBindingSource;
        private ErrorProvider errorProvider1;
        private DataGridView MainViewGridControl;
        private DataGridViewTextBoxColumn StationID;
        private DataGridViewTextBoxColumn attendantIDDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn attendantFirstNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn cPLCIDDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn Pumplist;

    }
}
