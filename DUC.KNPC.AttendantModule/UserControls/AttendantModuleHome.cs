﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DUC.KNPC.AttendantModule.DTO;
using DUC.KNPC.HOAddIn.HOConfig;

//◄ = &#9668; ► = &#9658; ▼ = &#9660; ▲ = &#9650;
namespace DUC.KNPC.AttendantModule.UserControls
{
    public partial class AttendantModuleHome : SupportToolbarControlBase
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("StationData");


        #region Collapse Filters
        //private void buttonCollapse_Click(object sender, EventArgs e)
        //{
        //    if (IsCollapsed())
        //    {
        //        CollapseFilters();
        //    }
        //    else
        //    {
        //        ExpandFilters();
        //    }
        //}

        //private bool IsCollapsed()
        //{
        //    return groupBoxFilters.Visible;
        //}

        //private void ExpandFilters()
        //{
        //    tableLayoutPanel.RowStyles[0].Height = 139;
        //    groupBoxFilters.Visible = true;
        //    buttonCollapse.Text = @"▲";
        //}

        //private void CollapseFilters()
        //{
        //    tableLayoutPanel.RowStyles[0].Height = 0;
        //    groupBoxFilters.Visible = false;
        //    buttonCollapse.Text = @"▼";
        //}
        #endregion

        public AttendantModuleHome()
        {
            InitializeComponent();
            MainViewGridControl.AutoGenerateColumns = false;
            MainViewGridControl.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
     
        //private void LoadComboBoxes()
        //{
        //    stn00TableAdapter.Fill(attendantLookup.Stn00);
        //    FillAttendantStatus();
        //    FillAttendantVacationStatus();
        //}
        
        private void AttendantModuleHome_Load(object sender, EventArgs e)
        {
            Dock=DockStyle.Fill;
            MainViewGridControl.Dock = DockStyle.Fill;
            Search();
            //LoadComboBoxes();
//            tableLayoutPanel.RowStyles[0].Height = 139;

        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                //decimal siteID;
                //errorProvider1.Clear();
                sp_GetAttendantDetailsTableAdapter.Connection = new System.Data.SqlClient.SqlConnection(CONNECTION_STRING);
                //sp_GetAttendantDetailsTableAdapter.Fill(attendantLookup.sp_GetAttendantDetails,
                //    decimal.TryParse(comboBoxSiteID.SelectedValue.ToString(), out siteID) ? siteID : (decimal?)null,
                //    (bool?)comboBoxAttednantStatus.SelectedValue, textBox1.Text, (bool?)comboBoxOnVacation.SelectedValue);
                sp_GetAttendantDetailsTableAdapter.Fill(attendantLookup.sp_GetAttendantDetails);
            }
            catch (Exception exception)
            {

                //errorProvider1.SetError(buttonSearch, exception.Message);
            }
           
        }

        private void FillAttendantStatus()
        {
            var statusList = new List<AttednantStatus>
            {
                new AttednantStatus() {StatusDescription = "All", Value = null},
                new AttednantStatus() {StatusDescription = "Active", Value = true},
                new AttednantStatus() {StatusDescription = "Disabled", Value = false}
            };
            attednantStatusBindingSource.DataSource = statusList;
        }
        private void FillAttendantVacationStatus()
        {
            var statusList = new List<VacationStatus>
            {
                new VacationStatus() {StatusDescription = "All", Value = null},
                new VacationStatus() {StatusDescription = "Present", Value = true},
                new VacationStatus() {StatusDescription = "On Vacation", Value = false}
            };
            vacationStatusBindingSource.DataSource = statusList;
        }

        private void comboBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                Search();
            }
        }

        private void MainViewGridControl_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
