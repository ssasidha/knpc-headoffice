namespace DUC.KNPC.AttendantModule.DTO
{
    public class ComboxBoxItem
    {
        public string StatusDescription { get; set; }
        public bool? Value { get; set; }
    }
}