﻿using DUC.KNPC.HOAddIn.DataAccess;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DataAccess.DTOs;
using Namos.Addin.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DUC.KNPC.AttendantModule
{
    public partial class SupportToolbarControlBase : UserControl, ISupportToolbarCommands
    {
        #region "Constants"

        protected const string _ADDIN_COMMAND_NEW = "AddNewObject";
        protected const string _ADDIN_COMMAND_DELETE = "Delete";
        protected const string _ADDIN_COMMAND_SAVE = "SaveObject";
        protected const string _ADDIN_COMMAND_REFRESH = "Refresh";
        protected const string _ADDIN_COMMAND_SHOW = "ShowObject";

        const string _SELECT_ALL = "Select All";
        #endregion

        #region "Protected Members"

        protected Hashtable _CommandStates = new Hashtable(); 

        #endregion

        #region "Constructors"

        public SupportToolbarControlBase()
        {
            InitializeComponent();
            InitCommandStates();
        } 

        #endregion

        #region "ISupportToolbarCommands Implementation"

        public virtual void DoCommand(string command)
        {
            if (command == _ADDIN_COMMAND_REFRESH)
                RefreshState();
        }

        public bool IsCommandSupported(string command)
        {
            return _CommandStates.ContainsKey(command) && bool.Parse(_CommandStates[command].ToString());
        }

        public event RefreshToolbarDelegate OnRefreshToolbar; 

        #endregion

        #region "Protected Methods"

        protected virtual void RefreshState()
        {

        }

        protected virtual void InitCommandStates()
        {
            // toolbar button states.
            _CommandStates[_ADDIN_COMMAND_DELETE] = false;
            _CommandStates[_ADDIN_COMMAND_NEW] = false;
            _CommandStates[_ADDIN_COMMAND_REFRESH] = true;
            _CommandStates[_ADDIN_COMMAND_SAVE] = false;
            _CommandStates[_ADDIN_COMMAND_SHOW] = false;
        }

        protected void ShowErrorNotification(string errorMessage)
        {
            MessageBox.Show(errorMessage, "Invalid Data", MessageBoxButtons.OK);
        } 

        protected void ShowValidationError(string errorMessage, Control validatedControl)
        {
            errorValidator.Clear();
            errorValidator.SetError(validatedControl, errorMessage);
        }

        protected List<AttendantDetailsDTO> GetAttendants(int stationId)
        {
            var repository = new Repository();
            var attendantList = GetEmptyAttendants();
            var queryResult = repository.GetAttendants(stationId);
            if (queryResult != null && queryResult.Rows.Count > 0)
            {
                foreach (StationData.AttendantDefRow row in queryResult.Rows)
                {
                    attendantList.Add(new AttendantDetailsDTO
                        {
                            Id = row.AttendantID,
                            EmployeeID = row.EmployeeID,
                            Name = row.AttendantName
                        });
                }
            }

            return attendantList;
        }
        protected List<AttendantDetailsDTO> GetEmptyAttendants()
        {
            var attendantList = new List<AttendantDetailsDTO>
            {
                new AttendantDetailsDTO
                {
                    Id = -1,
                    Name = _SELECT_ALL
                }
            };
            return attendantList;
        }

        protected List<ShiftDetailsDTO> GetShiftDetails(DateTime accountDate, int? stationId)
        {
            var repository = new Repository();
            var shiftList = GetEmptyShiftDetails();
            var queryResult = repository.GetAccountDate(accountDate, stationId);
            if (queryResult == null || queryResult.Rows.Count <= 0) return shiftList;
            foreach (StationData.EOD_GetAccountDateDetailsRow row in queryResult.Rows)
            {
                shiftList.Add(new ShiftDetailsDTO
                {
                    EODNo = row.eodno,
                    ShiftNo = row.shiftno,
                    ShiftName = row.shiftno.ToString()
                });
            }

            return shiftList;
        }

        protected List<ShiftDetailsDTO> GetEmptyShiftDetails()
        {
            var shiftList = new List<ShiftDetailsDTO>
            {
                new ShiftDetailsDTO
                {
                    EODNo = -1,
                    ShiftNo = -1,
                    ShiftName = _SELECT_ALL
                }
            };
            return shiftList;
        }

        protected List<StationDTO> GetAllStations()
        {
            var repository = new Repository();
            var stationList = new List<StationDTO>();
            var queryResult = repository.GetAllSites();

            stationList.Add(new StationDTO
                {
                    StationId = -1,
                    StationName = _SELECT_ALL
                });

            if (queryResult != null && queryResult.Rows.Count > 0)
            {
                foreach (StationData.StationRow row in queryResult)
                {
                    var stationid = 0;
                    stationList.Add(new StationDTO
                        {
                            StationId = int.TryParse(row.Site_ID,out stationid) ? stationid : 0,
                            StationName = row.Site_Name
                        });
                }
            }

            return stationList;
        }

        protected StationDTO GetStationById(int stationId)
        {
            var repository = new Repository();
            var stationList = new List<StationDTO>();
            var queryResult = repository.GetSiteById(stationId);
            if (queryResult == null || queryResult.Rows.Count == 0)
                return null;

            StationData.StationRow row = null;
            foreach (StationData.StationRow rowData in queryResult.Rows)
            {
                row = rowData;
                break;
            }

            return new StationDTO
            {
                StationId = int.Parse(row.Site_ID),
                StationName = row.Site_Name
            };
        }

        protected List<PaymentTypeDTO> GetAllOnlineTokenTypes(int? stationId)
        {
            var repository = new Repository();
            var paymentTypeList = new List<PaymentTypeDTO>();
            var queryresult = repository.GetOnlineTokenTypes();

            paymentTypeList.Add(new PaymentTypeDTO
                {
                    PayTNo = -1,
                    PayType = _SELECT_ALL
                });

            foreach (StationData.sp_GetAllTokenTypesRow row in queryresult.Rows)
            {
                paymentTypeList.Add(new PaymentTypeDTO
                    {
                        PayTNo = row.PayTNo,
                        PayType = row.PayType
                    });
            }

            return paymentTypeList;
        }

        #endregion
    }
}
