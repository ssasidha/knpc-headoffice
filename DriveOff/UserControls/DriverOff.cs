﻿using System;
using System.Data;
using System.Windows.Forms;
using DUC.KNPC.HOAddIn.DataAccess.Datasets;
using DUC.KNPC.HOAddIn.DriveOff.DAL;
using DUC.KNPC.HOAddIn.DriveOff.Forms;
using DUC.KNPC.DriveOff;
using DUC.KNPC.HOAddIn.HOConfig;

namespace DUC.KNPC.HOAddIn.DriveOff.UserControls
{
    public partial class DriverOff : SupportToolbarControlBase
    {
        private string CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("DriveOff");
        private string HOS_CONNECTION_STRING = ApplicationConfiguration.GetConfigurationOrDetault<string>("HOS");

        public DriverOff()
        {
            InitializeComponent();
        }
        #region Collapse Filters
        private void buttonCollapse_Click(object sender, EventArgs e)
        {
            if (IsCollapsed())
            {
                CollapseFilters();
            }
            else
            {
                ExpandFilters();
            }
        }

        private bool IsCollapsed()
        {   return groupBoxFilters.Visible;
        }

        private void ExpandFilters()
        {
            tableLayoutPanel.RowStyles[0].Height = 110;
            groupBoxFilters.Visible = true;
            buttonCollapse.Text = @"▲";
        }

        private void CollapseFilters()
        {
            tableLayoutPanel.RowStyles[0].Height = 0;
            groupBoxFilters.Visible = false;
            buttonCollapse.Text = @"▼";
        }
        #endregion

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            #region Validation

            int temp;
            if (textBox3.Text.Length>0 && !int.TryParse(textBox3.Text,out temp))
            {
                errorProvider1.SetError(textBox3, "Please enter a numeric value");
                return;
            }

            #endregion

            int siteID,receiptNo;
            sp_getDriveOffListTableAdapter.Connection = new System.Data.SqlClient.SqlConnection(CONNECTION_STRING);
            sp_getDriveOffListTableAdapter.Fill(driveOffDataSet.sp_getDriveOffList, int.TryParse(comboBox2.SelectedValue.ToString().Trim(), out siteID) ? siteID : (int?)null,
                dateTimePicker1.Checked? dateTimePicker1.Value.Date:(DateTime?)null, int.TryParse(textBox3.Text.ToString(), out receiptNo) ? receiptNo:(int?)null);
        }

        private void DriverOff_Load(object sender, EventArgs e)
        {
            Dock = DockStyle.Fill;
            stn00TableAdapter.Connection = new System.Data.SqlClient.SqlConnection(HOS_CONNECTION_STRING);
            stn00TableAdapter.Fill(driveOffDataSet.Stn00);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (spgetDriveOffListBindingSource.Current != null)
            {
                var row =
                    (DAL.DriveOffDataSet.sp_getDriveOffListRow)((DataRowView)spgetDriveOffListBindingSource.Current).Row;
                var detailsForm = new DriveOffDetails(row);
                detailsForm.ShowDialog();
            }
        }
    }
}
