﻿namespace DUC.KNPC.HOAddIn.DriveOff.UserControls
{
    partial class DriverOff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBoxFilters = new System.Windows.Forms.GroupBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.stn00BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.driveOffDataSet = new DUC.KNPC.HOAddIn.DriveOff.DAL.DriveOffDataSet();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.StationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TicketNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttendantName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransactionTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountPaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreatedBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberOfHOSNotesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spgetDriveOffListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buttonCollapse = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.stn00TableAdapter = new DUC.KNPC.HOAddIn.DriveOff.DAL.DriveOffDataSetTableAdapters.Stn00TableAdapter();
            this.sp_getDriveOffListTableAdapter = new DUC.KNPC.HOAddIn.DriveOff.DAL.DriveOffDataSetTableAdapters.sp_getDriveOffListTableAdapter();
            this.groupBoxFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stn00BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.driveOffDataSet)).BeginInit();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgetDriveOffListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxFilters
            // 
            this.groupBoxFilters.Controls.Add(this.comboBox2);
            this.groupBoxFilters.Controls.Add(this.dateTimePicker1);
            this.groupBoxFilters.Controls.Add(this.buttonSearch);
            this.groupBoxFilters.Controls.Add(this.textBox3);
            this.groupBoxFilters.Controls.Add(this.label5);
            this.groupBoxFilters.Controls.Add(this.label3);
            this.groupBoxFilters.Controls.Add(this.label2);
            this.groupBoxFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxFilters.Location = new System.Drawing.Point(3, 3);
            this.groupBoxFilters.Name = "groupBoxFilters";
            this.groupBoxFilters.Size = new System.Drawing.Size(971, 151);
            this.groupBoxFilters.TabIndex = 0;
            this.groupBoxFilters.TabStop = false;
            this.groupBoxFilters.Text = "Drive Off Report";
            // 
            // comboBox2
            // 
            this.comboBox2.DataSource = this.stn00BindingSource;
            this.comboBox2.DisplayMember = "site_name";
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(20, 40);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(277, 21);
            this.comboBox2.TabIndex = 10;
            this.comboBox2.ValueMember = "site_id";
            // 
            // stn00BindingSource
            // 
            this.stn00BindingSource.DataMember = "Stn00";
            this.stn00BindingSource.DataSource = this.driveOffDataSet;
            // 
            // driveOffDataSet
            // 
            this.driveOffDataSet.DataSetName = "DriveOffDataSet";
            this.driveOffDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Checked = false;
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(326, 41);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(136, 20);
            this.dateTimePicker1.TabIndex = 17;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(481, 39);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonSearch.TabIndex = 16;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(20, 95);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(277, 20);
            this.textBox3.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Receipt Number:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(323, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Account Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Station:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.dataGridView1, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.groupBoxFilters, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.buttonCollapse, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(977, 465);
            this.tableLayoutPanel.TabIndex = 3;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StationName,
            this.AccountDate,
            this.TicketNo,
            this.AttendantName,
            this.TransactionTime,
            this.AmountPaid,
            this.CreatedBy,
            this.numberOfHOSNotesDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.spgetDriveOffListBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 183);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(971, 279);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // StationName
            // 
            this.StationName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StationName.DataPropertyName = "StationName";
            this.StationName.HeaderText = "Station";
            this.StationName.Name = "StationName";
            this.StationName.ReadOnly = true;
            // 
            // AccountDate
            // 
            this.AccountDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AccountDate.DataPropertyName = "AccountDate";
            this.AccountDate.HeaderText = "Account Date";
            this.AccountDate.Name = "AccountDate";
            this.AccountDate.ReadOnly = true;
            // 
            // TicketNo
            // 
            this.TicketNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TicketNo.DataPropertyName = "RecieptNo";
            this.TicketNo.HeaderText = "Receipt Number";
            this.TicketNo.Name = "TicketNo";
            this.TicketNo.ReadOnly = true;
            // 
            // AttendantName
            // 
            this.AttendantName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AttendantName.DataPropertyName = "AttendantName";
            this.AttendantName.HeaderText = "Attendant Name";
            this.AttendantName.Name = "AttendantName";
            this.AttendantName.ReadOnly = true;
            // 
            // TransactionTime
            // 
            this.TransactionTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TransactionTime.DataPropertyName = "TransactionTime";
            this.TransactionTime.HeaderText = "Transaction Time";
            this.TransactionTime.Name = "TransactionTime";
            this.TransactionTime.ReadOnly = true;
            // 
            // AmountPaid
            // 
            this.AmountPaid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AmountPaid.DataPropertyName = "AmountPaid";
            this.AmountPaid.HeaderText = "Transaction Amount";
            this.AmountPaid.Name = "AmountPaid";
            this.AmountPaid.ReadOnly = true;
            // 
            // CreatedBy
            // 
            this.CreatedBy.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CreatedBy.DataPropertyName = "CreatedBy";
            this.CreatedBy.HeaderText = "Reported By";
            this.CreatedBy.Name = "CreatedBy";
            this.CreatedBy.ReadOnly = true;
            // 
            // numberOfHOSNotesDataGridViewTextBoxColumn
            // 
            this.numberOfHOSNotesDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.numberOfHOSNotesDataGridViewTextBoxColumn.DataPropertyName = "NumberOfHOSNotes";
            this.numberOfHOSNotesDataGridViewTextBoxColumn.HeaderText = "HO Comments";
            this.numberOfHOSNotesDataGridViewTextBoxColumn.Name = "numberOfHOSNotesDataGridViewTextBoxColumn";
            this.numberOfHOSNotesDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // spgetDriveOffListBindingSource
            // 
            this.spgetDriveOffListBindingSource.DataMember = "sp_getDriveOffList";
            this.spgetDriveOffListBindingSource.DataSource = this.driveOffDataSet;
            // 
            // buttonCollapse
            // 
            this.buttonCollapse.Location = new System.Drawing.Point(0, 157);
            this.buttonCollapse.Margin = new System.Windows.Forms.Padding(0);
            this.buttonCollapse.Name = "buttonCollapse";
            this.buttonCollapse.Size = new System.Drawing.Size(37, 20);
            this.buttonCollapse.TabIndex = 3;
            this.buttonCollapse.Text = "▲";
            this.buttonCollapse.UseVisualStyleBackColor = true;
            this.buttonCollapse.Click += new System.EventHandler(this.buttonCollapse_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // stn00TableAdapter
            // 
            this.stn00TableAdapter.ClearBeforeFill = true;
            // 
            // sp_getDriveOffListTableAdapter
            // 
            this.sp_getDriveOffListTableAdapter.ClearBeforeFill = true;
            // 
            // DriverOff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "DriverOff";
            this.Size = new System.Drawing.Size(977, 465);
            this.Load += new System.EventHandler(this.DriverOff_Load);
            this.groupBoxFilters.ResumeLayout(false);
            this.groupBoxFilters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stn00BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.driveOffDataSet)).EndInit();
            this.tableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgetDriveOffListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxFilters;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Button buttonCollapse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.BindingSource stn00BindingSource;
        private DAL.DriveOffDataSet driveOffDataSet;
        private System.Windows.Forms.BindingSource spgetDriveOffListBindingSource;
        private DAL.DriveOffDataSetTableAdapters.Stn00TableAdapter stn00TableAdapter;
        private DAL.DriveOffDataSetTableAdapters.sp_getDriveOffListTableAdapter sp_getDriveOffListTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn StationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn TicketNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttendantName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountPaid;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreatedBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberOfHOSNotesDataGridViewTextBoxColumn;
    }
}
