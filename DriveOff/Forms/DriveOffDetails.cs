﻿using System;
using System.Drawing;
using System.Security.Principal;
using System.Windows.Forms;
using DUC.KNPC.HOAddIn.DriveOff.DAL;
using DUC.KNPC.HOAddIn.DriveOff.DAL.DriveOffDataSetTableAdapters;

namespace DUC.KNPC.HOAddIn.DriveOff.Forms
{
    public partial class DriveOffDetails : Form
    {
        private readonly DriveOffDataSet.sp_getDriveOffListRow _driveOffDetails;
        private readonly DriveOffHeadOfficeCommentsTableAdapter _driveOffHeadOfficeCommentsTableAdapter = new DriveOffHeadOfficeCommentsTableAdapter();

        public DriveOffDetails(DriveOffDataSet.sp_getDriveOffListRow driveOffDetails)
        {
            _driveOffDetails = driveOffDetails;
            InitializeComponent();
        }

        private void LoadHONote()
        {
            richTextBoxHONotes.Clear();
            var dt = _driveOffHeadOfficeCommentsTableAdapter.GetData(_driveOffDetails.ID);
            foreach (var row in dt.Rows)
            {
                AddRichTextBoxEntry((DriveOffDataSet.DriveOffHeadOfficeCommentsRow) row);
            }
            richTextBoxHONotes.ScrollToCaret();
            
        }

        private void DriveOffDetails_Load(object sender, EventArgs e)
        {
            try
            {
                spgetDriveOffListRowBindingSource.DataSource = _driveOffDetails;
                LoadHONote();
            }
            catch (Exception exception)
            {

                MessageBox.Show(exception.Message, @"Data Load Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveNewNote();
            }
            catch (Exception exception)
            {

                MessageBox.Show(exception.Message, @"Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveNewNote()
        {
            #region Validation
            errorProvider1.Clear();
            var windowsIdentity = WindowsIdentity.GetCurrent();
            if (windowsIdentity == null)
            {
                errorProvider1.SetError(label26, "Unable to retrieve username");
                return;
            }
            if (richTextBoxNewNote.Text.Trim().Length == 0)
            {
                errorProvider1.SetError(label26, "Please Enter a valid Note");
                return;
            }
            #endregion

            _driveOffHeadOfficeCommentsTableAdapter.sp_SaveHoNote(_driveOffDetails.ID, _driveOffDetails.StationID,
                   _driveOffDetails.RecieptNo, _driveOffDetails.TicketNo, _driveOffDetails.TicketStamp,
                   windowsIdentity.Name,
                   DateTime.Now,
                   richTextBoxNewNote.Text);
            buttonSave.Enabled = false;
            LoadHONote();
        }

        private void AddRichTextBoxEntry(DriveOffDataSet.DriveOffHeadOfficeCommentsRow dataRow)
        {
            var richTextBoxFont = new Font("Arial", 11);
            richTextBoxHONotes.SelectionFont = richTextBoxFont;
            richTextBoxHONotes.SelectionColor = Color.Black;
            richTextBoxHONotes.SelectedText = "Note #";
            richTextBoxHONotes.SelectionColor = Color.DarkBlue;
            richTextBoxHONotes.SelectedText = dataRow.NoteID.ToString();
            //"Enrty #{0} added on {2} by user {1} "
            richTextBoxHONotes.SelectionColor = Color.Black;
            richTextBoxHONotes.SelectedText = " added on ";
            richTextBoxHONotes.SelectionColor = Color.DarkBlue;
            richTextBoxHONotes.SelectedText = dataRow.CreatedDate.ToString("dd-MMM-yyyy hh:mm tt");
            richTextBoxHONotes.SelectionColor = Color.Black;
            richTextBoxHONotes.SelectedText = "  by user ";
            richTextBoxHONotes.SelectionColor = Color.DarkBlue;
            richTextBoxHONotes.SelectedText = dataRow.CreateBy + "\n";
            richTextBoxHONotes.SelectionBullet = true;
            richTextBoxHONotes.SelectionFont = richTextBoxFont;
            richTextBoxHONotes.SelectionColor = Color.DarkBlue;
            richTextBoxHONotes.SelectedText = dataRow.Note + "\n";
            richTextBoxHONotes.SelectionBullet = false;
        }

       

     
      
    }
}
