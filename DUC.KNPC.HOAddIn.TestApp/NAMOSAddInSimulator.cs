﻿using DUC.NamosAddIn.Core.Constants;
using Namos.Addin.Interfaces;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

namespace DUC.KNPC.HOAddIn.TestApp
{
    public partial class NAMOSAddInSimulator : Form
    {
        List<Button> _allButtons;

        public NAMOSAddInSimulator()
        {
            InitializeComponent();
            _allButtons = new List<Button>();
            LoadClientAddingConfig();
            btnRefresh.Enabled = btnAddNewRecord.Enabled = btnDelete.Enabled = btnSave.Enabled = btnShow.Enabled = false;
        }

        private void LoadClientAddingConfig()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("TreeClientAddins.config.xml");
            var navigation = doc.SelectNodes("/configuration/configSections/section");
            var modules = new List<TreeNode>();
            foreach (XmlNode node in navigation)
            {
                if (node.Attributes["path"].Value != "/Client/AddinsTreeStructure")
                    continue;

                var addinNodes = node.SelectNodes("AddinNode");
                foreach (XmlNode addInNode in addinNodes)
                {
                    var parentNode = new TreeNode
                    {
                        Text = addInNode.Attributes["title"].Value,
                        Name = addInNode.Attributes["id"].Value,
                    };
                    if (addInNode.ChildNodes != null && addInNode.ChildNodes.Count > 0)
                    {
                        var childNodes = new List<TreeNode>();
                        foreach (XmlNode childNode in addInNode.ChildNodes)
                        {
                            try
                            {
                                var control = Activator.CreateInstance(childNode.Attributes["type"].Value.Substring(childNode.Attributes["type"].Value.IndexOf(',') + 1), childNode.Attributes["type"].Value.Substring(0, childNode.Attributes["type"].Value.IndexOf(',')));
                                parentNode.Nodes.Add(new TreeNode
                                {
                                    Tag = control.Unwrap() as UserControl,
                                    Text = childNode.Attributes["title"].Value
                                });
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK);
                            }
                        }
                    }

                    treeView1.Nodes.Add(parentNode);
                }
            }
        }

        void OnToolbarCommand(object sender, EventArgs e)
        {
            var button = sender as Button;
            this.splitContainer1.Panel2.Controls.Clear();
            this.splitContainer1.Panel2.Controls.Add(button.Tag as UserControl);
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag != null)
            {
                this.splitContainer1.Panel2.Controls.Clear();
                this.splitContainer1.Panel2.Controls.Add(e.Node.Tag as UserControl);

                var control = e.Node.Tag as ISupportToolbarCommands;
                btnAddNewRecord.Enabled = control.IsCommandSupported(NamosAddInConstants.ADDIN_COMMAND_NEW);
                btnDelete.Enabled = control.IsCommandSupported(NamosAddInConstants.ADDIN_COMMAND_DELETE);
                btnRefresh.Enabled = control.IsCommandSupported(NamosAddInConstants.ADDIN_COMMAND_REFRESH);
                btnSave.Enabled = control.IsCommandSupported(NamosAddInConstants.ADDIN_COMMAND_SAVE);
                btnShow.Enabled = control.IsCommandSupported(NamosAddInConstants.ADDIN_COMMAND_SHOW);
            }
            else
            {
                btnRefresh.Enabled = btnAddNewRecord.Enabled = btnDelete.Enabled = false;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            if (this.splitContainer1.Panel2.Controls.Count > 0)
            {
                var control = this.splitContainer1.Panel2.Controls[0] as ISupportToolbarCommands;
                control.DoCommand(NamosAddInConstants.ADDIN_COMMAND_REFRESH);
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys k)
        {
            if (k == Keys.Enter || k == Keys.Return)
            {
                if (this.splitContainer1.Panel2.Controls.Count > 0)
                {
                    var control = this.splitContainer1.Panel2.Controls[0] as ISupportToolbarCommands;
                    control.DoCommand(NamosAddInConstants.ADDIN_COMMAND_SAVE);
                }
            }

            return base.ProcessCmdKey(ref msg, k);
        }

        private void btnAddNewRecord_Click(object sender, EventArgs e)
        {
            if (this.splitContainer1.Panel2.Controls.Count > 0)
            {
                var control = this.splitContainer1.Panel2.Controls[0] as ISupportToolbarCommands;
                control.DoCommand(NamosAddInConstants.ADDIN_COMMAND_NEW);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.splitContainer1.Panel2.Controls.Count > 0)
            {
                var control = this.splitContainer1.Panel2.Controls[0] as ISupportToolbarCommands;
                control.DoCommand(NamosAddInConstants.ADDIN_COMMAND_DELETE);
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            if (this.splitContainer1.Panel2.Controls.Count > 0)
            {
                var control = this.splitContainer1.Panel2.Controls[0] as ISupportToolbarCommands;
                control.DoCommand(NamosAddInConstants.ADDIN_COMMAND_SHOW);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.splitContainer1.Panel2.Controls.Count > 0)
            {
                var control = this.splitContainer1.Panel2.Controls[0] as ISupportToolbarCommands;
                control.DoCommand(NamosAddInConstants.ADDIN_COMMAND_SAVE);
            }
        }
    }
}
