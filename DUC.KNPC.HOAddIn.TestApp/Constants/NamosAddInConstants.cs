﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DUC.NamosAddIn.Core.Constants
{
    public class NamosAddInConstants
    {
        public const string ADDIN_COMMAND_NEW = "AddNewObject";
        public const string ADDIN_COMMAND_DELETE = "Delete";
        public const string ADDIN_COMMAND_SAVE = "SaveObject";
        public const string ADDIN_COMMAND_REFRESH = "Refresh";
        public const string ADDIN_COMMAND_SHOW = "ShowObject";
    }
}
