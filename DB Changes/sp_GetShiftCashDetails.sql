USE AttendantModule
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sajeev Sasidharan
-- Create date: 10 April 2016
-- Description:	Get Shift Cash Details
-- =============================================
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[sp_GetShiftCashDetails]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[sp_GetShiftCashDetails]
END
GO
CREATE PROCEDURE dbo.[sp_GetShiftCashDetails]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @currentShift TABLE (accountdate datetime, shiftno int)
	DECLARE @cashInForeCourt DECIMAL, @cashDroppedByManager DECIMAL, @excessCash DECIMAL, @cashByAttendants DECIMAL
	
	-- Get the current shift details
	INSERT INTO @currentShift EXEC [GetCurrentShiftDetails]
	
	-- Get current sales
	SELECT @cashInForeCourt = SUM(SellAmount) from v_CurrentShiftSales

	-- Get safe dropped amount
	SELECT @cashByAttendants = SUM(DroppedAmount) FROM v_CurrentShiftSafeDrop 

	-- Get excess cash for the shift
	SELECT @excessCash = SUM(DroppedAmount) from AttendantSafeDropExcessShiftWise sd JOIN @currentShift cs on cs.accountdate = sd.AccountDate and cs.shiftno = sd.ShiftNo

	-- Get cash dropped by manager
	SELECT @cashDroppedByManager = SUM(DroppedAmount) FROM [v_CurrentShiftSupervisorSafeDrop] 
	
	 SELECT accountdate as AccountDate, shiftno as ShiftNumber, ISNULL(@cashInForeCourt, 0) as CashInForecourt,
	 ISNULL(@excessCash, 0) as ExcessCash, ISNULL(@cashByAttendants,0) as CashDroppedByAttendant,
	 ISNULL(@cashDroppedByManager, 0) as CashDroppedByManager, ISNULL(@cashDroppedByManager,0) - ISNULL(@cashByAttendants,0) AS CashSummary FROM @currentShift 
END
GO
