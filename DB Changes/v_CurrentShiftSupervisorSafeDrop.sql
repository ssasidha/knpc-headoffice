USE [AttendantModule]
GO

IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[v_CurrentShiftSupervisorSafeDrop]') 
                   and OBJECTPROPERTY(id, N'IsView') = 1 )
BEGIN
    DROP VIEW [dbo].[v_CurrentShiftSupervisorSafeDrop]
END
GO
/****** Object:  View [dbo].[v_CurrentShiftSupervisorSafeDrop]    Script Date: 10-04-2016 03:44:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_CurrentShiftSupervisorSafeDrop]
AS
SELECT        TrxID, dbo.FromTimestampUAE(TrxTimeStamp) AS TrxTimeStamp, sd.AttendantID, DroppedAmount, Note, Currency, DroppedFCurrency
FROM            dbo.AttendantSafeDropShiftWise sd JOIN AttendantDef ad on ad.AttendantID = sd.AttendantID
WHERE   ad.EmployeeID IS NULL AND (TrxTimeStamp >
                             (SELECT        MAX(eod_stamp) AS Expr1
                               FROM            dbo.v_NamosShift)) AND (AccountDate >
                             (SELECT        MAX(datadate) AS Expr1
                               FROM            dbo.AccountdateInfo)) OR
                         (TrxTimeStamp >
                             (SELECT        MAX(eod_stamp) AS Expr1
                               FROM            dbo.v_NamosShift AS v_NamosShift_1)) AND (AccountDate =
                             (SELECT        MAX(datadate) AS Expr1
                               FROM            dbo.AccountdateInfo AS AccountdateInfo_3)) AND (ShiftNo >
                             (SELECT        MAX(shiftno) AS Expr1
                               FROM            dbo.AccountdateInfo AS AccountdateInfo_2
                               WHERE        (datadate =
                                                             (SELECT        MAX(datadate) AS Expr1
                                                               FROM            dbo.AccountdateInfo AS AccountdateInfo_1))))




GO


