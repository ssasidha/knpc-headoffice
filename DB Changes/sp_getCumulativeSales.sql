USE StationData
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[sp_getCumulativeSales]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
	DROP PROCEDURE sp_getCumulativeSales
GO
/****** Object:  StoredProcedure [dbo].[sp_getCumulativeStock]    Script Date: 23-04-2016 23:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sajeev Sasidharan	
-- Create date: 23-4-2016
-- Description:	Get the cumulative sales
-- =============================================
-- sp_getCumulativeSales '2016-01-01', '2016-05-05', 1
CREATE PROCEDURE [dbo].sp_getCumulativeSales
	-- Add the parameters for the stored procedure here
	@accountFromDate datetime,
	@accountToDate datetime,
	@stationId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @ProductCode Table (grade int, shorttext varchar(100))
declare @Sales Table (Grade INT, unitPrice Float, SoldQuantity FLOAT, CashSales FLOAT, RFIDSales FLOAT, 
PetroCardSales FLOAT, CivilIdSales float, KNETSales FLOAT, ThirdPartySales FLOAT) 
DECLARE @KnetPayTNo INT
DECLARE @CivilIdPayTNo INT
DECLARE @RFIDPayTNo INT
DECLARE @RFIDOfflinePaytNo INT
DECLARE @PetroCardPayTNo INT 
DECLARE @BankCard INT 
SELECT @KnetPayTNo = PayTNo from PayTypeMapping ptm WHERE PayType = 'KNet'
SELECT @CivilIdPayTNo = PayTNo from PayTypeMapping ptm WHERE PayType = 'Civil ID'
SELECT @PetroCardPayTNo = PayTNo from PayTypeMapping ptm WHERE PayType = 'PetroCard'
SELECT TOP 1 @RFIDPayTNo =  PayTNo from PayTypeMapping ptm WHERE PayType = 'RFID' ORDER BY PayTNo asc
SELECT TOP 1 @RFIDOfflinePaytNo = PayTNo from PayTypeMapping ptm WHERE PayType = 'RFID' ORDER BY PayTNo DESC
SELECT @PetroCardPayTNo = PayTNo from PayTypeMapping ptm WHERE PayType = 'PetroEasy'
SELECT @BankCard = PayTNo from PayTypeMapping ptm WHERE PayType = 'Bank'

insert into @ProductCode
select grade,shorttext from plu00 inner join fct02 on plu00.artno=fct02.artno

insert into @Sales
select t05.grade,t05.sellprice,SUM(t05.quantity), 
SUM(CASE WHEN t01.rectype IN (0, 25) THEN t05.sellamount ELSE 0 END), 
SUM(CASE WHEN pa.paytno IN (@RFIDOfflinePaytNo, @RFIDPayTNo) THEN t05.sellamount ELSE 0 END),
SUM(CASE WHEN pa.paytno = @CivilIdPayTNo THEN t05.sellamount ELSE 0 END),
SUM(CASE WHEN pa.paytno = @CivilIdPayTNo THEN t05.sellamount ELSE 0 END),
SUM(CASE WHEN pa.paytno = @KnetPayTNo THEN t05.sellamount ELSE 0 END),
SUM(CASE WHEN pa.paytno = @BankCard THEN t05.sellamount ELSE 0 END) from 
trx01 t01 INNER JOIN
                         dbo.trx05 t05 ON t01.posno = t05.posno AND t01.ticketno = t05.ticketno and t05.StationId = t01.StationId AND 
                         t01.ticketstamp = t05.ticketstamp LEFT OUTER JOIN
                         dbo.PreAuthorization AS pa ON t05.StationId = pa.StationId AND t05.transact_id = pa.PrepayID AND CONVERT(DATE, dbo.FromTimestampUAE(t05.ticketstamp)) 
                         = CONVERT(DATE, pa.Timestamp)
where CONVERT(DATE, pa.Timestamp) BETWEEN @accountFromDate AND @accountToDate
AND pa.StationId = @stationId
group by t05.grade, t05.sellprice

SELECT s.Grade, p.shorttext as Product, s.unitPrice , s.SoldQuantity , s.CashSales , s.RFIDSales , 
s.PetroCardSales , s.CivilIdSales , s.KNETSales, s.ThirdPartySales FROM @Sales s JOIN @ProductCode p ON s.Grade = p.grade
ORDER BY p.shorttext

END
