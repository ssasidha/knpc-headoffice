USE BankData
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[sp_getBankReceipts]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
	DROP PROCEDURE sp_getBankReceipts
GO
/****** Object:  StoredProcedure [dbo].[sp_getCumulativeStock]    Script Date: 23-04-2016 23:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sajeev Sasidharan	
-- Create date: 23-5-2016
-- Description:	Get the bank receipts based on month and status
-- =============================================
-- sp_getBankReceipts '2016-01-01', '2016-05-05', 1
CREATE PROCEDURE [dbo].sp_getBankReceipts
	-- Add the parameters for the stored procedure here
	@startDate datetime,
	@endDate datetime,
	@status smallint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT br.ReceiptId, br.BankName, br.ReceiptDate, br.[Status], br.Total 
	FROM BankReceipts br 
	WHERE ReceiptDate BETWEEN @startDate AND @endDate AND ISNULL(@status, br.[Status]) = br.[Status]
END
