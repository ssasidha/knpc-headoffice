USE BankData
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[rptMonthlyBankedCashSummary]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
	DROP PROCEDURE rptMonthlyBankedCashSummary
GO
/****** Object:  StoredProcedure [dbo].[sp_getCumulativeStock]    Script Date: 23-04-2016 23:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sajeev Sasidharan	
-- Create date: 23-4-2016
-- Description:	Get monthly closed bank receipt corrections
-- =============================================
-- rptMonthlyBankedCashSummary '2016-01-01', '2016-06-30'
CREATE PROCEDURE [dbo].rptMonthlyBankedCashSummary
	-- Add the parameters for the stored procedure here
	@startDate DATETIME,
	@endDate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @summaryReport TABLE (AccountDate DATETIME, StationNo INT, CashSales FLOAT, CashBanked FLOAT)
	INSERT INTO @SummaryReport (AccountDate, StationNo, CashBanked) SELECT bsm.AccountDate, bsm.StationId
	, SUM(br.Total) FROM BankReceiptShiftsMapping bsm
	JOIN BankReceipts br on br.ReceiptId = bsm.ReceiptId WHERE br.ReceiptDate BETWEEN @startDate AND @endDate
	GROUP BY bsm.AccountDate, bsm.StationId

		Update s Set s.CashSales = ISNULL(v.CashSales, 0)
	from @summaryReport s JOIN 
	(SELECT CAST(stationData.dbo.FromTimestampUAE(v.timestamp_old) as DATE) as AccountDate, v.StationId,
	SUM(Expr1)as CashSales FROM 
	StationData..[v_AttendantShiftSummary] v
	LEFT JOIN StationData..PayTypeMapping ptm on v.rectype = ptm.PayTNo
	WHERE (v.rectype = 25) OR (ptm.PayType='Cash' and ptm.paytno= v.paytno)
	GROUP BY CAST(stationData.dbo.FromTimestampUAE(v.timestamp_old) as DATE), v.StationId
	) as v on v.AccountDate = Cast(s.accountDate as Date)
	AND v.StationId = s.StationNo

	SELECT AccountDate, StationNo, CashSales, CashBanked, (ISNULL(CashSales,0) - ISNULL(CashBanked,0)) as [Difference] FROM @summaryReport ORDER BY AccountDate, StationNo
END
