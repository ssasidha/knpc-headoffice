USE StationData
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[sp_GetShiftsForBRAssignment]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
	DROP PROCEDURE sp_GetShiftsForBRAssignment
GO
/****** Object:  StoredProcedure [dbo].[sp_getCumulativeStock]    Script Date: 23-04-2016 23:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sajeev Sasidharan	
-- Create date: 23-4-2016
-- Description:	Get all shift details for BR Assignment
-- =============================================
-- sp_GetShiftsForBRAssignment '2016-01-01', '2016-05-05', 1
CREATE PROCEDURE [dbo].sp_GetShiftsForBRAssignment
	-- Add the parameters for the stored procedure here
	@accountDate datetime,
	@stationId int,
	@checkUnAssigned bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here



END
