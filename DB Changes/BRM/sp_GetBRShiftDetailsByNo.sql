USE BankData
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[sp_GetBRShiftDetailsByNo]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
	DROP PROCEDURE sp_GetBRShiftDetailsByNo
GO
/****** Object:  StoredProcedure [dbo].[sp_getCumulativeStock]    Script Date: 23-04-2016 23:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sajeev Sasidharan	
-- Create date: 23-4-2016
-- Description:	Get all bank recepit details for BR no
-- =============================================
-- sp_GetBRShiftDetailsByNo 3, '2016-05-01', 1, '0'
CREATE PROCEDURE [dbo].sp_GetBRShiftDetailsByNo
	-- Add the parameters for the stored procedure here
	@bankReceiptNo BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @safeDropTotal FLOAT
	select @safeDropTotal = SUM(safd.DroppedAmount) from StationData..AttendantSafeDropShiftWise safd 
	JOIN BankReceiptShiftsMapping bsm on  CONVERT(DATE,safd.AccountDate) = CONVERT(DATE,bsm.AccountDate)  and safd.ShiftNo = bsm.ShiftNo and safd.StationId = bsm.StationId
	WHERE bsm.ReceiptId = @bankReceiptNo GROUP BY bsm.ReceiptId 

SELECT @safeDropTotal = (ISNULL(@safeDropTotal, 0)) + SUM (esafd.DroppedAmount) from StationData..AttendantSafeDropExcessShiftWise esafd 
JOIN BankReceiptShiftsMapping bsm on  CONVERT(DATE,esafd.AccountDate) = CONVERT(DATE,bsm.AccountDate)  and esafd.ShiftNo = bsm.ShiftNo and esafd.StationId = bsm.StationId
	WHERE bsm.ReceiptId = @bankReceiptNo GROUP BY bsm.ReceiptId 
	SET @safeDropTotal = ISNULL(@safeDropTotal, 0)
	SELECT [ReceiptId]
      ,[BankName]
      ,[BankID]
      ,[Total]
      ,[ReceiptDate]
      ,[Status]
      ,[Branch], @safeDropTotal as SafeDrop, (Total - @safeDropTotal) as [Difference] FROM BankReceipts
	  WHERE ReceiptId = @bankReceiptNo
END
