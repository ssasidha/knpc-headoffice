USE BankData
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[sp_AddBankReceipt]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
	DROP PROCEDURE sp_AddBankReceipt
GO
/****** Object:  StoredProcedure [dbo].[sp_getCumulativeStock]    Script Date: 23-04-2016 23:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sajeev Sasidharan	
-- Create date: 23-5-2016
-- Description:	Add the bank receipts 
-- =============================================

CREATE PROCEDURE [dbo].sp_AddBankReceipt
	-- Add the parameters for the stored procedure here
	@bankDate datetime,
	@bankName nvarchar(200),
	@bankId nvarchar(200),
	@totalCollected float,
	@branch nvarchar(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO [dbo].[BankReceipts]
           ([BankName]
           ,[BankID]
           ,[Total]
           ,[ReceiptDate]
           ,[Status])
		   VALUES
		   (@bankName, @bankId, @totalCollected, @bankDate, 1)
END
