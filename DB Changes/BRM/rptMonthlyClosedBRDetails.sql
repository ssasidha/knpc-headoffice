USE BankData
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[rptMonthlyClosedBRDetails]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
	DROP PROCEDURE rptMonthlyClosedBRDetails
GO
/****** Object:  StoredProcedure [dbo].[sp_getCumulativeStock]    Script Date: 23-04-2016 23:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sajeev Sasidharan	
-- Create date: 23-4-2016
-- Description:	Get monthly closed bank receipt details
-- =============================================
-- rptMonthlyClosedBRDetails '2016-05-01', '2016-05-30'
CREATE PROCEDURE [dbo].rptMonthlyClosedBRDetails
	-- Add the parameters for the stored procedure here
	@startDate DATETIME,
	@endDate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @brDetails TABLE (BankID NVARCHAR(200), BankName NVARCHAR(200), BankReceiptNo BIGINT, BankDate DATETIME, TotalBanked FLOAT, CorrectedTotal FLOAT, CorrectedBy NVARCHAR(200), CorrectedDate DATETIME, CorrectionReason NVARCHAR(MAX),
	AccountDate DATETIME, ShiftNo INT, StationId INT)

	DECLARE @safeDropSummary TABLE (ShiftNo INT, EODNo INT, AccountDate DATETIME, StationId INT, SafeDropAmount FLOAT, CashSales FLOAT)
INSERT INTO @safeDropSummary (ShiftNo, AccountDate, StationId, EODNo)
SELECT DISTINCT ShiftNo, AccountDate, StationId, EODNo FROM BankReceiptShiftsMapping bsm
JOIN BankReceipts br on bsm.ReceiptId = br.ReceiptId WHERE br.ReceiptDate BETWEEN @startDate AND @endDate

INSERT INTO @brDetails SELECT br.BankID, br.BankName, br.ReceiptId, br.ReceiptDate, br.Total, br.Total,
NULL, NULL, NULL, bsm.AccountDate, bsm.ShiftNo, bsm.StationId FROM BankReceipts br JOIN BankReceiptShiftsMapping bsm on br.ReceiptId = bsm.ReceiptId
WHERE br.Status = 1 and br.ClosedDate between @startDate and @endDate

update b set b.TotalBanked = ISNULL(bcc.OldTotal, b.TotalBanked), b.CorrectedBy = lastCorrection.CreatedBy,
b.CorrectedDate = lastCorrection.CorrectionDate, b.CorrectionReason = lastCorrection.CorrectionReason FROM @brDetails b
LEFT JOIN (SELECT Min(brc.CorrectionId) as CorrectionId, brc.ReceiptId FROM BankReceiptCorrections brc group by brc.ReceiptId)
bc on bc.ReceiptId = b.BankReceiptNo
LEFT JOIN BankReceiptCorrections bcc on bcc.CorrectionId = bc.CorrectionId
LEFT JOIN (SELECT Min(brc.CorrectionId) as CorrectionId, brc.ReceiptId FROM BankReceiptCorrections brc group by brc.ReceiptId) blc on blc.ReceiptId = b.BankReceiptNo
LEFT JOIN BankReceiptCorrections lastCorrection on blc.CorrectionId = lastCorrection.CorrectionId

UPDATE s SET s.SafeDropAmount = x.Amount FROM @safeDropSummary s JOIN (
--	select 
--@ShiftNo as shift,
--sd.DroppedAmt as Amount,dbo.FromTimestampUAE(sd.TrxTimeStamp) as TrxTime,(select convert(varchar,(select dbo.FromTimestampUAE(@startTime)))) as ShiftStart
--,(select convert(varchar,(select dbo.FromTimestampUAE(@endTime)))) as ShiftEnd,@Site as SiteName,Note,@site_id  as siteid, sd.Currency,sd.DroppedFCurrency, def.EmployeeID from AttendantSafeDrop sd inner join AttendantDef def on sd.AttendantID=def.AttendantID where def.EmployeeID IS NULL and sd.TrxTimeStamp between @startTime and @endTime
--union
select sd.ShiftNo, sd.AccountDate, sd.StationId,
SUM(sd.DroppedAmount) Amount
from StationData..AttendantSafeDropShiftWise sd inner join StationData..AttendantDef def on sd.AttendantID=def.AttendantID
and sd.StationId = def.StationId where def.EmployeeID IS NULL
GROUP BY sd.AccountDate, sd.ShiftNo, sd.StationId
union
select sd.ShiftNo, sd.AccountDate, sd.StationId,
SUM(sd.DroppedAmount) amount
from StationData..AttendantSafeDropExcessShiftWise sd inner join StationData..AttendantDef def on sd.AttendantID=def.AttendantID and sd.StationId = def.StationId where def.EmployeeID IS NULL 
GROUP BY sd.AccountDate, sd.StationId, sd.ShiftNo) x on x.AccountDate = s.AccountDate AND s.ShiftNo = x.ShiftNo and s.StationId = x.StationId

	Update s Set s.CashSales = ISNULL(v.CashSales, 0)
	from @safeDropSummary s JOIN 
	(SELECT CAST(stationData.dbo.FromTimestampUAE(v.timestamp_old) as DATE) as AccountDate, v.StationId,
	SUM(Expr1)as CashSales, v.eodno FROM 
	StationData..[v_AttendantShiftSummary] v
	LEFT JOIN StationData..PayTypeMapping ptm on v.rectype = ptm.PayTNo
	WHERE (v.rectype = 25) OR (ptm.PayType='Cash' and ptm.paytno= v.paytno)
	GROUP BY CAST(stationData.dbo.FromTimestampUAE(v.timestamp_old) as DATE), v.StationId, v.eodno
	) as v on v.AccountDate = Cast(s.accountDate as Date)
	AND v.StationId = s.StationId and v.eodno = s.EODNo

	SELECT DISTINCT br.BankReceiptNo, br.BankID,  br.BankName, br.BankDate, br.TotalBanked as Totalbanked, br.CorrectedTotal, ISNULL(br.CorrectedBy, '') as CorrectedBy, br.CorrectedDate, br.CorrectionReason, br.StationId, br.AccountDate, br.ShiftNo, COALESCE(br.correctedTotal, br.totalBanked, sd.SafeDropAmount) as AmountDeposited, ISNULL(sd.CashSales, 0) as Cash, ISNULL(sd.SafeDropAmount, 0) as ActualAmount from @brDetails br LEFT JOIN @safeDropSummary sd on br.AccountDate = sd.AccountDate and br.StationId = sd.StationId and br.ShiftNo = sd.ShiftNo

END
