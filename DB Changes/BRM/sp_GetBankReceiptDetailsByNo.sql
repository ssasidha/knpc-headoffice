USE BankData
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[sp_GetBankReceiptDetailsByNo]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
	DROP PROCEDURE sp_GetBankReceiptDetailsByNo
GO
/****** Object:  StoredProcedure [dbo].[sp_getCumulativeStock]    Script Date: 23-04-2016 23:46:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sajeev Sasidharan	
-- Create date: 23-4-2016
-- Description:	Get all bank recepit details for BR no
-- =============================================
-- sp_GetBankReceiptDetailsByNo 3, '2016-05-01', 1, '0'
CREATE PROCEDURE [dbo].sp_GetBankReceiptDetailsByNo
	-- Add the parameters for the stored procedure here
	@bankReceiptNo BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @safeDropTotal FLOAT

	SELECT * FROM BankReceipts
END
