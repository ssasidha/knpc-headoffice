USE [AttendantModule]
GO
/****** Object:  StoredProcedure [dbo].[GetShiftDetails]    Script Date: 12-04-2016 17:34:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sajeev Sasidharan
-- Create date: 12 Apr 2016
-- Description:	Get Shift Details for Eod number
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetShiftDetailsForEODNo]
	-- Add the parameters for the stored procedure here
 @EODNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select  dbo.FromTimestampUAE(eod02.timestamp_old) AS ShiftStart,  dbo.FromTimestampUAE(eod02.last_signoff_stamp) as ShiftEnd from namos..eod02 as eod02 where eodno=@EODNo
END
