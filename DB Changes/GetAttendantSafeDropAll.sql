USE [AttendantModule]
GO
/****** Object:  StoredProcedure [dbo].[GetAttendantSafeDropAll]    Script Date: 11-04-2016 13:37:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetAttendantSafeDropAll]
@eodno nvarchar(200)
AS
BEGIN

	SET NOCOUNT ON;
	CREATE TABLE #shifttable (shiftId INT)
		CREATE TABLE #outputtable (SiteName varchar(200),shift varchar(200),ShiftStart datetime null,ShiftEnd  datetime null,EmployeeName varchar(200),TrxTime datetime null ,Note varchar(500),
site_id int,Amount money null,Currency varchar(200),
DroppedFCurrency money null, employeeId VARCHAR(50) )

	declare @startTime bigint
	declare @endTime bigint,@site_id int
	declare @Site varchar(50)
	DECLARE @ShiftNo INT
DECLARE @AccountDate DATE


	if(@eodno='0')
	BEGIN

	select def.AttendantName as EmployeeName,@eodno as shift,@Site as SiteName,@site_id as site_id ,isnull(csd.DroppedAmount,0) as Amount,csd.TrxTimeStamp as TrxTime,'' as ShiftStart 
,'' as ShiftEnd,isnull(csd.Note,'') as Note, isnull(csd.Currency,'') as Currency,csd.DroppedFCurrency, def.EmployeeID from AttendantDef def inner  join v_CurrentShiftSafeDrop csd on csd.AttendantID=def.AttendantID order by csd.TrxTimeStamp;

	END

	ELSE
	BEGIN
		Insert into #shifttable select DISTINCT  Id from dbo.fn_ConvertStringToIdRows(@eodno,',')
		
DECLARE @crsID bigint
DECLARE @MyCursor CURSOR
SET @MyCursor = CURSOR FAST_FORWARD
FOR
SELECT shiftId from #shifttable
OPEN @MyCursor
FETCH NEXT FROM @MyCursor
INTO @crsID
WHILE @@FETCH_STATUS = 0
BEGIN

 SELECT @AccountDate = AccountDate, @ShiftNo = ShiftNo FROM dbo.fn_GetAccountDateDetailsFromEODNo(@crsID)
select @Site=stv.site_name,@site_id=stv.site_id from v_stn00 stv;
select @startTime=v.timestamp_old,@endTime=v.eod_stamp from v_NamosShift v where v.eodno=@crsID;

;with Details as(select def.AttendantName as FirstName,@crsID as shift,@Site as SiteName,@site_id as site_id ,sd.DroppedAmt as Amount,dbo.FromTimestampUAE(sd.TrxTimeStamp) as TrxTime,(select convert(varchar,(select dbo.FromTimestampUAE(@startTime)))) as ShiftStart
,(select convert(varchar,(select dbo.FromTimestampUAE(@endTime)))) as ShiftEnd,sd.Note, sd.Currency,sd.DroppedFCurrency, def.EmployeeID from AttendantSafeDrop sd inner join AttendantDef def on sd.AttendantID=def.AttendantID where sd.TrxTimeStamp between @startTime and @endTime
union
select def.AttendantName as FirstName,@crsID as shift,@Site 
as SiteName,@site_id as site_id ,sd.DroppedAmount as Amount,dbo.FromTimestampUAE(sd.TrxTimeStamp) as TrxTime
,(select convert(varchar,(select dbo.FromTimestampUAE(@startTime)))) as ShiftStart
,(select convert(varchar,(select dbo.FromTimestampUAE(@endTime)))) as ShiftEnd,sd.Note,sd.Currency,sd.DroppedFCurrency, def.EmployeeID

from AttendantSafeDropShiftWise sd inner join AttendantDef def on sd.AttendantID=def.AttendantID where CONVERT(DATE, AccountDate)= @AccountDate AND sd.ShiftNo=@ShiftNo
union
select def.AttendantName as FirstName,@crsID as shift,@Site 
as SiteName,@site_id as site_id ,sd.DroppedAmount as Amount,dbo.FromTimestampUAE(sd.TrxTimeStamp) as TrxTime
,(select convert(varchar,(select dbo.FromTimestampUAE(@startTime)))) as ShiftStart
,(select convert(varchar,(select dbo.FromTimestampUAE(@endTime)))) as ShiftEnd,sd.Note,sd.Currency,sd.DroppedFCurrency, def.EmployeeID

from AttendantSafeDropExcessShiftWise sd inner join AttendantDef def on sd.AttendantID=def.AttendantID where CONVERT(DATE, AccountDate)= @AccountDate AND sd.ShiftNo=@ShiftNo
)

Insert into #outputtable 
select d.SiteName,d.shift,d.ShiftStart,d.ShiftEnd,d.FirstName as EmployeeName,d.TrxTime,d.Note,
d.site_id ,SUM(d.Amount) as Amount,d.Currency,
sum(d.DroppedFCurrency) as DroppedFCurrency, EmployeeID
from details d group by d.SiteName,d.shift,d.ShiftStart,d.ShiftEnd,d.FirstName,d.TrxTime,d.Note,d.site_id,Currency, d.EmployeeID order by d.TrxTime

FETCH NEXT FROM @MyCursor
INTO @crsID
END
CLOSE @MyCursor
DEALLOCATE @MyCursor

SELECT SiteName ,shift ,ShiftStart ,ShiftEnd  ,EmployeeName ,TrxTime  ,Note ,
site_id ,Amount ,Currency ,
DroppedFCurrency , employeeId  FROM #outputtable 

DROP TABLE #shifttable

DROP TABLE #outputtable 

END

END
