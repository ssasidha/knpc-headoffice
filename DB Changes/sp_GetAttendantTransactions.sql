USE [AttendantModule]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAttendantTransactions]    Script Date: 06-04-2016 15:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_GetAttendantTransactions]
AS
BEGIN
(select
 a.AttendantName as AttendantFirstName, 
 a.AttendantID,
 isnull(v.trxamount,0) as Amount,
 v.TrxTimeStamp as TimeStamp,
 ISNULL(v.fuelpoint,0) as fuelpoint,
 'Safe Drop' as TransactionType
from dbo.AttendantDef a 
LEFT JOIN v_AttendantTrxViewer v ON a.AttendantID = v.AttendantID  where a.EmployeeID IS NOT NULL
)

UNION

(select
 a.AttendantName as AttendantFirstName, 
 a.AttendantID,
 ((isnull(v.sellamount,0))) as Amount,
 v.TicketStamp as TimeStamp,
 ISNULL(v.fuelpoint,0) as fuelpoint,
 'Cash Sale Transaction' as TransactionType
from dbo.AttendantDef a 
JOIN dbo.AttendantPLCMapping apm on a.EmployeeID = apm.EmployeeID 
LEFT JOIN v_CurrentShiftSales v ON apm.AttendantPLC = v.AttendantID  where a.EmployeeID IS NOT NULL
)
 order by AttendantFirstName,TimeStamp
END

