USE [NamosCustom]
GO
/****** Object:  StoredProcedure [dbo].[sp_getSystemAudit]    Script Date: 11-04-2016 12:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Pugazhendhi	
-- Create date: 23-8-2015
-- Description:	retrieves the system audit log
-- =============================================
ALTER PROCEDURE [dbo].[sp_getSystemAudit]
@FromDate date,
@ToDate date,
@DeviceType int--,
--@User int,
--@Level int
AS
BEGIN
	SET NOCOUNT ON;
	
	select dl.Description as DeviceType ,asrv.Log_Date, x.persname as User_id ,
	ISNULL(edl.Description,'') +ISNULL(asrv.Remarks,'')
	+ (case when edl.Device_Sub_1_Desc is not null then + CHAR(13)+ CHAR(10)+ edl.Device_Sub_1_Desc+':'+cast(asrv.Device_Sub_1_Nbr as nvarchar(20)) else '' end)
	+(case when edl.Device_Sub_2_Desc is not null then + CHAR(13) +CHAR(10)+edl.Device_Sub_2_Desc+':'+cast(asrv.Device_Sub_2_Nbr AS nvarchar(20)) else '' end)
	as Description ,clm.Description as LogType, '' as FailureReason
	from Namos..Audit_Svr asrv 
	LEFT JOIN
		NamosCustom..DeviceList  as dl on dl.Device_Nbr =  asrv.Device_Nbr 
	LEFT JOIN
		EventDescriptionList edl on edl.Device_Nbr = asrv.Device_Nbr and edl.Event_Nbr =  asrv.Event_Nbr
	LEFT JOIN 
	    LevelMapping clm on clm.Id =  asrv.Critical_Level
	LEFT JOIN 
    (SELECT     emp.persno as persno , emp.persname as persname FROM  dbo.Emp AS emp
    UNION
   SELECT emp00.persno as persno ,emp00.persname as persname from   Namos.dbo.emp00 as emp00 ) as x  on asrv.User_id = x.persno
   
	where CONVERT(DATE,asrv.Log_Date) between CONVERT(DATE,@FromDate) and CONVERT(DATE,@ToDate) and ISNULL(@DeviceType, asrv.Device_Nbr) = asrv.Device_Nbr --and 
	--ISNULL(@User,asrv.User_id ) = asrv.User_id and 	ISNULL(@Level,asrv.Critical_Level ) = asrv.Critical_Level 
   
   
END

