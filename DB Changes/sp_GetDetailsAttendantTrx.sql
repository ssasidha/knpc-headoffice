USE [AttendantModule]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDetailsAttendantTrx]    Script Date: 06-04-2016 13:23:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[sp_GetDetailsAttendantTrx]
 @attendantID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @isSupervisor BIT
	SELECT @isSupervisor = CASE WHEN EmployeeID IS NULL THEN 1 ELSE 0 END FROM AttendantDef WHERE AttendantID = @attendantID

select v.AttendantID, isnull(round(SUM(v.trxamount),2),0) as trxAmount from  v_AttendantTrxViewer v where v.AttendantID =  CASE WHEN @isSupervisor = 1 THEN AttendantID ELSE @attendantID END group by v.AttendantID

END

