USE [NamosCustom]
GO

/****** Object:  StoredProcedure [dbo].[EOD_GetSafeDropReport]    Script Date: 05-04-2016 16:55:45 ******/
DROP PROCEDURE [dbo].[EOD_GetSafeDropReport]
GO

/****** Object:  StoredProcedure [dbo].[EOD_GetSafeDropReport]    Script Date: 05-04-2016 16:55:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--[dbo].[EOD_GetSafeDropReport] '2015-11-01'
CREATE PROCEDURE [dbo].[EOD_GetSafeDropReport] @AccountDate DATETIME
AS
  BEGIN
      SET nocount ON;

      DECLARE @tempTable TABLE
        (
           accountdate DATETIME,
           shiftno     INT,
           eodno       INT,
           error       VARCHAR(100)
        );

      INSERT INTO @tempTable
      EXECUTE dbo.eod_getaccountdatedetails
        @AccountDate;

      WITH details
           AS (SELECT a.shiftno,
                      isnull((SELECT Sum(trx01.amt_paid)
                              FROM   namos.dbo.trx01
                              WHERE  trx01.rectype = 25
                                     AND trx01.eodno = a.eodno), 0)
                      AS
                      CashSale,
                      isnull((SELECT Sum(saf.DroppedAmt)
                              FROM   attendantmodule..AttendantSafeDrop saf
                              WHERE  saf.TrxTimeStamp BETWEEN
                                     ns.timestamp_old AND ns.eod_stamp), 0)
                                                        AS SafeDropSum,
                      isnull((SELECT Sum(droppedAmount)
                              FROM   (SELECT Sum(saf.DroppedAmount) AS
                                             droppedAmount
                                      FROM
                             attendantmodule..AttendantSafeDropShiftWise
                             saf
                                      WHERE  CONVERT(DATE, AccountDate) =
                                             CONVERT(DATE, @AccountDate)
                                             AND a.shiftno = saf.ShiftNo
                                      UNION
                                      SELECT Sum(saf.DroppedAmount) AS
                                             droppedAmount
                                      FROM
                             attendantmodule..AttendantSafeDropExcessShiftWise
                             saf
                                      WHERE  CONVERT(DATE, AccountDate) =
                                             CONVERT(DATE, @AccountDate)
                                             AND a.shiftno = saf.ShiftNo) x), 0)
                      AS
                         SafeDropShiftWise,
                      (SELECT isnull(Sum(dod.AmountPaid) *- 1, 0) AS driveoff
                       FROM   driveoff..DriveOffDetail dod
                              INNER JOIN attendantmodule..AttendantPLCMapping
                                         apm
                                      ON apm.AttendantPLC = dod.AttendantPLC
                              INNER JOIN attendantmodule..AttendantDef defr
                                      ON apm.EmployeeID = defr.EmployeeID
                       WHERE  dod.ShiftNo = a.shiftno
                              AND CONVERT(DATE, dod.AccountDate) =
                                  CONVERT(DATE, @AccountDate)
                              AND apm.status <> 'D')
                      AS
                      driveoff
               FROM   @tempTable a
                      INNER JOIN v_NamosShifts ns
                              ON a.eodno = ns.eodno
               WHERE  a.eodno IN (SELECT eodno
                                  FROM   @tempTable))
      SELECT d.shiftno,
             d.CashSale + d.driveoff                 AS CashSale,
             ( d.SafeDropSum + d.SafeDropShiftWise ) AS SafeDropSum
      FROM   details d
  END 
