USE AttendantModule

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_GetAttendantIdForDomainAccount 
	-- Add the parameters for the stored procedure here
	@domainAccount NVARCHAR(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @stationID nvarchar(50)

	-- Check if not exists
	IF NOT EXISTS (SELECT 1 FROM  AttendantDef where AttendantName = @domainAccount)
	BEGIN
	 -- Save domain account and create relations
	 SELECT @stationID = SettingsValue FROM StationEPi..EPISettings WHERE SettingsKey = 'StationId'
	 Insert into dbo.AttendantDef(AttendantName,EmployeeID, StationId) values(@domainAccount ,NULL, @stationID)
	END

	SELECT AttendantID FROM AttendantDef where AttendantName = @domainAccount
END
GO
