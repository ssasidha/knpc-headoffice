USE [AttendantModule]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDetailsForShiftReport]    Script Date: 11-04-2016 14:11:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_GetDetailsForShiftReport]
	-- Add the parameters for the stored procedure here
@AttendantID int,
@ShiftNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @startTime bigint,@endTime bigint,@max int,@site_id int
declare @Site varchar(50);
DECLARE @EODNo INT
DECLARE @AccountDate DATE
SET @EODNo = @ShiftNo
SELECT @AccountDate = AccountDate, @ShiftNo = ShiftNo FROM dbo.fn_GetAccountDateDetailsFromEODNo(@EODNo)

select @Site=stv.site_name,@site_id=stv.site_id from v_stn00 stv;
if(@AttendantID=0)
BEGIN
set @AttendantID=null;
END
if(@ShiftNo=0)
BEGIN

select def.AttendantName as FirstName,
'Current Shift' as shift,
isnull(csd.DroppedAmount,0) as Amount,csd.TrxTimeStamp as TrxTime,'' as ShiftStart 
,'' as ShiftEnd,@Site as SiteName,isnull(csd.Note,'') as note,@site_id as siteid,isnull(csd.Currency,'') as Currency,DroppedFCurrency, def.EmployeeID  from AttendantDef def inner  join v_CurrentShiftSafeDrop csd on csd.AttendantID=def.AttendantID where def.AttendantID=isnull(@AttendantID,def.AttendantID) order by csd.TrxTimeStamp;
END
ELSE
BEGIN
select @startTime=v.timestamp_old,@endTime=v.eod_stamp from v_NamosShift v where v.eodno=@EODNo;

select FirstName,shift,Amount,TrxTime,ShiftStart,ShiftEnd,SiteName,Note,siteid,Currency,DroppedFCurrency
from
(
select 
def.AttendantName as FirstName,
@ShiftNo as shift,
sd.DroppedAmt as Amount,dbo.FromTimestampUAE(sd.TrxTimeStamp) as TrxTime,(select convert(varchar,(select dbo.FromTimestampUAE(@startTime)))) as ShiftStart
,(select convert(varchar,(select dbo.FromTimestampUAE(@endTime)))) as ShiftEnd,@Site as SiteName,Note,@site_id  as siteid, sd.Currency,sd.DroppedFCurrency, def.EmployeeID from AttendantSafeDrop sd inner join AttendantDef def on sd.AttendantID=def.AttendantID where sd.AttendantID=isnull(@AttendantID,sd.AttendantID) and sd.TrxTimeStamp between @startTime and @endTime
union
select def.AttendantName as FirstName,
@ShiftNo as shift,
sd.DroppedAmount as Amount,dbo.FromTimestampUAE(sd.TrxTimeStamp) as TrxTime
,(select convert(varchar,(select dbo.FromTimestampUAE(@startTime)))) as ShiftStart
,(select convert(varchar,(select dbo.FromTimestampUAE(@endTime)))) as ShiftEnd,@Site as SiteName,Note,@site_id  as siteid
,sd.Currency,DroppedFCurrency, def.EmployeeID
from AttendantSafeDropShiftWise sd inner join AttendantDef def on sd.AttendantID=def.AttendantID where sd.AttendantID=isnull(@AttendantID,sd.AttendantID) and CONVERT(DATE, AccountDate)= @AccountDate AND sd.ShiftNo=@ShiftNo
) results order by TrxTime

END

END
