USE NamosCustom
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sajeev Sasidharan
-- Create date: 13 April 2016
-- Description:	Get Account Date Summary Report
-- =============================================
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[sp_GetAccountDateSummaryReport]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[sp_GetAccountDateSummaryReport]
END
GO
CREATE PROCEDURE [dbo].[sp_GetAccountDateSummaryReport]
@AccountDate datetime
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @results TABLE (ShiftNo INT, CashInForecourt DECIMAL, AttendantSafeDrops DECIMAL, StationManagerSafeDrops DECIMAL, SafeDropSummary DECIMAL,
	AttendantExcessSafeDrops DECIMAL, StationManagerExcessSafeDrops DECIMAL, ExcessSafeDropSummary DECIMAL)

	
declare @shiftDetails
 table(
    accountdate datetime,
    shiftno int,
    eodno int,
	error varchar(100)
);
DECLARE @supervisors TABLE (AttendantID INT, Name NVARCHAR(100))

-- Get Shift Details
insert into @shiftDetails execute dbo.EOD_GetAccountDateDetails @AccountDate

-- Get Supervisors
INSERT INTO @supervisors SELECT AttendantID, AttendantName FROM AttendantModule..AttendantDef WHERE EmployeeID IS NULL
INSERT INTO @results (ShiftNo) SELECT DISTINCT shiftno from @shiftDetails

UPDATE @results 
--;with Details as(
--select a.shiftno,isnull((select Sum(trx01.amt_paid) from namos.dbo.trx01  where trx01.rectype=25 and trx01.eodno=a.eodno),0) as CashSale,
--isnull((select sum(saf.DroppedAmt) from AttendantModule..AttendantSafeDrop saf where saf.TrxTimeStamp between ns.timestamp_old and ns.eod_stamp),0) as SafeDropSum ,
--isnull((select sum(saf.DroppedAmount) from AttendantModule..AttendantSafeDropShiftWise saf where CONVERT(DATE, AccountDate) = CONVERT(DATE,@AccountDate) AND  a.shiftno=saf.ShiftNo
--UNION
--select sum(saf.DroppedAmount) from AttendantModule..AttendantSafeDropExcessShiftWise saf where CONVERT(DATE, AccountDate) = CONVERT(DATE,@AccountDate) AND  a.shiftno=saf.ShiftNo
--),0) as SafeDropShiftWise,

--(select isnull(sum(dod.AmountPaid)*-1,0) AS driveoff from DriveOff..DriveOffDetail dod inner join AttendantModule..AttendantPLCMapping apm on  apm.AttendantPLC=dod.AttendantPLC 
--inner join AttendantModule..AttendantDef defr on  apm.EmployeeID=defr.EmployeeID
--where dod.ShiftNo=a.shiftno AND CONVERT(DATE,dod.AccountDate) = CONVERT(DATE,@AccountDate) and apm.status<>'D')  as driveoff

--from @tempTable a inner join v_NamosShifts ns on a.eodno=ns.eodno where a.eodno in ( select eodno from @tempTable)
--)select d.shiftno,d.CashSale+d.driveoff as  CashSale,(d.SafeDropSum+d.SafeDropShiftWise) as SafeDropSum from Details d

	SELECT * FROm @results

END


