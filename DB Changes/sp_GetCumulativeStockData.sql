USE StationData
GO

IF EXISTS (SELECT 1 FROM sys.objects
			where object_id = OBJECT_ID(N'dbo.sp_GetCumulativeStockData')
			AND type in (N'P', N'PC'))
BEGIN
/****** Object:  StoredProcedure [dbo].[sp_GetCumulativeStockData]    Script Date: 05-04-2016 16:55:45 ******/
DROP PROCEDURE [dbo].[sp_GetCumulativeStockData]
END
GO

/****** Object:  StoredProcedure [dbo].[sp_GetCumulativeStockData]    Script Date: 05-04-2016 16:55:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--[dbo].[sp_GetCumulativeStockData] '2015-11-01'
CREATE PROCEDURE [dbo].[sp_GetCumulativeStockData] 
	-- Add the parameters for the stored procedure here
	@fromDate datetime,
	@toDate datetime,
	@stationId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Declare @tempTable Table(StationId INT, AccountDate datetime,shiftno int,EODNo bigint, Error Varchar(50))
declare @ProductCode Table (StationId INT, grade int, shorttext varchar(100))
declare @SaleQuantity Table (StationId INT, grade int, SaleQuantity float)
declare @TestQuantity Table (StationId INT, grade int, TestQuantity float)
declare @DeliveryVolume Table (StationId INT, grade int, DeliveryVolume float)
declare @Closingstock Table (StationId INT, grade int, closingstock float)
declare @Openingstock Table (StationId INT, grade int, Openingstock float)


set @fromDate=dbo.DateOnly(@fromDate)
set @toDate=dbo.DateOnly(@toDate)

DECLARE @accountDate DATETIME
SET @accountDate = @fromDate

WHILE @accountDate <= @toDate
BEGIN
insert into @tempTable
EXEC [dbo].EOD_GetAccountDateDetails @accountDate, @stationId
SET @accountDate = DATEADD(dd, 1, @accountDate)
END

insert into @ProductCode
select plu00.StationId, grade,shorttext from plu00 inner join fct02 on plu00.artno=fct02.artno AND plu00.StationId = fct02.StationId 
WHERE ISNULL(@StationId, plu00.StationId) = plu00.StationId

insert into @SaleQuantity
select StationId, grade,SUM(quantity) as SaleQuantity from trx05
where eodno in (select eodno from @tempTable) AND ISNULL(@stationId, trx05.StationId) = trx05.StationId
group by StationId, grade

insert into @TestQuantity
select StationId,grade,sum(volume_return) as TestQuantity from trx02
where eodno in (select eodno from @tempTable) AND ISNULL(@stationId, trx02.StationId) = trx02.StationId
group by StationId,grade


insert into @DeliveryVolume
select StationId,grade,SUM(deliv_tov) as DeliveryVolume from codla
where eodno in (select eodno from @tempTable) AND ISNULL(@stationId, codla.StationId) = codla.StationId
group by StationId,grade

insert into @Closingstock
select StationId,grade,sum((case tov when -1 then  0 else  tov end)) as Openingstock from cotnk 
where timestamp in (select max(timestamp) from cotnk where eodno in (select max(eodno) from  @tempTable WHERE StationId = cotnk.StationId) ) 
AND ISNULL(@stationId, cotnk.StationId) = cotnk.StationId
group by StationId,grade

insert into @Openingstock
select StationId,grade,sum((case tov when -1 then  0 else  tov end)) as Openingstock from cotnk c
where timestamp in (select max(timestamp) from cotnk cn where StationId = c.StationId AND eodno in (select max(eodno) from cotnk cnk where StationId = cn.StationId AND eodno<( select MIN(eodno) from @tempTable where eodno<>0 AND StationId = cnk.StationId) ) )
group by StationId,grade

declare @DriveOff table (StationId INT,quantity float, grade int)
declare @StartTime datetime
select @StartTime=dbo.FromTimestampUAE(timestamp_old)  from eod02 
where eodno in (select min(eodno) from @tempTable where eodno<>0) and ISNULL(@stationId, StationId) = StationId
declare @EndTime datetime
select @EndTime=dbo.FromTimestampUAE(eod_stamp)  from eod02
where eodno in (select Max(eodno) from @tempTable) and dbo.FromTimestampUAE(eod_stamp)>DATEADD(hh,22,@accountDate) and ISNULL(@stationId, StationId) = StationId

insert into @DriveOff
select DriveOffDetail.StationId, sum(quantity),grade from  @ProductCode pc left join DriveOffDetail on DriveOffDetail.Product =pc.shorttext
where dbo.FromTimestampUAE(ticketstamp) between @StartTime and @EndTime and ISNULL(@stationId, DriveOffDetail.StationId) = DriveOffDetail.StationId group by grade,DriveOffDetail.StationId


select a.StationId, a.grade,a.shorttext,isnull(b.Openingstock,0) as Openingstock,ISNULL(c.DeliveryVolume,0)as DeliveryVolume,
(0) as transferin, 0 as transferout,isnull(d.SaleQuantity,0)-isnull(g.quantity,0)SaleQuantity,isnull(e.TestQuantity,0)TestQuantity,isnull(g.quantity,0)  as DriveOff,isnull(f.closingstock,0)closingstock
 from @ProductCode a
left outer join @Openingstock b on a.grade=b.grade AND a.StationId = b.StationId
left outer join @DeliveryVolume c on a.grade=c.grade AND a.StationId = c.StationId
left outer join @SaleQuantity d on a.grade=d.grade AND a.StationId = d.StationId
left outer join @TestQuantity e on a.grade=e.grade AND a.StationId = e.StationId
left outer join @Closingstock f on a.grade=f.grade AND a.StationId = f.StationId
left outer join @DriveOff g on a.grade=g.grade AND a.StationId = g.StationId
END
