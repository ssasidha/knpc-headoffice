USE [AttendantModule]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDetailAttendantTrxShiftWise]    Script Date: 06-04-2016 13:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- [dbo].[sp_GetShiftSummaryReportDetails] 118
ALTER PROCEDURE [dbo].[sp_GetDetailAttendantTrxShiftWise] 
@ShiftNo int,
@attendantID int
AS
BEGIN
	SET NOCOUNT ON;
	
declare @StartTime bigint;
declare @EndTime bigint;
declare @Site varchar(50),@site_id int;
DECLARE @EODNo INT
DECLARE @AccountDate DATE
DECLARE @isSupervisor BIT

SELECT @isSupervisor = CASE WHEN EmployeeID IS NULL THEN 1 ELSE 0 END FROM AttendantDef WHERE AttendantID = @attendantID

SET @EODNo = @ShiftNo
SELECT @AccountDate = AccountDate, @ShiftNo = ShiftNo FROM dbo.fn_GetAccountDateDetailsFromEODNo(@EODNo)

select @StartTime=v1.timestamp_old,@EndTime=v1.eod_stamp from v_NamosShift v1 where v1.eodno=@EODNo
select @Site=stv.site_name,@site_id=stv.site_id from v_stn00 stv;

SELECT  AttendantID,round(SUM(SalesAmount) -Sum(DroppedAmt),2) as trxAmount FROM(
	SELECT ad.AttendantID,ad.AttendantName, v.Expr1 as SalesAmount, 0 as DroppedAmt  FROM v_ShiftSalesSummary v JOIN AttendantModule..AttendantPLCMapping apm ON  apm.AttendantPLC = ((SUBSTRING(v.AttendantID, 0, CHARINDEX('=', v.AttendantID))))
	JOIN AttendantDef ad ON ad.EmployeeID = apm.EmployeeID WHERE v.eodno = @EODNo 
	UNION
		SELECT ad.AttendantID,ad.AttendantName, v.Expr1 as SalesAmount, 0 as DroppedAmt  FROM v_ShiftSalesSummary v JOIN AttendantDef ad ON ad.AttendantID = v.tag_operator WHERE v.eodno = @EODNo AND v.AttendantID IS NULL
	UNION 
		SELECT asd.AttendantID,ad.AttendantName, 0, isnull(sum(DroppedAmount),0) as DroppedAmt  from AttendantSafeDropShiftWise asd 
		inner JOIN AttendantDef ad ON ad.AttendantID = asd.AttendantID WHERE CONVERT(DATE, AccountDate)= @AccountDate AND ShiftNo=@ShiftNo group by  asd.AttendantID,ad.AttendantName
	union
	
	select   defr.AttendantID, defr.AttendantName, sum(dod.AmountPaid)*-1 AS SalesAmount, 0 DroppedAmt from DriveOff..DriveOffDetail dod inner join AttendantModule..AttendantPLCMapping apm on  apm.AttendantPLC=dod.AttendantPLC 
inner join AttendantModule..AttendantDef defr on  apm.EmployeeID=defr.EmployeeID

where dod.ShiftNo=@ShiftNo AND CONVERT(DATE,dod.AccountDate) = CONVERT(DATE,@accountDate) and apm.Status<>'D'
group by defr.AttendantID, defr.AttendantName	
)X  where X.AttendantID = CASE WHEN @isSupervisor = 1 THEN X.AttendantID ELSE @attendantID END GROUP BY X.AttendantID,X.AttendantName  
END
